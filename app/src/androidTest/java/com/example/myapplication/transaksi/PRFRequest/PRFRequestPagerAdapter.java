package com.example.myapplication.transaksi.PRFRequest;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.transaksi.prfrequest.DataPRFRequestFragment;

public class PRFRequestPagerAdapter extends FragmentStatePagerAdapter {
    int numTab;

    public PRFRequestPagerAdapter(@NonNull FragmentManager fm, int numTab) {
        super(fm);
        this.numTab = numTab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                DataPRFRequestFragment tabData = new DataPRFRequestFragment();
                return tabData;
            case 1:
                com.example.myapplication.transaksi.PRFRequest.CandidatePRFRequestFragment tabCandidate = new com.example.myapplication.transaksi.PRFRequest.CandidatePRFRequestFragment();
                return tabCandidate;
                default:
                    return null;
        }

    }

    @Override
    public int getCount() {
        return numTab;
    }
}
