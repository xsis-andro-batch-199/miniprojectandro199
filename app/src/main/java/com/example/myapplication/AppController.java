package com.example.myapplication;

import android.app.Application;

import androidx.room.Room;

import com.example.myapplication.db.Database;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
public class AppController extends Application {
    public static Database db;

    @Override
    public void onCreate() {
        super.onCreate();
        db = Room.databaseBuilder(getApplicationContext(), Database.class, "xsis_project")
                .allowMainThreadQueries()
                .build();
    }
}
