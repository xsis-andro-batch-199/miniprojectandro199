package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelPRFRequestCandidates implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int id;
    @ColumnInfo(name = "nickname")
    private String nickname;
    @ColumnInfo(name = "nama")
    private String nama;
    @ColumnInfo(name = "batchbootcamp")
    private String batchBootcamp;
    @ColumnInfo(name = "position")
    private String position;
    @ColumnInfo(name = "placementdate")
    private String placementdate;
    @ColumnInfo(name = "srfnumber")
    private String srfnumber;
    @ColumnInfo(name = "customallowance")
    private String customallowance;
    @ColumnInfo(name = "candidatestatus")
    private String candidatestatus;
    @ColumnInfo(name = "signcontractdate")
    private String signcontractdate;
    @ColumnInfo(name = "notes")
    private String notes;
    @ColumnInfo(name = "singkatan")
    private String singkatan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBatchBootcamp() {
        return batchBootcamp;
    }

    public void setBatchBootcamp(String batchBootcamp) {
        this.batchBootcamp = batchBootcamp;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPlacementdate() {
        return placementdate;
    }

    public void setPlacementdate(String placementdate) {
        this.placementdate = placementdate;
    }

    public String getSrfnumber() {
        return srfnumber;
    }

    public void setSrfnumber(String srfnumber) {
        this.srfnumber = srfnumber;
    }

    public String getCustomallowance() {
        return customallowance;
    }

    public void setCustomallowance(String customallowance) {
        this.customallowance = customallowance;
    }

    public String getCandidatestatus() {
        return candidatestatus;
    }

    public void setCandidatestatus(String candidatestatus) {
        this.candidatestatus = candidatestatus;
    }

    public String getSigncontractdate() {
        return signcontractdate;
    }

    public void setSigncontractdate(String signcontractdate) {
        this.signcontractdate = signcontractdate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSingkatan() {
        return singkatan;
    }

    public void setSingkatan(String singkatan) {
        this.singkatan = singkatan;
    }
}
