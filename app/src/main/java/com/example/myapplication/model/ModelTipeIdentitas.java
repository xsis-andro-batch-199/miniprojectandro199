package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelTipeIdentitas implements Serializable {

    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "tipe_identitas") private String tipe_identitas;
    @ColumnInfo(name = "deskripsi") private String deskripsi;
    @ColumnInfo(name = "isDelete") private Boolean isDelete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipe_identitas() {
        return tipe_identitas;
    }

    public void setTipe_identitas(String tipe_identitas) {
        this.tipe_identitas = tipe_identitas;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}
