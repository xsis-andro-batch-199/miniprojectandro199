package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelJenisCatatan implements Serializable {

    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "nama_jenis_catatan")
    String jenisCatatanNama;
    @ColumnInfo(name = "deskripsi_jenis_catatan")
    String jenisCatatanDeskripsi;
    @ColumnInfo(name = "is_delete")
    Boolean isDelete = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenisCatatanNama() {
        return jenisCatatanNama;
    }

    public void setJenisCatatanNama(String jenisCatatanNama) {
        this.jenisCatatanNama = jenisCatatanNama;
    }

    public String getJenisCatatanDeskripsi() {
        return jenisCatatanDeskripsi;
    }

    public void setJenisCatatanDeskripsi(String jenisCatatanDeskripsi) {
        this.jenisCatatanDeskripsi = jenisCatatanDeskripsi;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}
