package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelStatusPernikahan implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int id;
    @ColumnInfo(name = "statuspernikahan")
    private String statuspernikahan;
    @ColumnInfo(name = "deskripsi")
    private String deskripsi;
    @ColumnInfo(name = "is_delete")
    private Boolean is_delete;

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatuspernikahan() {
        return statuspernikahan;
    }

    public void setStatuspernikahan(String statuspernikahan) {
        this.statuspernikahan = statuspernikahan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
