package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelProject implements Serializable {
    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "is_delete") private Boolean is_delete;
    @ColumnInfo(name = "client_name") private String client_name;
    @ColumnInfo(name = "location") private String location;
    @ColumnInfo(name = "department") private String department;
    @ColumnInfo(name = "pic_name") private String pic_name;
    @ColumnInfo(name = "project_name") private String project_name;
    @ColumnInfo(name = "start") private String start;
    @ColumnInfo(name = "end") private String end;
    @ColumnInfo(name = "role") private String role;
    @ColumnInfo(name = "project_phase") private String project_phase;
    @ColumnInfo(name = "project_description") private String project_description;
    @ColumnInfo(name = "project_technology") private String project_technology;
    @ColumnInfo(name = "main_task") private String main_task;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPic_name() {
        return pic_name;
    }

    public void setPic_name(String pic_name) {
        this.pic_name = pic_name;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProject_phase() {
        return project_phase;
    }

    public void setProject_phase(String project_phase) {
        this.project_phase = project_phase;
    }

    public String getProject_description() {
        return project_description;
    }

    public void setProject_description(String project_description) {
        this.project_description = project_description;
    }

    public String getProject_technology() {
        return project_technology;
    }

    public void setProject_technology(String project_technology) {
        this.project_technology = project_technology;
    }

    public String getMain_task() {
        return main_task;
    }

    public void setMain_task(String main_task) {
        this.main_task = main_task;
    }
}
