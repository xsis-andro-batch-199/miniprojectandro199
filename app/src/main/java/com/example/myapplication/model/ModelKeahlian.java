package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelKeahlian implements Serializable {
    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "nama_keahlian")
    private String nama_keahlian;
    @ColumnInfo(name = "deskripsi_keahlian")
    private String deskripsi_keahlian;
    @ColumnInfo(name = "is_delete")
    private boolean is_delete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_keahlian() {
        return nama_keahlian;
    }

    public void setNama_keahlian(String nama_keahlian) {
        this.nama_keahlian = nama_keahlian;
    }

    public String getDeskripsi_keahlian() {
        return deskripsi_keahlian;
    }

    public void setDeskripsi_keahlian(String deskripsi_keahlian) {
        this.deskripsi_keahlian = deskripsi_keahlian;
    }

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }
}
