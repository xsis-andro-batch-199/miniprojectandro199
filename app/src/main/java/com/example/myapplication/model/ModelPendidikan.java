package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
@Entity
public class ModelPendidikan implements Serializable {
    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "pendidikan") private String pendidikan;
    @ColumnInfo(name = "deskripsi")  private String deskripsi;
    @ColumnInfo(name = "is_delete")  private boolean is_delete;

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
