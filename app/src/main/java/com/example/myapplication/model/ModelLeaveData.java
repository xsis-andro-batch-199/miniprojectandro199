package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelLeaveData implements Serializable {
    @PrimaryKey(autoGenerate = true)
    int id = 1;
    @ColumnInfo(name = "previous_year_leave_quota")
    private int previous_year_leave_quota = 2;
    @ColumnInfo(name = "regular_leave_quota")
    private int regular_leave_quota = 5;
    @ColumnInfo(name = "annual_collective_leave")
    private int annual_collective_leave = 10;
    @ColumnInfo(name = "already_taken")
    private int already_taken = 4;
    @ColumnInfo(name = "total_leave_remaining")
    private int total_leave_remaining = 10;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = 1;
    }

    public int getPrevious_year_leave_quota() {
        return previous_year_leave_quota;
    }

    public void setPrevious_year_leave_quota(int previous_year_leave_quota) {
        this.previous_year_leave_quota = 10;
    }

    public int getRegular_leave_quota() {
        return regular_leave_quota;
    }

    public void setRegular_leave_quota(int regular_leave_quota) {
        this.regular_leave_quota = 10;
    }

    public int getAnnual_collective_leave() {
        return annual_collective_leave;
    }

    public void setAnnual_collective_leave(int annual_collective_leave) {
        this.annual_collective_leave = 5;
    }

    public int getAlready_taken() {
        return already_taken;
    }

    public void setAlready_taken(int already_taken) {
        this.already_taken = 5;
    }

    public int getTotal_leave_remaining() {
        return total_leave_remaining;
    }

    public void setTotal_leave_remaining(int total_leave_remaining) {
        this.total_leave_remaining = 6;
    }
}
