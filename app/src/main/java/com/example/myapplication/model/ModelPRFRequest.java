package com.example.myapplication.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelPRFRequest implements Serializable {
    //model untuk data form
    @PrimaryKey(autoGenerate = true)
    int id;
    @ColumnInfo(name = "tanggal")
    private String tanggal;
    @ColumnInfo(name = "tipe")
    private String tipe;
    @ColumnInfo(name = "placementin")
    private String placementin;
    @ColumnInfo(name = "pid")
    private String pid;
    @ColumnInfo(name = "location")
    private String location;
    @ColumnInfo(name = "period")
    private String periode;
    @ColumnInfo(name = "username")
    private String username;
    @ColumnInfo(name = "tlp")
    private String tlp;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "notebook")
    private String notebook;
    @ColumnInfo(name = "overtime")
    private String overtime;
    @ColumnInfo(name = "bast")
    private String bast;
    @ColumnInfo(name = "billing")
    private String billing;
    @ColumnInfo(name = "singkatan")
    private String singkatan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getPlacementin() {
        return placementin;
    }

    public void setPlacementin(String placementin) {
        this.placementin = placementin;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTlp() {
        return tlp;
    }

    public void setTlp(String tlp) {
        this.tlp = tlp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotebook() {
        return notebook;
    }

    public void setNotebook(String notebook) {
        this.notebook = notebook;
    }

    public String getOvertime() {
        return overtime;
    }

    public void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    public String getBast() {
        return bast;
    }

    public void setBast(String bast) {
        this.bast = bast;
    }

    public String getBilling() {
        return billing;
    }

    public void setBilling(String billing) {
        this.billing = billing;
    }

    public String getSingkatan() {
        return singkatan;
    }

    public void setSingkatan(String singkatan) {
        this.singkatan = singkatan;
    }
}
