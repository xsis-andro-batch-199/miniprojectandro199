package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ModelSchedule implements Serializable {

    @PrimaryKey (autoGenerate = true) int id;
    @ColumnInfo (name = "nama_jadwal") String jadwalNama;
    @ColumnInfo (name = "deskripsi_jadwal") String jadwalDeskripsi;
    @ColumnInfo (name = "is_delete") Boolean isDelete = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJadwalNama() {
        return jadwalNama;
    }

    public void setJadwalNama(String jadwalNama) {
        this.jadwalNama = jadwalNama;
    }

    public String getJadwalDeskripsi() {
        return jadwalDeskripsi;
    }

    public void setJadwalDeskripsi(String jadwalDeskripsi) {
        this.jadwalDeskripsi = jadwalDeskripsi;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}
