package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
@Entity
public class ModelTipeTes implements Serializable {
    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "nama_tipe_tes") String tipeTesNama;
    @ColumnInfo (name = "deskripsi_tipe_tes") String tipeTesDeskripsi;
    @ColumnInfo (name = "is_delete") Boolean isDelete = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipeTesNama() {
        return tipeTesNama;
    }

    public void setTipeTesNama(String tipeTesNama) {
        this.tipeTesNama = tipeTesNama;
    }

    public String getTipeTesDeskripsi() {
        return tipeTesDeskripsi;
    }

    public void setTipeTesDeskripsi(String tipeTesDeskripsi) {
        this.tipeTesDeskripsi = tipeTesDeskripsi;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}
