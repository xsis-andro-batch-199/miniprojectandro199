package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * Dibuat oleh petersam pada 10/07/2019.
 * man.sanji23@gmail.com
 */
@Entity
public class ModelTimesheet implements Serializable {
    @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "status")            private String status;
    @ColumnInfo(name = "client")            private String client;
    @ColumnInfo(name = "report_date")       private String reportDate;
    @ColumnInfo(name = "time_work_start")   private String timeStartWork;
    @ColumnInfo(name = "time_work_end")     private String timeEndWork;
    @ColumnInfo(name = "overtime")          private String overtime;
    @ColumnInfo(name = "time_over_start")   private String timeStartOver;
    @ColumnInfo(name = "time_over_end")     private String timeEndOver;
    @ColumnInfo(name = "notes")             private String notes;
    @ColumnInfo(name = "is_delete")         private boolean is_delete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getTimeStartWork() {
        return timeStartWork;
    }

    public void setTimeStartWork(String timeStartWork) {
        this.timeStartWork = timeStartWork;
    }

    public String getTimeEndWork() {
        return timeEndWork;
    }

    public void setTimeEndWork(String timeEndWork) {
        this.timeEndWork = timeEndWork;
    }

    public String getOvertime() {
        return overtime;
    }

    public void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    public String getTimeStartOver() {
        return timeStartOver;
    }

    public void setTimeStartOver(String timeStartOver) {
        this.timeStartOver = timeStartOver;
    }

    public String getTimeEndOver() {
        return timeEndOver;
    }

    public void setTimeEndOver(String timeEndOver) {
        this.timeEndOver = timeEndOver;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }
}