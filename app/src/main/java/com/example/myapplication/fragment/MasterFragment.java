package com.example.myapplication.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toolbar;


import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.master.agama.AgamaActivity;
import com.example.myapplication.master.back_office_position.HolderBOPFragment;
import com.example.myapplication.master.company.HolderCompanyFragment;
import com.example.myapplication.master.contract_status.MainContractStatusFragment;
import com.example.myapplication.master.employee_status.MainEmployeeStatusFragment;
import com.example.myapplication.master.employee_type.HolderEmployeeTypeFragment;
import com.example.myapplication.master.grade.MainGradeFragment;
import com.example.myapplication.master.jenis_catatan.JenisCatatanActivity;
import com.example.myapplication.master.keahlian.MKeahlianActivity;
import com.example.myapplication.master.pendidikan.JenjangPendidikanActivity;
import com.example.myapplication.master.periode.PeriodeActivity;
import com.example.myapplication.master.position_level.MainPositionLevelFragment;
import com.example.myapplication.master.prf_status.MainPRFStatusFragment;
import com.example.myapplication.master.provider_tools.ProviderToolsFragment;
import com.example.myapplication.master.schedule.ScheduleActivity;
import com.example.myapplication.master.status_pernikahan.StatusPernikahanActivity;
import com.example.myapplication.master.tipe_identitas.TipeIdentitasActivity;
import com.example.myapplication.master.tipe_tes.TipeTesActivity;
import com.example.myapplication.master.training.MainTrainingFragment;
import com.example.myapplication.master.training_organizer.MainTrainingOrganizerFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment {


    public MasterFragment() {
        // Required empty public constructor
    }

    Button btnMDM009, btnMDM005, btnMDM002, btnMDM003, btnMDM001,
            btnMDM008, btnMDM011, btnMDM010, btnMDM004, btnMDM014,
            btnMDM015, btnMDM016, btnMDM012, btnMDM013, btnMDM017,
            btnMDM018, btnMDM019, btnMDM020, btnMDM022, btnMDM021;


    Toolbar toolbar;
    Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_master, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        setUpView(view);
        pindahActivity(view);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

    }

    private void setUpView(View view) {
        getActivity().setTitle("Master");
        toolbar = view.findViewById(R.id.toolbar);
        btnMDM001 = view.findViewById(R.id.btnMDM001);
        btnMDM002 = view.findViewById(R.id.btnMDM002);
        btnMDM003 = view.findViewById(R.id.btnMDM003);
        btnMDM004 = view.findViewById(R.id.btnMDM004);
        btnMDM005 = view.findViewById(R.id.btnMDM005);


        btnMDM008 = view.findViewById(R.id.btnMDM008);
        btnMDM009 = view.findViewById(R.id.btnMDM009);
        btnMDM010 = view.findViewById(R.id.btnMDM010);
        btnMDM011 = view.findViewById(R.id.btnMDM011);
        btnMDM012 = view.findViewById(R.id.btnMDM012);
        btnMDM013 = view.findViewById(R.id.btnMDM013);
        btnMDM014 = view.findViewById(R.id.btnMDM014);
        btnMDM015 = view.findViewById(R.id.btnMDM015);
        btnMDM016 = view.findViewById(R.id.btnMDM016);
        btnMDM017 = view.findViewById(R.id.btnMDM017);
        btnMDM018 = view.findViewById(R.id.btnMDM018);
        btnMDM019 = view.findViewById(R.id.btnMDM019);
        btnMDM020 = view.findViewById(R.id.btnMDM020);
        btnMDM022 = view.findViewById(R.id.btnMDM022);
        btnMDM021 = view.findViewById(R.id.btnMDM021);

    }

    private void pindahActivity(View view) {
        btnMDM001.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AgamaActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM002.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), TipeIdentitasActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM003.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StatusPernikahanActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM004.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), PeriodeActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM005.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), JenjangPendidikanActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM008.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MKeahlianActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM009.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), JenisCatatanActivity.class);
                startActivity(intent);
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM010.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), TipeTesActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM011.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ScheduleActivity.class));
                ((Activity) getActivity()).finish();
            }
        });
        btnMDM012.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                toolbar.setTitle("Company");
                fragment = new HolderCompanyFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM013.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new HolderBOPFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM014.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                toolbar.setTitle(getResources().getString(R.string.providerTools));
                fragment = new ProviderToolsFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM015.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainEmployeeStatusFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM016.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainGradeFragment();
                loadFragmentMaster(fragment);
            }
        });

        btnMDM017.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainPositionLevelFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM018.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainContractStatusFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM019.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new HolderEmployeeTypeFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM020.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainPRFStatusFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM022.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainTrainingOrganizerFragment();
                loadFragmentMaster(fragment);
            }
        });
        btnMDM021.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new MainTrainingFragment();
                loadFragmentMaster(fragment);
            }
        });

    }

    private void loadFragmentMaster(Fragment fragment) {
        FragmentTransaction fragmentTransaction =
                getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
