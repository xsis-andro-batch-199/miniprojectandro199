package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelProject;

import java.util.List;

@Dao
public interface DaoAccessProject {
    //insert table to database
    @Insert void InsertPoject(ModelProject modelProject);

    //select to view all record
    @Query ("SELECT * FROM ModelProject") List<ModelProject> getAllProject();

    //select to view project not deleted yet
    @Query("SELECT * FROM ModelProject WHERE is_delete = 0") List<ModelProject> getViewProject();

    //to set colom is_delete state from each record
    @Query ("UPDATE ModelProject SET is_delete = 1 WHERE id LIKE :id") void deleteViewProject(int id);

    //to update selected record
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateProject(ModelProject modelProject);

    //to search by client name
    @Query("SELECT * FROM ModelProject WHERE client_name LIKE '%' || :clientname || '%'  AND is_delete = 0") List<ModelProject> findByClientName(String clientname);
}
