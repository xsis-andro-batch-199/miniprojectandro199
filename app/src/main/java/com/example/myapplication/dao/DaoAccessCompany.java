package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelCompany;

import java.util.List;

@Dao
public interface DaoAccessCompany {
    @Insert void insertCompany(ModelCompany modelCompany);

    @Query("SELECT * FROM ModelCompany") List<ModelCompany> getDBCompany();
    @Query("SELECT * FROM ModelCompany WHERE is_delete = 0") List<ModelCompany> getViewCompany();
    @Query("UPDATE ModelCompany SET is_delete = 1 WHERE id LIKE :id") void deleteViewCompany(int id);
    @Query("SELECT * FROM ModelCompany WHERE company LIKE '%' || :company || '%'  AND is_delete = 0") List<ModelCompany> findByCompany(String company);

    @Update(onConflict = OnConflictStrategy.REPLACE) void updateCompany(ModelCompany modelCompany);
}
