package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelTimesheet;

import java.util.List;

@Dao
public interface DaoAccessTimesheet {
    @Insert void insertTimesheet(ModelTimesheet modelTimesheet);

    @Query("SELECT status FROM ModelTimesheet WHERE id LIKE :id") String statusTimesheet(int id);
    @Query("SELECT client FROM ModelTimesheet WHERE id LIKE :id") String clientTimesheet(int id);
    @Query("SELECT report_date FROM ModelTimesheet WHERE id LIKE :id") String reportDateTimesheet(int id);
    @Query("SELECT time_work_start FROM ModelTimesheet WHERE id LIKE :id") String timeWorkStartTimesheet(int id);
    @Query("SELECT time_work_end FROM ModelTimesheet WHERE id LIKE :id") String timeWorkEndTimesheet(int id);
    @Query("SELECT overtime FROM ModelTimesheet WHERE id LIKE :id") String overtimeTimesheet(int id);
    @Query("SELECT time_over_start FROM ModelTimesheet WHERE id LIKE :id") String timeOverStartTimesheet(int id);
    @Query("SELECT time_over_end FROM ModelTimesheet WHERE id LIKE :id") String timeOverEndTimesheet(int id);
    @Query("SELECT notes FROM ModelTimesheet WHERE id LIKE :id") String notesTimesheet(int id);

    @Query("SELECT * FROM ModelTimesheet") List<ModelTimesheet> getDBTimesheet();
    @Query("SELECT * FROM ModelTimesheet WHERE is_delete = 0") List<ModelTimesheet> getViewTimesheet();
    @Query("UPDATE ModelTimesheet SET is_delete = 1 WHERE id LIKE :id") void deleteViewTimesheet(int id);
    @Query("SELECT * FROM ModelTimesheet WHERE report_date LIKE '%' || :reportDate || '%'  AND is_delete = 0") List<ModelTimesheet> findByTimesheet(String reportDate);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateTimesheet(ModelTimesheet modelTimesheet);
}
