package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelTraining;

import java.util.List;

@Dao
public interface DaoAccessTraining {

    /*Insert data*/
    @Insert
    void insert_Training(ModelTraining modelTraining);

    /*Select all data*/
    @Query("SELECT * FROM ModelTraining")
    List<ModelTraining> getDB_Training();

    /*Update data*/
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void update_Training(ModelTraining modelTraining);

    /*Delete data*/
    @Query("UPDATE ModelTraining SET is_delete = 1 WHERE id = :id")
    void delete_Training(int id);

    /*Select data is_delete = 0*/
    @Query("SELECT * FROM ModelTraining WHERE is_delete = 0")
    List<ModelTraining> getAll_Training();

    /*Search data*/
    @Query("SELECT * FROM ModelTraining WHERE training_name LIKE '%' || :inputSearch || '%' AND is_delete = 0")
    List<ModelTraining> findByTraining_Name(String inputSearch);

}
