package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelStatusPernikahan;

import java.util.List;

@Dao
public interface DaoAccessStatusPernikahan {
    @Insert
    void insertDataStatusPernikahan(ModelStatusPernikahan modelStatusPernikahan);

    @Query("SELECT * FROM ModelStatusPernikahan WHERE is_delete = 0")
    List<ModelStatusPernikahan> getAllStatusPernikahan();

    @Query("SELECT * FROM ModelStatusPernikahan") List<ModelStatusPernikahan> getAllStatusNoCondition();

    @Query("SELECT * FROM ModelStatusPernikahan") List<ModelStatusPernikahan> getAllStatusPernikahanNoCondition();

    @Query("UPDATE ModelStatusPernikahan SET is_delete = 1 WHERE id LIKE :id") void getUpdateStatusPernikahanAll(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateStatusPernikahan(ModelStatusPernikahan modelStatusPernikahan);

    @Query("SELECT * FROM ModelStatusPernikahan WHERE statuspernikahan LIKE '%' || :statuspernikahan || '%' AND is_delete = 0")
    List<ModelStatusPernikahan> findByStatusPernikahan(String statuspernikahan);


}
