package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelKeahlian;

import java.util.List;

@Dao
public interface DaoAccessKeahlian {
    @Insert
    void insertKeahlian(ModelKeahlian modelKeahlian);
    @Query("SELECT * FROM ModelKeahlian")
    List<ModelKeahlian> getAllKeahlianNoCondition();
    @Query("SELECT * FROM ModelKeahlian WHERE is_delete = 0")
    List<ModelKeahlian> getAllKeahlian();
    @Query("UPDATE ModelKeahlian SET is_delete = 1 WHERE id LIKE :id")
    void getUpdateKeahlian(int id);
    @Query("SELECT * FROM ModelKeahlian WHERE (nama_keahlian LIKE '%' || :keahlian || '%' OR deskripsi_keahlian LIKE '%' || :keahlian || '%') AND is_delete = 0 ")
    List<ModelKeahlian> findByKeahlian(String keahlian);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateKeahlian(ModelKeahlian modelKeahlian);
}
