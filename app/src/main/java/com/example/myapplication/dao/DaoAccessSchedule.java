package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelSchedule;

import java.util.List;

@Dao
public interface DaoAccessSchedule {
    /*Insert Data*/
    @Insert
    void QueryInserSchedule(ModelSchedule modelSchedule);

    /*Select Data*/
    @Query("SELECT * FROM ModelSchedule WHERE is_delete = 0 ")
    List<ModelSchedule> QueryGetAllSchedule();
    @Query("SELECT * FROM ModelSchedule") List<ModelSchedule> QueryGetAllScheduleNoCon();

    /*Delete Data*/
    @Query("UPDATE ModelSchedule SET is_delete = 1 WHERE id LIKE :id") void QueryDeleteSchedule(int id);
//    @Query("DELETE FROM ModelSchedule WHERE id LIKE :id") void deleteSchedule(int id);

    /*Update Data*/
    @Update(onConflict = OnConflictStrategy.REPLACE) public void QueryUpdateSchedule(ModelSchedule modelSchedule);

    /*Pencarian Data*/
    @Query("SELECT * FROM ModelSchedule WHERE (nama_jadwal LIKE '%' || :input || '%' OR deskripsi_jadwal LIKE '%' || :input || '%')  AND is_delete = 0")
    List<ModelSchedule> QueryFindBySchedule(String input);
}
