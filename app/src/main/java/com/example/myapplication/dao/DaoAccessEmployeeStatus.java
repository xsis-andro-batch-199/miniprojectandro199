package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelEmployeeStatus;

import java.util.List;

@Dao
public interface DaoAccessEmployeeStatus {

    @Insert void insertEmployeeStatus(ModelEmployeeStatus modelEmployeeStatus);

    @Query("SELECT * FROM ModelEmployeeStatus")
    List<ModelEmployeeStatus> getAllNoCondition();

    @Query("SELECT * FROM ModelEmployeeStatus WHERE is_delete = 0")
    List<ModelEmployeeStatus> getAllEmployeeStatus();

    @Query("UPDATE ModelEmployeeStatus SET is_delete = 1 WHERE id = :id")
    void getUpdateEmployeeStatus(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updateEmployeeStatus(ModelEmployeeStatus modelEmployeeStatus);

    @Query("SELECT * FROM ModelEmployeeStatus WHERE namaemployee LIKE '%' || :namaEmployee || '%' AND is_delete = 0")
    List<ModelEmployeeStatus> findbyEmployeeStatus(String namaEmployee);

}
