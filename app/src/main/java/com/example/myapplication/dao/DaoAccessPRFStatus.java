package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelPRFStatus;

import java.util.List;

@Dao
public interface DaoAccessPRFStatus {
    @Insert void insertPRFStatus(ModelPRFStatus modelPRFStatus);

    @Query("SELECT * FROM ModelPRFStatus")
    List<ModelPRFStatus> getAllPRFStatus();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePRFStatus(ModelPRFStatus modelPRFStatus);

    @Query("DELETE FROM ModelPRFStatus WHERE id LIKE :id")
    void deletePRFStatus(int id);

    @Query("SELECT * FROM ModelPRFStatus WHERE namaprf LIKE '%' || :inputSearch")
    List<ModelPRFStatus> findPRFStatus(String inputSearch);
}
