package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;

import java.util.List;

@Dao
public interface DaoAccessPRFRequest {

    @Insert
    void insertDataPRF(ModelPRFRequest modelPRFRequest);
    @Insert
    void insertDataPRFCandidates(ModelPRFRequestCandidates modelPRFRequestCandidates);

    @Query("SELECT * FROM ModelPRFRequest")
    List<ModelPRFRequest> getAllPRF();
    @Query("SELECT * FROM ModelPRFRequestCandidates") List<ModelPRFRequestCandidates> getAllPRFCandidates();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePRFCandidate(ModelPRFRequestCandidates modelPRFRequestCandidates);
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePRFData(ModelPRFRequest modelPRFRequestData);

    @Query("DELETE FROM ModelPRFRequest WHERE id LIKE :id") void deletePRFData(int id);
    @Query("DELETE FROM modelprfrequestcandidates WHERE id LIKE :id") void DeletePRFCandidate(int id);

    @Query("SELECT * FROM ModelPRFRequest WHERE placementin LIKE '%' || :tempat || '%' ")
    List<ModelPRFRequest> findbyplacement(String tempat);



}
