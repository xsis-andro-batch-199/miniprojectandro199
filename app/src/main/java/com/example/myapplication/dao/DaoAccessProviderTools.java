package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelProviderTools;

import java.util.List;

@Dao
public interface DaoAccessProviderTools {

    @Insert void insertProviderTools(ModelProviderTools modelProviderTools);

    @Query("SELECT * FROM ModelProviderTools")
    List<ModelProviderTools> getAllNoCondition();

    @Query("SELECT * FROM ModelProviderTools WHERE is_delete = 0")
    List<ModelProviderTools> getAllProviderTools();

    @Query("UPDATE ModelProviderTools SET is_delete = 1 WHERE id = :id")
    void getUpdateProviderTools(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updateProvider(ModelProviderTools modelProviderTools);

    @Query("SELECT * FROM ModelProviderTools WHERE provider LIKE '%' || :providerTools || '%' AND is_delete = 0")
    List<ModelProviderTools> findByProvider(String providerTools);
}
