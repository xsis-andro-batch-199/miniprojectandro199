package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelEmployeeTraining;

import java.util.List;

@Dao
public interface DaoAccessEmployeeTraining {
    @Insert
    void insertEmployeeTraining(ModelEmployeeTraining modelEmployeeTraining);

    @Query("SELECT * FROM ModelEmployeeTraining")
    List<ModelEmployeeTraining> getAllEmplyeeTrainingNoCondition();

    @Query("SELECT * FROM ModelEmployeeTraining WHERE is_delete = 0")
    List<ModelEmployeeTraining> getAllEmployeeTraining();

    @Query("UPDATE ModelEmployeeTraining SET is_delete = 1 WHERE id = :id")
    void getUpdateEmployeeTraining(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateEmployeeTraining(ModelEmployeeTraining modelEmployeeTraining);

    @Query("SELECT * FROM ModelEmployeeTraining WHERE nama_pegawai LIKE '%' || :pegawai || '%' AND is_delete = 0")
    List<ModelEmployeeTraining> findByEmployeeTraining(String pegawai);
}
