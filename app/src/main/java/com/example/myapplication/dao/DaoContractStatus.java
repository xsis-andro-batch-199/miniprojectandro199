package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelContractStatus;

import java.util.List;

@Dao
public interface DaoContractStatus {

    @Insert void insertContractStatus(ModelContractStatus modelContractStatus);

    @Query("SELECT * FROM ModelContractStatus")
    List<ModelContractStatus> getAllContractStatus();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updateContractStatus(ModelContractStatus modelContractStatus);

    @Query("DELETE FROM ModelContractStatus WHERE id LIKE :id")
    void deleteContractStatus(int id);

    @Query("SELECT * FROM ModelContractStatus WHERE namacontract LIKE '%' || :inputSearch || '%'")
    List<ModelContractStatus> findContractStatus(String inputSearch);
}
