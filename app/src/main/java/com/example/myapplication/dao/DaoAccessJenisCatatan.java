package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelJenisCatatan;

import java.util.List;

@Dao
public interface DaoAccessJenisCatatan {
    /*Insert Data*/
    @Insert
    void QueryInserJenisCatatan(ModelJenisCatatan modelJenisCatatan);

    /*Select Data*/
    @Query("SELECT * FROM ModelJenisCatatan WHERE is_delete = 0 ")
    List<ModelJenisCatatan> QueryGetAllJenisCatatan();
    @Query("SELECT * FROM ModelJenisCatatan") List<ModelJenisCatatan> QueryGetAllJenisCatatanNoCon();

    /*Delete Data*/
    @Query("UPDATE ModelJenisCatatan SET is_delete = 1 WHERE id LIKE :id") void QueryDeleteJenisCatatan(int id);
//    @Query("DELETE FROM ModelJenisCatatan WHERE id LIKE :id") void deleteJenisCatatan(int id);

    /*Update Data*/
    @Update(onConflict = OnConflictStrategy.REPLACE) public void QueryUpdateJenisCatatan(ModelJenisCatatan modelJenisCatatan);

    /*Pencarian Data*/
    @Query("SELECT * FROM ModelJenisCatatan WHERE (nama_jenis_catatan LIKE '%' || :input || '%' OR deskripsi_jenis_catatan LIKE '%' || :input || '%')  AND is_delete = 0")
    List<ModelJenisCatatan> QueryFindByJenisCatatan(String input);
}
