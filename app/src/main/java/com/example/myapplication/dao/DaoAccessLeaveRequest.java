package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelLeave;
import com.example.myapplication.model.ModelLeaveData;

import java.util.List;

@Dao
public interface DaoAccessLeaveRequest {
    /*Insert data*/
    @Insert void insertLeave(ModelLeave modelLeave);

    /*Select all data*/
    @Query("SELECT * FROM ModelLeave")
    List<ModelLeave> getDBLeave();

    /*Delete data*/
    @Query("DELETE FROM ModelLeave WHERE id LIKE :id")
    void  deleteLeave(int id);

    /*Update data*/
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updateLeave(ModelLeave modelLeave);

    /*Select data from any field*/
    @Query("SELECT leave_type FROM ModelLeave WHERE id LIKE :id") String leaveType(int id);
    @Query("SELECT leave_name FROM ModelLeave WHERE id LIKE :id") String leaveName(int id);
    @Query("SELECT startDate FROM ModelLeave WHERE id LIKE :id") String startDate(int id);
    @Query("SELECT endDate FROM ModelLeave WHERE id LIKE :id") String endDate(int id);
    @Query("SELECT address FROM ModelLeave WHERE id LIKE :id") String address(int id);
    @Query("SELECT contact FROM ModelLeave WHERE id LIKE :id") String contact(int id);
    @Query("SELECT reason FROM ModelLeave WHERE id LIKE :id") String reason(int id);
    @Query("SELECT approval1 FROM ModelLeave WHERE id LIKE :id") String approval1(int id);
    @Query("SELECT approval2 FROM ModelLeave WHERE id LIKE :id") String approval2(int id);
    @Query("SELECT approval3 FROM ModelLeave WHERE id LIKE :id") String approval3(int id);

    /*Search data*/
    @Query("SELECT * from ModelLeave WHERE startDate LIKE '%' || :searchDate || '%' OR endDate LIKE '%' || :searchDate || '%'")
    List<ModelLeave> findByDate(String searchDate);

    /*Select data leave data*/
    @Query("SELECT * FROM ModelLeaveData") ModelLeaveData getViewLeaveData();


}
