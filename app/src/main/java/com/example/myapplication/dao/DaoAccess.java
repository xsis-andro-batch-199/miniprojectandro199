package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelAgama;
import com.example.myapplication.model.ModelJenisCatatan;

import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;

import com.example.myapplication.model.ModelProject;
import com.example.myapplication.model.ModelSchedule;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.example.myapplication.model.ModelTimesheet;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.example.myapplication.model.ModelPendidikan;
import com.example.myapplication.model.ModelTipeTes;

import java.util.List;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
@Dao
public interface DaoAccess {
    /*insert data to database*/
    @Insert void insertDataPRF(ModelPRFRequest modelPRFRequest);
    @Insert void insertDataPRFCandidates(ModelPRFRequestCandidates modelPRFRequestCandidates);
    @Insert void insertPendidikan(ModelPendidikan modelPendidikan);
    @Insert void innsertTipeIdentitas(ModelTipeIdentitas modelTipeIdentitas);
    @Insert void insertDataStatusPernikahan(ModelStatusPernikahan modelStatusPernikahan);
    @Insert void insertAgama(ModelAgama modelAgama);
    /*end insert data to database*/

    /*select data from database*/
    @Query("SELECT * FROM ModelPendidikan WHERE is_delete = 0") List<ModelPendidikan> getAllPendidikan();
    @Query("SELECT * FROM ModelTipeIdentitas WHERE isDelete = 0") List<ModelTipeIdentitas> getAllTipeIdentitas();
    @Query("SELECT * FROM ModelStatusPernikahan WHERE is_delete = 0") List<ModelStatusPernikahan> getAllStatusPernikahan();
    @Query("SELECT * FROM ModelAgama WHERE is_delete = 0") List<ModelAgama> getAllAgama();
    @Query("SELECT * FROM ModelStatusPernikahan") List<ModelStatusPernikahan> getAllStatusNoCondition();
    /*end select data from database*/

    /*cek data di database*/
    @Query("SELECT * FROM ModelPendidikan") List<ModelPendidikan> getAllPendidikanNoCondition();
    @Query("SELECT * FROM ModelTipeIdentitas") List<ModelTipeIdentitas> getAllIdentitasNoCondition();
    @Query("SELECT * FROM ModelStatusPernikahan") List<ModelStatusPernikahan> getAllStatusPernikahanNoCondition();
    @Query("SELECT * FROM ModelAgama") List<ModelAgama> getAllAgamaNoCondition();
    @Query("SELECT * FROM ModelPRFRequest") List<ModelPRFRequest> getAllPRF();
    @Query("SELECT * FROM ModelPRFRequestCandidates") List<ModelPRFRequestCandidates> getAllPRFCandidates();
    /*end cek data di database*/

    /*hapus data from view*/
    @Query("UPDATE ModelPendidikan SET is_delete = 1 WHERE id LIKE :id") void getUpdatePendidikanAll(int id);
    @Query("UPDATE ModelTipeIdentitas SET isDelete = 1 WHERE id LIKE :id") void getUpdateTipeIdentitasAll(int id);
    @Query("UPDATE ModelStatusPernikahan SET is_delete = 1 WHERE id LIKE :id") void getUpdateStatusPernikahanAll(int id);
    @Query("UPDATE ModelAgama SET is_delete = 1 WHERE id LIKE :id") void getUpdateAgama(int id);
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePRFCandidate(ModelPRFRequestCandidates modelPRFRequestCandidates);
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePRFData(ModelPRFRequest modelPRFRequestData);
    /*end hapus data from view*/


    /*delete data dari database*/
    @Query("DELETE FROM ModelPendidikan WHERE id LIKE :id") void deletePendidikan(int id);
    @Query("DELETE FROM ModelTipeIdentitas WHERE id LIKE :id") void deleteIdentitas(int id);
    @Query("DELETE FROM ModelStatusPernikahan WHERE id LIKE :id") void deleteStatusPernikahan(int id);
    @Query("DELETE FROM ModelPRFRequest WHERE id LIKE :id") void deletePRFData(int id);
    @Query("DELETE FROM modelprfrequestcandidates WHERE id LIKE :id") void DeletePRFCandidate(int id);
    @Query("DELETE FROM ModelAgama WHERE id LIKE :id") void deleteAgama(int id);
    /*end delete data dari database*/

    /*update data for edit data*/
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updatePendidikan(ModelPendidikan modelPendidikan);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateIdentitas(ModelTipeIdentitas modelTipeIdentitas);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateStatusPernikahan(ModelStatusPernikahan modelStatusPernikahan);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updateAgama(ModelAgama modelAgama);
    /*end update data for edit data*/

    /*pencarian data*/
    @Query("SELECT * FROM ModelPRFRequest WHERE placementin LIKE '%' || :tempat || '%' ")
    List<ModelPRFRequest> findbyplacement(String tempat);
    @Query("SELECT * FROM ModelPendidikan WHERE (pendidikan LIKE '%' || :pendidikan || '%' OR deskripsi LIKE '%' || :pendidikan || '%') AND is_delete = 0")
    List<ModelPendidikan> findByPendidikan(String pendidikan);
    @Query("SELECT * FROM ModelStatusPernikahan WHERE statuspernikahan LIKE '%' || :statuspernikahan || '%' AND is_delete = 0")
    List<ModelStatusPernikahan> findByStatusPernikahan(String statuspernikahan);
    @Query("SELECT * FROM ModelAgama WHERE (agama LIKE '%' || :agama || '%' OR deskripsi LIKE '%' || :agama ||'%') AND is_delete = 0")
    List<ModelAgama> findByAgama(String agama);
    @Query("SELECT * FROM ModelTipeIdentitas WHERE tipe_identitas LIKE '%' || :identitas || '%' AND isDelete = 0")
    List<ModelTipeIdentitas> findByIdentitas(String identitas);


}