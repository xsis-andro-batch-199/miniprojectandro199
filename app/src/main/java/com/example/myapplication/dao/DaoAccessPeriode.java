package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelPeriode;

import java.util.List;

@Dao
public interface DaoAccessPeriode {
    @Insert
    void insertPeriode(ModelPeriode modelPeriode);
    @Query("SELECT * FROM ModelPeriode")
    List<ModelPeriode> getAllPeriodeNoCondition();
    @Query("SELECT * FROM ModelPeriode WHERE is_delete = 0") List<ModelPeriode> getAllPeriode();
    @Query("UPDATE ModelPeriode SET is_delete = 1 WHERE id LIKE :id") void getUpdatePeriode(int id);
    @Update(onConflict = OnConflictStrategy.REPLACE) public void updatePeriode(ModelPeriode modelPeriode);
    @Query("SELECT * FROM ModelPeriode WHERE (periode LIKE '%' || :periode || '%' OR deskripsi LIKE '%' || :periode || '%') AND is_delete = 0")
    List<ModelPeriode> findByPeriode(String periode);
}
