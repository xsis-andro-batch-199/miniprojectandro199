package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelTrainingOrganizer;

import java.util.List;

@Dao
public interface DaoAccessTrainingOrganizer {

    /*Insert data*/
    @Insert
    void insert_TO(ModelTrainingOrganizer modelTrainingOrganizer);

    /*Select all data*/
    @Query("SELECT * FROM ModelTrainingOrganizer")
    List<ModelTrainingOrganizer> getDB_TO();

    /*Update data*/
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void update_TO(ModelTrainingOrganizer modelTrainingOrganizer);

    /*Delete data*/
    @Query("UPDATE ModelTrainingOrganizer SET is_delete = 1 WHERE id = :id")
    void delete_TO(int id);

    /*Select data is_delete = 0*/
    @Query("SELECT * FROM ModelTrainingOrganizer WHERE is_delete = 0")
    List<ModelTrainingOrganizer> getAll_TO();

    /*Search data*/
    @Query("SELECT * FROM ModelTrainingOrganizer WHERE to_name LIKE '%' || :inputSearch || '%' AND is_delete = 0")
    List<ModelTrainingOrganizer> findByTO_Name(String inputSearch);
}
