package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelGrade;

import java.util.List;

@Dao
public interface DaoAccessGrade {
    @Insert void insertGrade(ModelGrade modelGrade);

    @Query("SELECT * FROM ModelGrade")
    List<ModelGrade> getAllGradeNoCondition();

    @Query("SELECT * FROM ModelGrade WHERE is_delete = 0")
    List<ModelGrade> getAllGrade();

    @Query("UPDATE ModelGrade SET is_delete = 1 WHERE id = :id")
    void getUpdateGrade(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateGrade(ModelGrade modelGrade);

    @Query("SELECT * FROM ModelGrade WHERE grade LIKE '%' || :grade || '%' AND is_delete = 0")
    List<ModelGrade> findByGrade(String grade);
}
