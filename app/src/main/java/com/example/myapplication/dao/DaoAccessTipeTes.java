package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelTipeTes;

import java.util.List;

@Dao
public interface DaoAccessTipeTes {
    /*Insert Data*/
    @Insert
    void QueryInserTipeTes(ModelTipeTes modelTipeTes);

    /*Select Data*/
    @Query("SELECT * FROM ModelTipeTes WHERE is_delete = 0 ")
    List<ModelTipeTes> QueryGetAllTipeTes();
    @Query("SELECT * FROM ModelTipeTes") List<ModelTipeTes> QueryGetAllTipeTesNoCon();

    /*Delete Data*/
    @Query("UPDATE ModelTipeTes SET is_delete = 1 WHERE id LIKE :id") void QueryDeleteTipeTes(int id);
//    @Query("DELETE FROM ModelTipeTes WHERE id LIKE :id") void deleteTipeTes(int id);

    /*Update Data*/
    @Update(onConflict = OnConflictStrategy.REPLACE) public void QueryUpdateTipeTes(ModelTipeTes modelTipeTes);

    /*Pencarian Data*/
    @Query("SELECT * FROM ModelTipeTes WHERE (nama_tipe_tes LIKE '%' || :input || '%' OR deskripsi_tipe_tes LIKE '%' || :input || '%')  AND is_delete = 0")
    List<ModelTipeTes> QueryFindByTipeTes(String input);
}
