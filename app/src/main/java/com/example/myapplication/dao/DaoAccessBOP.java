package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelBOP;

import java.util.List;

@Dao
public interface DaoAccessBOP {
    @Insert void insertBOP(ModelBOP modelBOP);

    @Query("SELECT * FROM ModelBOP") List<ModelBOP> getDBBOP();
    @Query("SELECT * FROM ModelBOP WHERE is_delete = 0") List<ModelBOP> getViewBOP();
    @Query("UPDATE ModelBOP SET is_delete = 1 WHERE id LIKE :id") void deleteViewBOP(int id);
    @Query("SELECT * FROM ModelBOP WHERE name LIKE '%' || :name || '%'  AND is_delete = 0") List<ModelBOP> findByBOP(String name);

    @Update(onConflict = OnConflictStrategy.REPLACE) void updateBOP(ModelBOP modelBOP);
}
