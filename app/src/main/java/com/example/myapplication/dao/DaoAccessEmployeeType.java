package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelEmployeeType;

import java.util.List;

@Dao
public interface DaoAccessEmployeeType {
    @Insert void insertEmployeeType(ModelEmployeeType modelEmployeeType);

    @Query("SELECT * FROM ModelEmployeeType")
    List<ModelEmployeeType> getDBEmployeeType();
    @Query("SELECT * FROM ModelEmployeeType WHERE is_delete = 0") List<ModelEmployeeType> getViewEmployeeType();
    @Query("UPDATE ModelEmployeeType SET is_delete = 1 WHERE id LIKE :id") void deleteViewEmployeeType(int id);
    @Query("SELECT * FROM ModelEmployeeType WHERE name LIKE '%' || :name || '%'  AND is_delete = 0") List<ModelEmployeeType> findByEmployeeType(String name);

    @Update(onConflict = OnConflictStrategy.REPLACE) void updateEmployeeType(ModelEmployeeType modelEmployeeType);
}
