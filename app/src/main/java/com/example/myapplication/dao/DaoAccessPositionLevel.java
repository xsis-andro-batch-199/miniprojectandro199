package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelPositionLevel;

import java.util.List;

@Dao
public interface DaoAccessPositionLevel {

    /*Insert data*/
    @Insert
    void insertPositionLevel(ModelPositionLevel modelPositionLevel);

    /*Select all data*/
    @Query("SELECT * FROM ModelPositionLevel")
    List<ModelPositionLevel> getDBPositionLevel();

    /*Update data*/
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void updatePositionLevel(ModelPositionLevel modelPositionLevel);

    /*Delete data*/
    @Query("DELETE FROM ModelPositionLevel WHERE id LIKE :id")
    void  deletePositionLevel(int id);

    /*Search Data*/
    @Query("SELECT * FROM ModelPositionLevel WHERE position_name LIKE '%' || :inputSearch || '%'")
    List<ModelPositionLevel> findByPositionLevel(String inputSearch);
}
