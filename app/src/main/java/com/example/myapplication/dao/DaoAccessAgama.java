package com.example.myapplication.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.model.ModelAgama;

import java.util.List;

@Dao
public interface DaoAccessAgama {

    /*insert data*/
    @Insert
    void insertAgama(ModelAgama modelAgama);

    /*select data from database*/
    @Query
            ("SELECT * FROM ModelAgama WHERE is_delete = 0")
    List<ModelAgama> getAllAgama();

    @Query
            ("SELECT * FROM ModelAgama")
    List<ModelAgama> getAllAgamaNoCondition();

    /*delete data from view*/
    @Query
            ("UPDATE ModelAgama SET is_delete = 1 WHERE id LIKE :id")
    void getUpdateAgama(int id);

    /*delete data from database*/
    @Query
            ("DELETE FROM ModelAgama WHERE id LIKE :id")
    void deleteAgama(int id);

    /*update data for edit form*/
    @Update
            (onConflict = OnConflictStrategy.REPLACE)
    public void updateAgama(ModelAgama modelAgama);

    /*search data*/
    @Query
            ("SELECT * FROM ModelAgama WHERE " +
                    "(agama LIKE '%' || :search || '%' " +
                    "OR deskripsi LIKE '%' || :search || '%') " +
                    "AND is_delete = 0")
    List<ModelAgama> findByAgama(String search);

}
