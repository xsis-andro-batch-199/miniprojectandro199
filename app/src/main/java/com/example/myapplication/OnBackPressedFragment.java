package com.example.myapplication;

public interface OnBackPressedFragment {
    boolean onBackPressed();
}
