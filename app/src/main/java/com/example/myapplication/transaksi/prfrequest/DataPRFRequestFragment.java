package com.example.myapplication.transaksi.prfrequest;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.PRFRequestDataAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelPRFRequest;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataPRFRequestFragment extends Fragment {
    private List<ModelPRFRequest> listModel = new ArrayList<>();
    private PRFRequestDataAdapter dataAdapter;
    private RecyclerView recyclerView;
    private ImageView btnAdd;


    public DataPRFRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_prf_request,container,false);
        setupView(view);
        loadData();
        ShowData();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                listModel = db.daoAccessPRFRequest().findbyplacement(s);
                ShowData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                listModel = db.daoAccessPRFRequest().findbyplacement(s);
                ShowData();
                return true;
            }
        });
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.recDataPRF);
        btnAdd = view.findViewById(R.id.btn_tambah);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentFormData = new PRFRequestFormFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentFormData);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project")
                .allowMainThreadQueries().build();
        listModel = db.daoAccessPRFRequest().getAllPRF();

    }

    private void ShowData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        dataAdapter = new PRFRequestDataAdapter(getContext(), listModel);
        recyclerView.setAdapter(dataAdapter);

    }

}
