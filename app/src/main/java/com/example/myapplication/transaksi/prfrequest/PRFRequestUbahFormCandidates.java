package com.example.myapplication.transaksi.prfrequest;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;


import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;
import com.google.android.material.textfield.TextInputLayout;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class PRFRequestUbahFormCandidates extends Fragment {
    private EditText bootcamp, placementDate, customAllowance, signContract, notes;
    private AutoCompleteTextView name, positionCandidate, srfNumber, candidatesStatus;
    private Button submit, reset;


    public PRFRequestUbahFormCandidates() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_prfrequest_ubah_form_candidates, container, false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Candidate");
        setupView(view);
        receiveData();

        return view;

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelPRFRequestCandidates modelPRFRequestCandidates = (ModelPRFRequestCandidates) bundle.getSerializable("data");
        final TextInputLayout msgErrorName = (TextInputLayout) view.findViewById(R.id.msgErrorNama);
        final TextInputLayout msgErrorPositionCandidate = (TextInputLayout) view.findViewById(R.id.msgErrorPositionCandidate);
        final TextInputLayout msgErrorPlacementDate = (TextInputLayout) view.findViewById(R.id.msgErrorPlacementDate);
        final TextInputLayout msgErrorSRFnumber = (TextInputLayout) view.findViewById(R.id.msgErrorSRFNumber);
        final TextInputLayout msgErrorCandidateStatus = (TextInputLayout) view.findViewById(R.id.msgErrorCandidateStatus);

        bootcamp = view.findViewById(R.id.inputBatch);
        bootcamp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputBootcamp = bootcamp.getText().toString();
                if (!modelPRFRequestCandidates.getBatchBootcamp().equals(inputBootcamp) &&
                        !bootcamp.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });
        placementDate = view.findViewById(R.id.inputPlacementDate);
        placementDate.setHint("Placement Date *");
        placementDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(placementDate.getText())) {
                    placementDate.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPlacementDate.setError("Required");
                } else {
                    msgErrorPlacementDate.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputPlacementDate = placementDate.getText().toString();
                if (!modelPRFRequestCandidates.getCandidatestatus().equals(inputPlacementDate) &&
                        !placementDate.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });
        customAllowance = view.findViewById(R.id.inputCustom);
        customAllowance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputCustomeAllowance = customAllowance.getText().toString();
                if (!modelPRFRequestCandidates.getCustomallowance().equals(inputCustomeAllowance) &&
                        !customAllowance.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        signContract = view.findViewById(R.id.inputSignContract);
        signContract.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputSignContract = signContract.getText().toString();
                if (!modelPRFRequestCandidates.getSigncontractdate().equals(inputSignContract) &&
                        !signContract.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        notes = view.findViewById(R.id.inputNotes);
        notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputNotes = notes.getText().toString();
                if (!modelPRFRequestCandidates.getNotes().equals(inputNotes) &&
                        !notes.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });

        name = view.findViewById(R.id.inputNamaCandidate);
        name.setHint("Name *");
        name.setThreshold(0);
        String[] nameSpinner = getResources().getStringArray(R.array.input_name);
        name.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, nameSpinner));
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(name.getText())) {
                    name.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorName.setError("Required");
                } else {
                    msgErrorName.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputNama = name.getText().toString();
                if (!modelPRFRequestCandidates.getNama().equals(inputNama) &&
                        !name.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });


        srfNumber = view.findViewById(R.id.inputSRFNumber);
        srfNumber.setHint("SRF Number *");
        srfNumber.setThreshold(0);
        String[] srfSpinner = getResources().getStringArray(R.array.SRF_Number);
        srfNumber.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, srfSpinner));
        srfNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(srfNumber.getText())) {
                    srfNumber.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorSRFnumber.setError("Required");
                } else {
                    msgErrorSRFnumber.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputSrf = srfNumber.getText().toString();
                if (!modelPRFRequestCandidates.getSrfnumber().equals(inputSrf) &&
                        !srfNumber.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });

        positionCandidate = view.findViewById(R.id.inputPositionCandidate);
        positionCandidate.setHint("Position *");
        positionCandidate.setThreshold(0);
        String[] positionCandidateSpinner = getResources().getStringArray(R.array.position);
        positionCandidate.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, positionCandidateSpinner));
        positionCandidate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(positionCandidate.getText())) {
                    positionCandidate.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPositionCandidate.setError("Required");
                } else {
                    msgErrorPositionCandidate.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputpostionCandidate = positionCandidate.getText().toString();
                if (!modelPRFRequestCandidates.getPosition().equals(inputpostionCandidate) &&
                        !positionCandidate.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });

        candidatesStatus = view.findViewById(R.id.inputCandidateStatus);
        candidatesStatus.setHint("Candidate Status *");
        candidatesStatus.setThreshold(0);
        String[] candidateStatusSpinner = getResources().getStringArray(R.array.status);
        candidatesStatus.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, candidateStatusSpinner));
        candidatesStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(candidatesStatus.getText())) {
                    candidatesStatus.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorCandidateStatus.setError("Required");
                } else {
                    msgErrorCandidateStatus.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputCandidatesStatus = candidatesStatus.getText().toString();
                if (!modelPRFRequestCandidates.getCandidatestatus().equals(inputCandidatesStatus) &&
                        !candidatesStatus.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    reset.setEnabled(false);
                    submit.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });

        reset = view.findViewById(R.id.btnReset);
        submit = view.findViewById(R.id.btnSubmit);

    }

    private void receiveData() {
        Bundle bundle = this.getArguments();
        ModelPRFRequestCandidates modelPRFRequestCandidates = (ModelPRFRequestCandidates) bundle.getSerializable("data");

        if (bundle != null) {
            String namaCandidate = modelPRFRequestCandidates.getNama();
            String bootcampCandidate = modelPRFRequestCandidates.getBatchBootcamp();
            String positionCandidateCandidate = modelPRFRequestCandidates.getPosition();
            String placementCandidate = modelPRFRequestCandidates.getPlacementdate();
            String srfNimberCandidate = modelPRFRequestCandidates.getSrfnumber();
            String customAllowanceCandidate = modelPRFRequestCandidates.getCustomallowance();
            String candidatestatusCandidate = modelPRFRequestCandidates.getCandidatestatus();
            String signupcontractCandidate = modelPRFRequestCandidates.getSigncontractdate();
            String notesCandidate = modelPRFRequestCandidates.getNotes();

            bootcamp.setText(bootcampCandidate);
            placementDate.setText(placementCandidate);
            customAllowance.setText(customAllowanceCandidate);
            signContract.setText(signupcontractCandidate);
            notes.setText(notesCandidate);
            name.setText(namaCandidate);
            positionCandidate.setText(positionCandidateCandidate);
            srfNumber.setText(srfNimberCandidate);
            candidatesStatus.setText(candidatestatusCandidate);

        } else {

        }

    }

    private void inputData() {
        Bundle bundle = this.getArguments();
        final ModelPRFRequestCandidates modelPRFRequest = (ModelPRFRequestCandidates) bundle.getSerializable("data");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelPRFRequestCandidates updateData = new ModelPRFRequestCandidates();
                updateData.setId(modelPRFRequest.getId());
                updateData.setNama(name.getText().toString());
                updateData.setBatchBootcamp(bootcamp.getText().toString());
                updateData.setPlacementdate(placementDate.getText().toString());
                updateData.setCustomallowance(customAllowance.getText().toString());
                updateData.setSigncontractdate(signContract.getText().toString());
                updateData.setNotes(notes.getText().toString());
                updateData.setSrfnumber(srfNumber.getText().toString());
                updateData.setPosition(positionCandidate.getText().toString());
                updateData.setCandidatestatus(candidatesStatus.getText().toString());
                db.daoAccessPRFRequest().updatePRFCandidate(updateData);
                Fragment MainFragmentPRF = new MainPRFRequestFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, MainFragmentPRF);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name.setText(modelPRFRequest.getNama());
                bootcamp.setText(modelPRFRequest.getBatchBootcamp());
                placementDate.setText(modelPRFRequest.getPlacementdate());
                customAllowance.setText(modelPRFRequest.getCustomallowance());
                signContract.setText(modelPRFRequest.getSigncontractdate());
                notes.setText(modelPRFRequest.getNotes());
                srfNumber.setText(modelPRFRequest.getSrfnumber());
                positionCandidate.setText(modelPRFRequest.getPosition());
                candidatesStatus.setText(modelPRFRequest.getCandidatestatus());
            }
        });
    }
}
