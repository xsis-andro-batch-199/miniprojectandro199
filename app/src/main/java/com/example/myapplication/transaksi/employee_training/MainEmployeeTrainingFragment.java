package com.example.myapplication.transaksi.employee_training;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainEmployeeTrainingFragment extends Fragment {


    public MainEmployeeTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_employee_training, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tabLayoutEmployeeTraining);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.data)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.detail_activity)));

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1 || i == 0){
                viewGroupTab.setEnabled(false);
            }
        }

        Fragment fragmentData = new DataEmployeeTrainingFragment();
        loadFragment(fragmentData);
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameEmployeeTraining, fragment);
        fragmentTransaction.commit();
    }

}
