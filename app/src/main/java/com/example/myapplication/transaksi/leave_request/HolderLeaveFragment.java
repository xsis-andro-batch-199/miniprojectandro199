package com.example.myapplication.transaksi.leave_request;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterLeavePager;
import com.google.android.material.tabs.TabLayout;

public class HolderLeaveFragment extends Fragment {

    public HolderLeaveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_holder_leave, container, false);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        final AdapterLeavePager adapterLeavePager = new AdapterLeavePager(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapterLeavePager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount ; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1){
                viewGroupTab.setEnabled(true);
            }
        }

        return view;
    }
}
