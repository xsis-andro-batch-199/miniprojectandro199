package com.example.myapplication.transaksi.timesheet;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.TimesheetDataAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelTimesheet;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataTimesheetFragment extends Fragment {
    private RecyclerView recyclerView;
    private TimesheetDataAdapter adapter;
    private List<ModelTimesheet> list = new ArrayList<>();

    private ImageView btn_tambah;

    public DataTimesheetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_timesheet, container, false);
        setupView(view);
        loadData();
        showData();
        setupButton();

        return view;
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        recyclerView = view.findViewById(R.id.rv_card);
        btn_tambah = view.findViewById(R.id.btn_tambah);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessTimesheet().findByTimesheet(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessTimesheet().findByTimesheet(s);
                showData();
                return true;
            }
        });
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessTimesheet().getViewTimesheet();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new TimesheetDataAdapter(getContext(), list);
        recyclerView.setAdapter(adapter);
    }

    private void setupButton(){
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentInput = new InputTimesheetFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentInput);
                transaction.commit();
            }
        });

    }
}
