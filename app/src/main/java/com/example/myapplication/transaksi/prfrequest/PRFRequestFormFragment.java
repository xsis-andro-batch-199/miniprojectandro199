package com.example.myapplication.transaksi.prfrequest;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFRequest;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class PRFRequestFormFragment extends Fragment {
    private EditText iDate, placement, location, period, username, telp, email, overtime, billing;
    private AutoCompleteTextView tipe, pid, notebook, bast;
    private Button btnReset, btnSubmit;
    private ModelPRFRequest modelPRFRequest;
    private ImageView tanggal;

    public PRFRequestFormFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prfrequest_form, container, false);
        setupView(view);
        inputData();
        return view;
    }

    private void setupView(View view) {
        final TextInputLayout msgErrorPlacement = (TextInputLayout) view.findViewById(R.id.msgErrorPlacement);
        final TextInputLayout msgErrorTipe = (TextInputLayout) view.findViewById(R.id.msgErrorTipe);
        final TextInputLayout msgErrorPID = (TextInputLayout) view.findViewById(R.id.msgErrorPID);
        final TextInputLayout msgErrorPeriod = (TextInputLayout) view.findViewById(R.id.msgErrorPeriod);
        final TextInputLayout msgErrorUserName = (TextInputLayout) view.findViewById(R.id.msgErrorUserName);
        final TextInputLayout msgErrorNomer = (TextInputLayout) view.findViewById(R.id.msgErrorNomer);
        final TextInputLayout msgErrorEmail = (TextInputLayout) view.findViewById(R.id.msgErrorEmail);
        final TextInputLayout msgErrorNotebook = (TextInputLayout) view.findViewById(R.id.msgErrorNotebook);
        final TextInputLayout msgErrorBast = (TextInputLayout) view.findViewById(R.id.msgErrorBast);
        iDate = view.findViewById(R.id.spinerDate);
        iDate.setHint("January 2, 2019");
        iDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        placement = view.findViewById(R.id.inputPlacement);
        placement.setHint("Placement In*");
        placement.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(placement.getText())) {
                    placement.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPlacement.setError("Required");
                } else {
                    msgErrorPlacement.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        location = view.findViewById(R.id.inputLocation);
        location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        period = view.findViewById(R.id.inputPeriod);
        period.setHint("Period *");
        period.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(period.getText())) {
                    period.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPeriod.setError("Required");
                } else {
                    msgErrorPeriod.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        username = view.findViewById(R.id.inputUserName);
        username.setHint("User Name *");
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(username.getText())) {
                    username.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorUserName.setError("Required");
                } else {
                    msgErrorUserName.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        telp = view.findViewById(R.id.inputNomer);
        telp.setHint("Tlp/Mobile Phone *");
        telp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(telp.getText())) {
                    telp.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorNomer.setError("Required");
                } else {
                    msgErrorNomer.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });

        email = view.findViewById(R.id.inputEmail);
        email.setHint("Email *");
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(email.getText())) {
                    email.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorEmail.setError("Required");
                } else {
                    msgErrorEmail.setErrorEnabled(false);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        overtime = view.findViewById(R.id.inputOverTime);
        overtime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        billing = view.findViewById(R.id.inputBilling);
        billing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });
        tipe = view.findViewById(R.id.inputTipe);
        tipe.setHint("Type *");
        String[] tipeItem = getResources().getStringArray(R.array.input_type);
        tipe.setThreshold(0);
        tipe.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, tipeItem));
        tipe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(tipe.getText())) {
                    tipe.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorTipe.setError("Required");
                } else {
                    msgErrorTipe.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });

        pid = view.findViewById(R.id.inputPID);
        pid.setHint("PID *");
        String[] pidSpinner = getResources().getStringArray(R.array.input_pid);
        pid.setThreshold(0);
        pid.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, pidSpinner));
        pid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(pid.getText())) {
                    pid.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPID.setError("Required");
                } else {
                    msgErrorPID.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });


        notebook = view.findViewById(R.id.inputNotebook);
        notebook.setHint("Notebook *");
        String[] notebookSpinner = getResources().getStringArray(R.array.input_notebook);
        notebook.setThreshold(0);
        notebook.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, notebookSpinner));
        notebook.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(notebook.getText())) {
                    notebook.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorNotebook.setError("Required");
                } else {
                    msgErrorNotebook.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });

        bast = view.findViewById(R.id.inputBast);
        bast.setHint("Bast *");
        String[] bastSpinner = getResources().getStringArray(R.array.input_bast);
        bast.setThreshold(0);
        bast.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, bastSpinner));
        bast.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(bast.getText())) {
                    bast.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorBast.setError("Required");
                } else {
                    msgErrorBast.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                resetON();

            }
        });

        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnReset = view.findViewById(R.id.btnReset);
        tanggal = view.findViewById(R.id.linearTanggal);
        final Calendar c = Calendar.getInstance();
        iDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String bulan = new DateFormatSymbols().getMonths()[month];
                        String hari = day + "";
                        iDate.setText(bulan + " " + hari + ", " + year);
                    }
                },
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });

    }

    private void resetON() {
        if (!TextUtils.isEmpty(tipe.getText())&& !TextUtils.isEmpty(placement.getText()) &&
        !TextUtils.isEmpty(pid.getText()) && !TextUtils.isEmpty(period.getText()) &&
        !TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(telp.getText()) &&
        !TextUtils.isEmpty(email.getText()) && !TextUtils.isEmpty(notebook.getText())){
            btnSubmit.setEnabled(true);
        } else {
            btnSubmit.setEnabled(false);
        }

        if (!TextUtils.isEmpty(iDate.getText()) || !TextUtils.isEmpty(placement.getText()) ||
                !TextUtils.isEmpty(location.getText()) || !TextUtils.isEmpty(username.getText()) ||
                !TextUtils.isEmpty(period.getText()) || !TextUtils.isEmpty(telp.getText()) ||
                !TextUtils.isEmpty(email.getText()) || !TextUtils.isEmpty(overtime.getText()) ||
                !TextUtils.isEmpty(billing.getText()) || !TextUtils.isEmpty(tipe.getText()) ||
                !TextUtils.isEmpty(notebook.getText()) || !TextUtils.isEmpty(bast.getText()) ||
                !TextUtils.isEmpty(pid.getText())) {
            btnReset.setEnabled(true);
            btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            btnReset.setEnabled(false);
            btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
    }

    private void inputData() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelPRFRequest = new ModelPRFRequest();
                modelPRFRequest.setTanggal(iDate.getText().toString());
                modelPRFRequest.setPlacementin(placement.getText().toString());
                modelPRFRequest.setLocation(location.getText().toString());
                modelPRFRequest.setPeriode(period.getText().toString());
                modelPRFRequest.setUsername(username.getText().toString());
                modelPRFRequest.setTlp(telp.getText().toString());
                modelPRFRequest.setEmail(email.getText().toString());
                modelPRFRequest.setOvertime(overtime.getText().toString());
                modelPRFRequest.setBilling(billing.getText().toString());
                modelPRFRequest.setTipe(tipe.getText().toString());
                modelPRFRequest.setPid(pid.getText().toString());
                modelPRFRequest.setNotebook(notebook.getText().toString());
                modelPRFRequest.setBast(bast.getText().toString());
                db.daoAccessPRFRequest().insertDataPRF(modelPRFRequest);
                Fragment mainFragmentPRF = new MainPRFRequestFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, mainFragmentPRF);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iDate.setText(null);
                placement.setText(null);
                location.setText(null);
                period.setText(null);
                username.setText(null);
                telp.setText(null);
                email.setText(null);
                overtime.setText(null);
                billing.setText(null);
                tipe.setText(null);
                pid.setText(null);
                notebook.setText(null);
                bast.setText(null);

            }
        });
    }
}
