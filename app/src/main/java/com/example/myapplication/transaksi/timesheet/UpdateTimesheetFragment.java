package com.example.myapplication.transaksi.timesheet;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.OnBackPressedFragment;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelTimesheet;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import static com.example.myapplication.AppController.db;

public class UpdateTimesheetFragment extends Fragment implements OnBackPressedFragment {
    private TextInputLayout requiredStatus, requiredClient, requiredTanggal, requiredNotes;
    private TextInputLayout tilStartWork, tilEndWork, tilOvertime, tilStartOver, tilEndOver;
    private EditText etTanggal, etTimeStartWork, etTimeEndWork, etTimeStartOver, etTimeEndOver;
    private AutoCompleteTextView actvStatus, actvClient, actvOvertime, actvNotes;
    private Button buttonSubmit, buttonReset;
    private ImageView buttonClearStatus, buttonClearClient, buttonClearDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_timesheet, container, false);
        setupView(view);
        setupTextInputLayout();
        setupAction();
        receiveData();

        return view;
    }

    private void setupView(View view) {
        actvStatus = view.findViewById(R.id.actv_status);
        actvClient = view.findViewById(R.id.actv_client);
        actvOvertime = view.findViewById(R.id.actv_overtime);
        actvNotes = view.findViewById(R.id.actv_notes);

        etTanggal = view.findViewById(R.id.et_date);
        etTimeStartWork = view.findViewById(R.id.et_start_work);
        etTimeEndWork = view.findViewById(R.id.et_end_work);
        etTimeStartOver = view.findViewById(R.id.et_start_over);
        etTimeEndOver = view.findViewById(R.id.et_end_over);

        requiredStatus = view.findViewById(R.id.required_status);
        requiredClient = view.findViewById(R.id.required_client);
        requiredTanggal = view.findViewById(R.id.required_date);
        requiredNotes = view.findViewById(R.id.required_notes);

        tilStartWork = view.findViewById(R.id.til_start_work);
        tilEndWork = view.findViewById(R.id.til_end_work);
        tilOvertime = view.findViewById(R.id.til_overtime);
        tilStartOver = view.findViewById(R.id.til_start_over);
        tilEndOver = view.findViewById(R.id.til_end_over);

        buttonSubmit = view.findViewById(R.id.btn_submit);
        buttonReset = view.findViewById(R.id.btn_reset);
        buttonClearStatus = view.findViewById(R.id.btn_clear_status);
        buttonClearClient = view.findViewById(R.id.btn_clear_client);
        buttonClearDate = view.findViewById(R.id.btn_clear_date);
    }

    private void setupTextInputLayout(){
        Bundle bundle = getArguments();
        final ModelTimesheet modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");

//        status
        actvStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredStatus.setErrorEnabled(true);
                requiredStatus.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputStatus = actvStatus.getText().toString();
                if (!inputStatus.isEmpty()){
                    requiredStatus.setErrorEnabled(false);
                    requiredStatus.setError(null);
                } else {
                    requiredStatus.setErrorEnabled(true);
                    requiredStatus.setError(getString(R.string.word_error));
                }
                if (!inputStatus.equals(modelTimesheet.getStatus())){
                    buttonClearStatus.setVisibility(View.VISIBLE);
                    buttonClearStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            actvStatus.setText(modelTimesheet.getStatus());
                        }
                    });
                } else {
                    buttonClearStatus.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupInputField();
                setupButton();
            }
        });
//        client
        actvClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredClient.setErrorEnabled(true);
                requiredClient.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputClient = actvClient.getText().toString();
                if (!inputClient.isEmpty()){
                    requiredClient.setErrorEnabled(false);
                    requiredClient.setError(null);
                } else {
                    requiredClient.setErrorEnabled(true);
                    requiredClient.setError(getString(R.string.word_error));
                }

                if (!inputClient.equals(modelTimesheet.getClient())){
                    buttonClearClient.setVisibility(View.VISIBLE);
                    buttonClearClient.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            actvClient.setText(modelTimesheet.getClient());
                        }
                    });
                } else {
                    buttonClearClient.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
//        report date
        etTimeStartWork.setEnabled(false);
        etTimeEndWork.setEnabled(false);
        etTanggal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredTanggal.setErrorEnabled(true);
                requiredTanggal.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputTanggal = etTanggal.getText().toString();
                if (!inputTanggal.isEmpty()){
                    requiredTanggal.setErrorEnabled(false);
                    requiredTanggal.setError(null);

                    etTimeStartWork.setEnabled(true);
                    etTimeEndWork.setEnabled(true);
                } else {
                    requiredTanggal.setErrorEnabled(true);
                    requiredTanggal.setError(getString(R.string.word_error));

                    etTimeStartWork.setEnabled(false);
                    etTimeEndWork.setEnabled(false);
                }

                if (!inputTanggal.equals(modelTimesheet.getReportDate())){
                    buttonClearDate.setVisibility(View.VISIBLE);
                    buttonClearDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etTanggal.setText(modelTimesheet.getReportDate());
                        }
                    });
                } else {
                    buttonClearDate.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
//        overtime
        etTimeStartOver.setEnabled(false);
        etTimeEndOver.setEnabled(false);
        actvOvertime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputOvertime = actvOvertime.getText().toString();
                if (!inputOvertime.isEmpty() && inputOvertime.equals("Yes")){
                    etTimeStartOver.setEnabled(true);
                    etTimeEndOver.setEnabled(true);
                } else {
                    etTimeStartOver.setEnabled(false);
                    etTimeEndOver.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        actvNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredNotes.setErrorEnabled(true);
                requiredNotes.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputNotes = actvNotes.getText().toString();
                if (!inputNotes.isEmpty()){
                    requiredNotes.setErrorEnabled(false);
                    requiredNotes.setError(null);
                } else {
                    requiredNotes.setErrorEnabled(true);
                    requiredNotes.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void receiveData() {
        Bundle bundle = getArguments();
        ModelTimesheet modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");

        String status = modelTimesheet.getStatus();
        String client = modelTimesheet.getClient();
        String date = modelTimesheet.getReportDate();
        String startWork = modelTimesheet.getTimeStartWork();
        String endWork = modelTimesheet.getTimeEndWork();
        String overtime = modelTimesheet.getOvertime();
        String startOver = modelTimesheet.getTimeStartOver();
        String endOver = modelTimesheet.getTimeEndOver();
        String notes = modelTimesheet.getNotes();

        actvStatus.setText(status);
        actvClient.setText(client);
        etTanggal.setText(date);
        etTimeStartWork.setText(startWork);
        etTimeEndWork.setText(endWork);
        actvOvertime.setText(overtime);
        etTimeStartOver.setText(startOver);
        etTimeEndOver.setText(endOver);
        actvNotes.setText(notes);
    }

    private void setupAction() {
//        status
        String[] statusArray = getResources().getStringArray(R.array.status_timesheet);
        ArrayAdapter<String> adapterStatus = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, statusArray);
        actvStatus.setThreshold(0);
        actvStatus.setAdapter(adapterStatus);

//        client
        String[] clientArray = getResources().getStringArray(R.array.client_timesheet);
        ArrayAdapter<String> adapterClient = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, clientArray);
        actvClient.setThreshold(0);
        actvClient.setAdapter(adapterClient);

//        report date
        final Calendar calendar = Calendar.getInstance();
        etTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        String bulan = new DateFormatSymbols().getMonths()[m];
                        String hari = d + "";
                        etTanggal.setText(bulan + " " + hari + ", " + y);
                    }
                },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });
//        jam kerja start - end
        etTimeStartWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog dialog = new TimePickerDialog(
                        getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                String jam = hour + "";
                                if (hour < 10){
                                    jam = "0" + hour;
                                }
                                String menit = minute + "";
                                if (minute < 10){
                                    menit = "0" + minute;
                                }
                                etTimeStartWork.setText(jam + "." + menit);
                            }
                        },
                        8,
                        0,
                        true
                );
                dialog.show();
            }
        });
        etTimeEndWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog dialog = new TimePickerDialog(
                        getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                String jam = hour + "";
                                if (hour < 10){
                                    jam = "0" + hour;
                                }
                                String menit = minute + "";
                                if (minute < 10){
                                    menit = "0" + minute;
                                }
                                etTimeEndWork.setText(jam + "." + menit);
                            }
                        },
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true
                );
                dialog.show();
            }
        });

//        overtime
        String[] overtimeArray = getResources().getStringArray(R.array.overtime_timesheet);
        ArrayAdapter<String> adapterOvertime = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, overtimeArray);
        actvOvertime.setThreshold(0);
        actvOvertime.setAdapter(adapterOvertime);

//        jam lembur start - end
        etTimeStartOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog dialog = new TimePickerDialog(
                        getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                String jam = hour + "";
                                if (hour < 10){
                                    jam = "0" + hour;
                                }
                                String menit = minute + "";
                                if (minute < 10){
                                    menit = "0" + minute;
                                }
                                etTimeStartOver.setText(jam + "." + menit);
                            }
                        },
                        17,
                        0,
                        true
                );
                dialog.show();
            }
        });
        etTimeEndOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog dialog = new TimePickerDialog(
                        getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                String jam = hour + "";
                                if (hour < 10){
                                    jam = "0" + hour;
                                }
                                String menit = minute + "";
                                if (minute < 10){
                                    menit = "0" + minute;
                                }
                                etTimeEndOver.setText(jam + "." + menit);
                            }
                        },
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        true
                );
                dialog.show();
            }
        });

//        notes
        String[] notesArray = getResources().getStringArray(R.array.notes_timesheet);
        ArrayAdapter<String> adapterNotes = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, notesArray);
        actvNotes.setThreshold(0);
        actvNotes.setAdapter(adapterNotes);
    }

    private void setupButton(){
        Bundle bundle = getArguments();
        final ModelTimesheet modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");

        final String inputStatus = actvStatus.getText().toString();
        final String inputClient = actvClient.getText().toString();
        final String inputTanggal = etTanggal.getText().toString();
        final String inputTimeStartWork = etTimeStartWork.getText().toString();
        final String inputTimeEndWork = etTimeEndWork.getText().toString();
        final String inputOvertime = actvOvertime.getText().toString();
        final String inputTimeStartOver = etTimeStartOver.getText().toString();
        final String inputTimeEndOver = etTimeEndOver.getText().toString();
        final String inputNotes = actvNotes.getText().toString();

//        button submit
        if ((!inputStatus.equals(modelTimesheet.getStatus())
                || !inputClient.equals(modelTimesheet.getClient())
                || !inputTanggal.equals(modelTimesheet.getReportDate())
                || !inputTimeStartWork.equals(modelTimesheet.getTimeStartWork())
                || !inputTimeEndWork.equals(modelTimesheet.getTimeEndWork())
                || !inputOvertime.equals(modelTimesheet.getOvertime())
                || !inputTimeStartOver.equals(modelTimesheet.getTimeStartOver())
                || !inputTimeEndOver.equals(modelTimesheet.getTimeEndOver())
                || !inputNotes.equals(modelTimesheet.getNotes()))
            && !inputStatus.isEmpty()
            && !inputClient.isEmpty()
            && !inputTanggal.isEmpty()
            && !inputNotes.isEmpty()){
            buttonSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
            buttonSubmit.setEnabled(true);
        } else {
            buttonSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
            buttonSubmit.setEnabled(false);
        }
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputTimesheetFragment itf = new InputTimesheetFragment();
                if (!inputTanggal.equals(modelTimesheet.getReportDate())){
                    if (itf.cekTanggal(inputTanggal)){
                        requiredTanggal.setError(getString(R.string.data_ada));
                    } else {
                        updateData(modelTimesheet);
                        Fragment fragmentHolder = new HolderTimesheetFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_content, fragmentHolder);
                        transaction.commit();
                    }
                } else {
                    updateData(modelTimesheet);
                    Fragment fragmentHolder = new HolderTimesheetFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_content, fragmentHolder);
                    transaction.commit();
                }
            }
        });

//        button reset
        if (!inputStatus.equals(modelTimesheet.getStatus())
                || !inputClient.equals(modelTimesheet.getClient())
                || !inputTanggal.equals(modelTimesheet.getReportDate())
                || !inputNotes.equals(modelTimesheet.getNotes())
                || !inputTimeStartWork.equals(modelTimesheet.getTimeStartWork())
                || !inputTimeEndWork.equals(modelTimesheet.getTimeEndWork())
                || !inputOvertime.equals(modelTimesheet.getOvertime())
                || !inputTimeStartOver.equals(modelTimesheet.getTimeStartOver())
                || !inputTimeEndOver.equals(modelTimesheet.getTimeEndOver())){
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
            buttonReset.setEnabled(true);
        } else {
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
            buttonReset.setEnabled(false);
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = getArguments();
                ModelTimesheet modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");
                actvStatus.setText(modelTimesheet.getStatus());
                actvClient.setText(modelTimesheet.getClient());
                etTanggal.setText(modelTimesheet.getReportDate());
                etTimeStartWork.setText(modelTimesheet.getTimeStartWork());
                etTimeEndWork.setText(modelTimesheet.getTimeEndWork());
                actvOvertime.setText(modelTimesheet.getOvertime());
                etTimeStartOver.setText(modelTimesheet.getTimeStartOver());
                etTimeEndOver.setText(modelTimesheet.getTimeEndOver());
                actvNotes.setText(modelTimesheet.getNotes());
            }
        });
    }

    private void setupInputField(){
        String inputStatus = actvStatus.getText().toString();
        if (!inputStatus.equals("Masuk")){
            tilStartWork.setVisibility(View.GONE);
            tilEndWork.setVisibility(View.GONE);
            tilOvertime.setVisibility(View.GONE);
            tilStartOver.setVisibility(View.GONE);
            tilEndOver.setVisibility(View.GONE);
            requiredNotes.setVisibility(View.GONE);

            etTimeStartWork.setVisibility(View.GONE);
            etTimeEndWork.setVisibility(View.GONE);
            actvOvertime.setVisibility(View.GONE);
            etTimeStartOver.setVisibility(View.GONE);
            etTimeEndOver.setVisibility(View.GONE);
            actvNotes.setVisibility(View.GONE);
            actvNotes.setText("-");
        } else {
            tilStartWork.setVisibility(View.VISIBLE);
            tilEndWork.setVisibility(View.VISIBLE);
            tilOvertime.setVisibility(View.VISIBLE);
            tilStartOver.setVisibility(View.VISIBLE);
            tilEndOver.setVisibility(View.VISIBLE);
            requiredNotes.setVisibility(View.VISIBLE);

            etTimeStartWork.setVisibility(View.VISIBLE);
            etTimeEndWork.setVisibility(View.VISIBLE);
            actvOvertime.setVisibility(View.VISIBLE);
            etTimeStartOver.setVisibility(View.VISIBLE);
            etTimeEndOver.setVisibility(View.VISIBLE);
            actvNotes.setVisibility(View.VISIBLE);
            actvNotes.setText(null);
        }
    }

    private void updateData(ModelTimesheet modelTimesheet){
        String inputStatus = actvStatus.getText().toString();
        String inputClient = actvClient.getText().toString();
        String inputTanggal = etTanggal.getText().toString();
        String inputTimeStartWork = etTimeStartWork.getText().toString();
        String inputTimeEndWork = etTimeEndWork.getText().toString();
        String inputOvertime = actvOvertime.getText().toString();
        String inputTimeStartOver = etTimeStartOver.getText().toString();
        String inputTimeEndOver = etTimeEndOver.getText().toString();
        String inputNotes = actvNotes.getText().toString();

        ModelTimesheet ubahTimesheet = new ModelTimesheet();
        ubahTimesheet.setId(modelTimesheet.getId());

        ubahTimesheet.setStatus(inputStatus);
        ubahTimesheet.setClient(inputClient);
        ubahTimesheet.setReportDate(inputTanggal);
        ubahTimesheet.setTimeStartWork(inputTimeStartWork);
        ubahTimesheet.setTimeEndWork(inputTimeEndWork);
        ubahTimesheet.setOvertime(inputOvertime);
        ubahTimesheet.setTimeStartOver(inputTimeStartOver);
        ubahTimesheet.setTimeEndOver(inputTimeEndOver);
        ubahTimesheet.setNotes(inputNotes);
        ubahTimesheet.setIs_delete(false);
        db.daoAccessTimesheet().updateTimesheet(ubahTimesheet);
    }

    @Override
    public boolean onBackPressed() {
        Fragment fragmentHolder = new HolderTimesheetFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_content, fragmentHolder);
        transaction.commit();
        return true;
    }
}
