package com.example.myapplication.transaksi.employee_training;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.google.android.material.tabs.TabLayout;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailEmployeeTrainingFragment extends Fragment {
    private TextView tvNamaPegawai, tvNamaTraining, tvDate, tvOrganizer, tvTrainingType, tvCertification;
    private ImageView btnDelete, btnEdit;


    public DetailEmployeeTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_employee_training, container, false);

        setupView(view);
        loadData();

        return view;
    }

    private void setupView(View view) {
        tvNamaPegawai = view.findViewById(R.id.tvPegawaiName);
        tvNamaTraining = view.findViewById(R.id.tvTrainingNameET);
        tvDate = view.findViewById(R.id.tvTrainingDateET);
        tvOrganizer = view.findViewById(R.id.tvOrganizerET);
        tvTrainingType = view.findViewById(R.id.tvTrainingTypeET);
        tvCertification = view.findViewById(R.id.tvCertificationTypeET);

        btnDelete = view.findViewById(R.id.btnDeleteET);
        btnEdit = view.findViewById(R.id.btnEditET);

        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutEmployeeTraining);
        tabLayout.getTabAt(1).select();

        final Bundle bundle = getArguments();
        final ModelEmployeeTraining modelEmployeeTraining = (ModelEmployeeTraining) bundle.getSerializable("data");

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelEmployeeTraining.getNama_pegawai() + " ?");
                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessEmployeeTraining().getUpdateEmployeeTraining(modelEmployeeTraining.getId());
                        Fragment fragmentData = new DataEmployeeTrainingFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.mainFrameEmployeeTraining, fragmentData);
                        fragmentTransaction.commit();
                    }
                });
                builder.show();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment ubahEmployeeTrainingFragment = new UbahEmployeeTraining();

                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("dataUbahET", modelEmployeeTraining);
                ubahEmployeeTrainingFragment.setArguments(bundle1);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_content, ubahEmployeeTrainingFragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelEmployeeTraining modelEmployeeTraining = (ModelEmployeeTraining) bundle.getSerializable("data");

        tvNamaPegawai.setText(modelEmployeeTraining.getNama_pegawai());
        tvNamaTraining.setText(modelEmployeeTraining.getTraining());
        tvDate.setText(modelEmployeeTraining.getTraining_date());
        tvOrganizer.setText(modelEmployeeTraining.getOrganizer());
        tvTrainingType.setText(modelEmployeeTraining.getTraining_type());
        tvCertification.setText(modelEmployeeTraining.getCertification_type());
    }

}
