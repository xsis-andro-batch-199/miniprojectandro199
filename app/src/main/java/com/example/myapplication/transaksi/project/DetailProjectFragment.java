package com.example.myapplication.transaksi.project;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapplication.OnBackPressed;
import com.example.myapplication.R;
import com.example.myapplication.adapter.ExpandableListDetailProjectAdapter;
import com.example.myapplication.model.ModelProject;
import com.example.myapplication.transaksi.timesheet.DataTimesheetFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailProjectFragment extends Fragment implements OnBackPressed {
    ExpandableListView expandableListView;
    ImageView btnDelete, btnEdit;
    ExpandableListDetailProjectAdapter adapter;
    List<String> listMenu = new ArrayList<>();
    HashMap<String, List<String>> listValue = new HashMap<>();
    String  dClientName ,dLocation, dDepartment, dPicName, dProjectName, dDurasi,
            dRole, dProjectPhase, dProjectDescription, dProjectTechnology, dMainTask;

    public DetailProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_project, container, false);
        receiveData();
        setupView(view);
        setupTabLayout();
        setupExpanList();
        setupButtonAction();
        return view;
    }

    private void setupButtonAction() {
        Bundle bundle = getArguments();
        final ModelProject modelProject = (ModelProject) bundle.getSerializable("data");
        //button delete
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure want to delete this project history ?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessProject().deleteViewProject(modelProject.getId());
                        Fragment fragment = new MainProjectFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.replace(R.id.frame_content, fragment);
                        transaction.commit();
                    }
                });
                builder.create();
                builder.show();
            }
        });
        //button edit
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle update = new Bundle();
                update.putSerializable("data", modelProject);
                UpdateProjectFragment upf = new UpdateProjectFragment();
                upf.setArguments(update);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.frame_content, upf);
                transaction.commit();
            }
        });
    }

    private void setupExpanList() {
        //prepare list menu
        listMenu.add("Project Iinformation");
        listMenu.add("Project Desctription");
        listMenu.add("Project Technology");
        listMenu.add("Main Task");
        //prepare data value
        List<String> value1 = new ArrayList<>();
        value1.add(dClientName);
        value1.add(dLocation);
        value1.add(dDepartment);
        value1.add(dPicName);
        value1.add(dProjectName);
        value1.add(dDurasi);
        value1.add(dRole);
        value1.add(dProjectPhase);
        List<String> value2 = new ArrayList<>();
        value2.add(dProjectDescription);
        List<String> value3 = new ArrayList<>();
        value3.add(dProjectTechnology);
        List<String> value4 = new ArrayList<>();
        value4.add(dMainTask);
        //prepare hashmap listvalue
        listValue.put(listMenu.get(0), value1);
        listValue.put(listMenu.get(1), value2);
        listValue.put(listMenu.get(2), value3);
        listValue.put(listMenu.get(3), value4);

        adapter = new ExpandableListDetailProjectAdapter(getContext(), listMenu, listValue, expandableListView);
        expandableListView.setAdapter(adapter);
    }

    private void receiveData() {
        Bundle bundle = this.getArguments();
       ModelProject modelProject = (ModelProject) bundle.getSerializable("data");

        if (bundle != null){
            dClientName = modelProject.getClient_name(); dLocation = modelProject.getLocation();
            dDepartment = modelProject.getDepartment(); dPicName = modelProject.getPic_name();
            dProjectName = modelProject.getProject_name(); dDurasi = modelProject.getStart()+" - "+modelProject.getEnd();
            dRole = modelProject.getRole(); dProjectPhase = modelProject.getProject_phase();
            dProjectDescription = modelProject.getProject_description(); dProjectTechnology = modelProject.getProject_technology();
            dMainTask= modelProject.getMain_task();
        } else {
            Toast.makeText(getContext(), "null bundle", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupTabLayout() {
        TabLayout tabLayout = getActivity().findViewById(R.id.tab_layout_project);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = tabLayout.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 0){
                viewGroupTab.setEnabled(true);
            }
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    Fragment fragment = new DataProjectFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frameLayout_project, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupView(View view) {
        expandableListView = view.findViewById(R.id.elv_detail_project);
        btnDelete = view.findViewById(R.id.btn_hapus_project);
        btnEdit = view.findViewById(R.id.btn_ubah_project);
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
        super.getActivity().onBackPressed();
    }
}
