package com.example.myapplication.transaksi.leave_request;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelLeave;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class UpdateLeaveFragment extends Fragment {

    private TextView HeaderLeave;
    private TextInputLayout requiredLeaveType, requiredLeaveName, requiredStart, requiredEnd, requiredAddress, requiredContact, requiredReason;
    private EditText etStart, etEnd, etAddress, etContact, etReason;
    private AutoCompleteTextView actvLeaveType, actvLeaveName;
    private Button buttonSubmit, buttonReset;

    public UpdateLeaveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_input_leave, container, false);
        setupView(view);
        setupTextInputLayout();
        setupAction();
        loadData();
        return view;
    }

    private void setupAction() {
        //LEAVE TYPE
        String[] leaveTypeArray = getResources().getStringArray(R.array.leave_type);
        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, leaveTypeArray);
        actvLeaveType.setThreshold(0);
        actvLeaveType.setAdapter(adapterType);
        actvLeaveType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(),(CharSequence)adapterView.getItemAtPosition(i), Toast.LENGTH_LONG).show();
            }
        });

        //LEAVE NAME
        String[] leaveNameArray = getResources().getStringArray(R.array.leave_name);
        ArrayAdapter<String> adapterName = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, leaveNameArray);
        actvLeaveName.setThreshold(0);
        actvLeaveName.setAdapter(adapterName);
        actvLeaveName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(),(CharSequence)adapterView.getItemAtPosition(i), Toast.LENGTH_LONG).show();
            }
        });

        //START DATE
        final Calendar calendarStart = Calendar.getInstance();
        etStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        String bulan = new DateFormatSymbols().getMonths()[m];
                        String hari = d + "";
                        etStart.setText(bulan + " " + hari + ", " + y);
                    }
                },
                        calendarStart.get(Calendar.YEAR),
                        calendarStart.get(Calendar.MONTH),
                        calendarStart.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });

        //END DATE
        final Calendar calendarEnd = Calendar.getInstance();
        etEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int ye, int me, int de) {
                        String bulanend = new DateFormatSymbols().getMonths()[me];
                        String hariend = de + "";
                        etEnd.setText(bulanend + " " + hariend + ", " + ye);
                    }
                },
                        calendarEnd.get(Calendar.YEAR),
                        calendarEnd.get(Calendar.MONTH),
                        calendarEnd.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });
    }

    private void setupTextInputLayout() {

        //LEAVE TYPE
        actvLeaveType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredLeaveType.setErrorEnabled(true);
                requiredLeaveType.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputLeaveType = actvLeaveType.getText().toString();
                if (!inputLeaveType.isEmpty()){
                    requiredLeaveType.setErrorEnabled(false);
                    requiredLeaveType.setError(null);
                } else {
                    requiredLeaveType.setErrorEnabled(true);
                    requiredLeaveType.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupInputField();
                setupButton();
            }
        });

        //LEAVE NAME
        actvLeaveName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredLeaveName.setErrorEnabled(true);
                requiredLeaveName.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputLeaveName = actvLeaveName.getText().toString();
                if (!inputLeaveName.isEmpty()){
                    requiredLeaveName.setErrorEnabled(false);
                    requiredLeaveName.setError(null);
                } else {
                    requiredLeaveName.setErrorEnabled(true);
                    requiredLeaveName.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        //START DATE
        etStart.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredStart.setErrorEnabled(true);
                requiredStart.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputStart = etStart.getText().toString();
                if (!inputStart.isEmpty()){
                    requiredStart.setErrorEnabled(false);
                    requiredStart.setError(null);
                } else {
                    requiredStart.setErrorEnabled(true);
                    requiredStart.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        //END DATE
        etEnd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredEnd.setErrorEnabled(true);
                requiredEnd.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputEnd = etEnd.getText().toString();
                if (!inputEnd.isEmpty()){
                    requiredEnd.setErrorEnabled(false);
                    requiredEnd.setError(null);
                } else {
                    requiredEnd.setErrorEnabled(true);
                    requiredEnd.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        //ADDRESS
        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredAddress.setErrorEnabled(true);
                requiredAddress.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputAddress = etAddress.getText().toString();
                if (!inputAddress.isEmpty()){
                    requiredAddress.setErrorEnabled(false);
                    requiredAddress.setError(null);
                } else {
                    requiredAddress.setErrorEnabled(true);
                    requiredAddress.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        //CONTACT NUMBER
        etContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredContact.setErrorEnabled(true);
                requiredContact.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputContact = etContact.getText().toString();
                if (!inputContact.isEmpty()){
                    requiredContact.setErrorEnabled(false);
                    requiredContact.setError(null);
                } else {
                    requiredContact.setErrorEnabled(true);
                    requiredContact.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        //REASON
        etReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredReason.setErrorEnabled(true);
                requiredReason.setError(getString(R.string.word_error));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputReason = etReason.getText().toString();
                if (!inputReason.isEmpty()){
                    requiredReason.setErrorEnabled(false);
                    requiredReason.setError(null);
                } else {
                    requiredReason.setErrorEnabled(true);
                    requiredReason.setError(getString(R.string.word_error));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

    }


    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelLeave modelLeave = (ModelLeave) bundle.getSerializable("data");

        if (bundle != null){
            String leaveTypeDetail  = modelLeave.getLeave_type();
            String leaveNameDetail  = modelLeave.getLeave_name();
            String startDateDetail  = modelLeave.getStartDate();
            String endDateDetail    = modelLeave.getEndDate();
            String addressDetail    = modelLeave.getAddress();
            String contactDetail    = modelLeave.getContact();
            String reasonDetail     = modelLeave.getReason();

            actvLeaveType.setText(leaveTypeDetail);
            actvLeaveName.setText(leaveNameDetail);
            etStart.setText(startDateDetail);
            etEnd.setText(endDateDetail);
            etAddress.setText(addressDetail);
            etContact.setText(contactDetail);
            etReason.setText(reasonDetail);

        } else {
            Toast.makeText(getContext(), "null bundle", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupView(View view) {
        HeaderLeave = view.findViewById(R.id.tvAddEditLeave);
        HeaderLeave.setText(getResources().getString(R.string.edit_leave));

        actvLeaveType = view.findViewById(R.id.actv_leave_type);
        actvLeaveName = view.findViewById(R.id.actv_leave_name);

        etStart = view.findViewById(R.id.start_date);
        etEnd = view.findViewById(R.id.end_date);
        etAddress = view.findViewById(R.id.address);
        etContact = view.findViewById(R.id.contact_number);
        etReason = view.findViewById(R.id.reason);

        requiredLeaveType = view.findViewById(R.id.required_leave_type);
        requiredLeaveName = view.findViewById(R.id.required_leave_name);
        requiredStart = view.findViewById(R.id.required_start);
        requiredEnd = view.findViewById(R.id.required_end);
        requiredAddress = view.findViewById(R.id.required_address);
        requiredContact = view.findViewById(R.id.required_contact);
        requiredReason = view.findViewById(R.id.required_reason);

        buttonSubmit = view.findViewById(R.id.btn_submit);
        buttonReset = view.findViewById(R.id.btn_reset);

    }

    private void setupButton() {
        final Bundle bundle = getArguments();
        final ModelLeave modelLeave = (ModelLeave) bundle.getSerializable("data");

        final String inputLeaveType = actvLeaveType.getText().toString();
        final String inputLeaveName = actvLeaveName.getText().toString();
        final String inputStart = etStart.getText().toString();
        final String inputEnd = etEnd.getText().toString();
        final String inputAddress = etAddress.getText().toString();
        final String inputContact = etContact.getText().toString();
        final String inputReason = etReason.getText().toString();

        //BUTTON SUBMIT
        if (    (!inputLeaveType.equals(modelLeave.getLeave_type())
                || !inputLeaveName.equals(modelLeave.getLeave_name())
                || !inputStart.equals(modelLeave.getStartDate())
                || !inputEnd.equals(modelLeave.getEndDate())
                || !inputAddress.equals(modelLeave.getAddress())
                || !inputContact.equals(modelLeave.getContact())
                || !inputReason.equals(modelLeave.getReason())) &&
                (!inputLeaveType.isEmpty() && !inputLeaveName.isEmpty()
                && !inputStart.isEmpty() && !inputEnd.isEmpty()
                && !inputAddress.isEmpty() && !inputContact.isEmpty()
                && !inputReason.isEmpty())){
            buttonSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
            buttonSubmit.setEnabled(true);
        } else {
            buttonSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
            buttonSubmit.setEnabled(false);
        }
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (inputLeaveType.equals(modelLeave.getLeave_type())
                    && inputStart.equals(modelLeave.getStartDate())
                    && inputEnd.equals(modelLeave.getEndDate())){
                    updateData(modelLeave);
                    Fragment fragment = new HolderLeaveFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_content, fragment);
                    transaction.commit();
                }   else {
                    if (cekData(inputLeaveType) == true
                            && cekData(inputStart) == true
                            && cekData(inputEnd) == true){
                        actvLeaveType.setError(getResources().getString(R.string.data_ada));
                        etStart.setError(getResources().getString(R.string.data_ada));
                        etEnd.setError(getResources().getString(R.string.data_ada));
                    } else {
                        updateData(modelLeave);
                        Fragment fragment = new HolderLeaveFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_content, fragment);
                        transaction.commit();
                    }
                }

                /*if (cekData(inputLeaveType) == true
                        && cekData(inputStart) == true
                        && cekData(inputEnd) == true){
                    actvLeaveType.setError(getResources().getString(R.string.data_ada));
                    etStart.setError(getResources().getString(R.string.data_ada));
                    etEnd.setError(getResources().getString(R.string.data_ada));
                } else {
                    updateData(modelLeave);
                    Fragment fragment = new HolderLeaveFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_content, fragment);
                    transaction.commit();
                }*/
            }
        });
        if (!inputLeaveType.equals(modelLeave.getLeave_type())
                || !inputLeaveName.equals(modelLeave.getLeave_name())
                || !inputStart.equals(modelLeave.getStartDate())
                || !inputEnd.equals(modelLeave.getEndDate())
                || !inputAddress.equals(modelLeave.getAddress())
                || !inputContact.equals(modelLeave.getContact())
                || !inputReason.equals(modelLeave.getReason())){
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
            buttonReset.setEnabled(true);
        } else {
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
            buttonReset.setEnabled(false);
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle1 = getArguments();
                ModelLeave modelLeave1 = (ModelLeave) bundle.getSerializable("data");
                actvLeaveType.setText(modelLeave.getLeave_type());
                actvLeaveName.setText(modelLeave.getLeave_name());
                etStart.setText(modelLeave.getStartDate());
                etEnd.setText(modelLeave.getEndDate());
                etAddress.setText(modelLeave.getAddress());
                etContact.setText(modelLeave.getContact());
                etReason.setText(modelLeave.getReason());
            }
        });

    }

    private void updateData(ModelLeave modelLeave) {
        String inputLeaveType = actvLeaveType.getText().toString();
        String inputLeaveName = actvLeaveName.getText().toString();
        String inputStart = etStart.getText().toString();
        String inputEnd = etEnd.getText().toString();
        String inputAddress = etAddress.getText().toString();
        String inputContact = etContact.getText().toString();
        String inputReason = etReason.getText().toString();

        ModelLeave updateModel = new ModelLeave();
        updateModel.setId(modelLeave.getId());
        updateModel.setLeave_type(inputLeaveType);
        updateModel.setLeave_name(inputLeaveName);
        updateModel.setStartDate(inputStart);
        updateModel.setEndDate(inputEnd);
        updateModel.setAddress(inputAddress);
        updateModel.setContact(inputContact);
        updateModel.setReason(inputReason);
        db.daoAccessLeaveRequest().updateLeave(updateModel);

    }

    private void setupInputField() {
        String inputLeaveType = actvLeaveType.getText().toString();
        if (!inputLeaveType.equals("Cuti Khusus")){
            requiredLeaveName.setVisibility(View.GONE);
            actvLeaveName.setVisibility(View.GONE);
        } else {
            requiredLeaveName.setVisibility(View.VISIBLE);
            actvLeaveName.setVisibility(View.VISIBLE);
        }
    }

    public boolean cekData(String inputText){
        List<ModelLeave> list = db.daoAccessLeaveRequest().getDBLeave();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getLeave_type())
                    || inputText.contentEquals(list.get(i).getStartDate())
                    || inputText.contentEquals(list.get(i).getEndDate())){
                return true;
            }
        }
        return false;
    }

}
