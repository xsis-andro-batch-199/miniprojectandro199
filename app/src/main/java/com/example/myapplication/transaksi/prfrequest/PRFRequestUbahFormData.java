package com.example.myapplication.transaksi.prfrequest;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFRequest;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class PRFRequestUbahFormData extends Fragment {
    private EditText iDate, placement, location, period, username, telp, email, overtime, billing;
    private AutoCompleteTextView tipe, pid, notebook, bast;
    private Button btnReset, btnSubmit;
    private ImageView tanggal;


    public PRFRequestUbahFormData() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_prfrequest_ubah_form_data, container, false);
        setupView(view);
        receiveData();
        return view;
    }

    private void receiveData() {
        Bundle bundle = this.getArguments();
        ModelPRFRequest modelPRFRequest = (ModelPRFRequest) bundle.getSerializable("data");
        if (bundle != null) {
            String uIdate = modelPRFRequest.getTanggal();
            String uPlacement = modelPRFRequest.getPlacementin();
            String uLocation = modelPRFRequest.getLocation();
            String uPeriod = modelPRFRequest.getPeriode();
            String uUsername = modelPRFRequest.getUsername();
            String uTelp = modelPRFRequest.getTlp();
            String uEmail = modelPRFRequest.getEmail();
            String uOvertime = modelPRFRequest.getOvertime();
            String uBilling = modelPRFRequest.getBilling();
            String uTipe = modelPRFRequest.getTipe();
            String uPID = modelPRFRequest.getPid();
            String uNotebook = modelPRFRequest.getNotebook();
            String uBast = modelPRFRequest.getBast();

            iDate.setText(uIdate);
            placement.setText(uPlacement);
            location.setText(uLocation);
            period.setText(uPeriod);
            username.setText(uUsername);
            telp.setText(uTelp);
            email.setText(uEmail);
            overtime.setText(uOvertime);
            billing.setText(uBilling);
            tipe.setText(uTipe);
            pid.setText(uPID);
            notebook.setText(uNotebook);
            bast.setText(uBast);

        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelPRFRequest modelPRFRequest = (ModelPRFRequest) bundle.getSerializable("data");
        final TextInputLayout msgErrorPlacement = (TextInputLayout) view.findViewById(R.id.msgErrorPlacement);
        final TextInputLayout msgErrorTipe = (TextInputLayout) view.findViewById(R.id.msgErrorTipe);
        final TextInputLayout msgErrorPID = (TextInputLayout) view.findViewById(R.id.msgErrorPID);
        final TextInputLayout msgErrorPeriod = (TextInputLayout) view.findViewById(R.id.msgErrorPeriod);
        final TextInputLayout msgErrorUserName = (TextInputLayout) view.findViewById(R.id.msgErrorUserName);
        final TextInputLayout msgErrorNomer = (TextInputLayout) view.findViewById(R.id.msgErrorNomer);
        final TextInputLayout msgErrorEmail = (TextInputLayout) view.findViewById(R.id.msgErrorEmail);
        final TextInputLayout msgErrorNotebook = (TextInputLayout) view.findViewById(R.id.msgErrorNotebook);
        final TextInputLayout msgErrorBast = (TextInputLayout) view.findViewById(R.id.msgErrorBast);
        iDate = view.findViewById(R.id.spinerDate);
        iDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputDate = iDate.getText().toString();
                if (!modelPRFRequest.getTanggal().equals(inputDate) &&
                        !iDate.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        placement = view.findViewById(R.id.inputPlacement);
        placement.setHint("Placement In*");
        placement.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(placement.getText())) {
                    placement.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPlacement.setError("Required");
                } else {
                    msgErrorPlacement.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputPlacement = placement.getText().toString();
                if (!modelPRFRequest.getPlacementin().equals(inputPlacement) &&
                        !placement.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        location = view.findViewById(R.id.inputLocation);
        location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputLocation = location.getText().toString();
                if (!modelPRFRequest.getLocation().equals(inputLocation) &&
                        !location.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        period = view.findViewById(R.id.inputPeriod);
        period.setHint("Period *");
        period.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(period.getText())) {
                    period.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPeriod.setError("Required");
                } else {
                    msgErrorPeriod.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputPeriod = period.getText().toString();
                if (!modelPRFRequest.getPeriode().equals(inputPeriod) &&
                        !period.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        username = view.findViewById(R.id.inputUserName);
        username.setHint("User Name *");
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(username.getText())) {
                    username.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorUserName.setError("Required");
                } else {
                    msgErrorUserName.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputUserName = username.getText().toString();
                if (!modelPRFRequest.getUsername().equals(inputUserName) &&
                        !username.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        telp = view.findViewById(R.id.inputNomer);
        telp.setHint("Tlp/Mobile Phone *");
        telp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(telp.getText())) {
                    telp.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorNomer.setError("Required");
                } else {
                    msgErrorNomer.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputTelp = telp.getText().toString();
                if (!modelPRFRequest.getTanggal().equals(inputTelp) &&
                        telp.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });

        email = view.findViewById(R.id.inputEmail);
        email.setHint("Email *");
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(email.getText())) {
                    email.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorEmail.setError("Required");
                } else {
                    msgErrorEmail.setErrorEnabled(false);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputEmail = email.getText().toString();
                if (!modelPRFRequest.getEmail().equals(inputEmail) &&
                        !email.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        overtime = view.findViewById(R.id.inputOverTime);
        overtime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputOverTime = overtime.getText().toString();
                if (!modelPRFRequest.getOvertime().equals(inputOverTime) &&
                        !overtime.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        billing = view.findViewById(R.id.inputBilling);
        billing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputBilling = billing.getText().toString();
                if (!modelPRFRequest.getBilling().equals(inputBilling) &&
                        !billing.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });

        tipe = view.findViewById(R.id.inputTipe);
        tipe.setHint("Type *");
        final String[] tipeItem = getResources().getStringArray(R.array.input_type);
        tipe.setThreshold(0);
        tipe.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, tipeItem));
        tipe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(tipe.getText())) {
                    tipe.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorTipe.setError("Required");
                } else {
                    msgErrorTipe.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputTipe = tipe.getText().toString();
                if (!modelPRFRequest.getTipe().equals(inputTipe) &&
                        !tipe.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });

        pid = view.findViewById(R.id.inputPID);
        pid.setHint("PID *");
        final String[] pidSpinner = getResources().getStringArray(R.array.input_pid);
        pid.setThreshold(0);
        pid.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, pidSpinner));
        pid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(pid.getText())) {
                    pid.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPID.setError("Required");
                } else {
                    msgErrorPID.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputPID = pid.getText().toString();
                if (!modelPRFRequest.getPid().equals(inputPID) &&
                        !pid.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });


        notebook = view.findViewById(R.id.inputNotebook);
        notebook.setHint("Notebook *");
        String[] notebookSpinner = getResources().getStringArray(R.array.input_notebook);
        notebook.setThreshold(0);
        notebook.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, notebookSpinner));
        notebook.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(notebook.getText())) {
                    notebook.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorNotebook.setError("Required");
                } else {
                    msgErrorNotebook.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputNotebook = notebook.getText().toString();
                if (!modelPRFRequest.getNotebook().equals(inputNotebook) &&
                        !notebook.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });

        bast = view.findViewById(R.id.inputBast);
        bast.setHint("Bast *");
        String[] bastSpinner = getResources().getStringArray(R.array.input_bast);
        bast.setThreshold(0);
        bast.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, bastSpinner));
        bast.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(bast.getText())) {
                    bast.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorBast.setError("Required");
                } else {
                    msgErrorBast.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputBast = bast.getText().toString();
                if (!modelPRFRequest.getBast().equals(inputBast) &&
                        !bast.getText().toString().isEmpty()) {
                    btnReset.setEnabled(true);
                    btnSubmit.setEnabled(true);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    inputData();
                } else {
                    btnReset.setEnabled(false);
                    btnSubmit.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });

        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnReset = view.findViewById(R.id.btnReset);
        tanggal = view.findViewById(R.id.linearTanggal);
        final Calendar c = Calendar.getInstance();
        iDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String bulan = new DateFormatSymbols().getMonths()[month];
                        String hari = day + "";
                        iDate.setText(bulan + " " + hari + ", " + year);
                    }
                },
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });

    }


    private void inputData() {
        Bundle bundle = this.getArguments();
        final ModelPRFRequest modelPRFRequest = (ModelPRFRequest) bundle.getSerializable("data");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelPRFRequest updateData = new ModelPRFRequest();
                updateData.setId(modelPRFRequest.getId());
                updateData.setTanggal(iDate.getText().toString());
                updateData.setPlacementin(placement.getText().toString());
                updateData.setLocation(location.getText().toString());
                updateData.setPeriode(period.getText().toString());
                updateData.setUsername(username.getText().toString());
                updateData.setTlp(telp.getText().toString());
                updateData.setEmail(email.getText().toString());
                updateData.setOvertime(overtime.getText().toString());
                updateData.setBilling(billing.getText().toString());
                updateData.setTipe(tipe.getText().toString());
                updateData.setPid(pid.getText().toString());
                updateData.setNotebook(notebook.getText().toString());
                updateData.setBast(bast.getText().toString());
                db.daoAccessPRFRequest().updatePRFData(updateData);
                Fragment mainFragmentPRF = new MainPRFRequestFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, mainFragmentPRF);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iDate.setText(modelPRFRequest.getTanggal());
                placement.setText(modelPRFRequest.getPlacementin());
                location.setText(modelPRFRequest.getLocation());
                period.setText(modelPRFRequest.getPeriode());
                username.setText(modelPRFRequest.getUsername());
                telp.setText(modelPRFRequest.getTlp());
                email.setText(modelPRFRequest.getEmail());
                overtime.setText(modelPRFRequest.getOvertime());
                billing.setText(modelPRFRequest.getBilling());
                tipe.setText(modelPRFRequest.getTipe());
                pid.setText(modelPRFRequest.getPid());
                notebook.setText(modelPRFRequest.getNotebook());
                bast.setText(modelPRFRequest.getBast());

            }
        });
    }
}
