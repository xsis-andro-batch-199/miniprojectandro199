package com.example.myapplication.transaksi.timesheet;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.OnBackPressedFragment;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelTimesheet;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailTimesheetFragment extends Fragment implements OnBackPressedFragment {
    private TextView date, status, client, date_time, overtime, notes;
    private ImageView buttonDelete, buttonUpdate;

    private TabLayout tabLayout;

    public DetailTimesheetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_timesheet, container, false);
        setupView(view);
        receiveData();
        setupButton();
        return view;
    }

    private void setupView(View view) {
        date = view.findViewById(R.id.detail_date);
        status = view.findViewById(R.id.detail_status);
        client = view.findViewById(R.id.detail_client);
        date_time = view.findViewById(R.id.detail_date_time);
        overtime = view.findViewById(R.id.detail_overtime);
        notes = view.findViewById(R.id.detail_notes);

        buttonDelete = view.findViewById(R.id.btn_delete);
        buttonUpdate = view.findViewById(R.id.btn_update);

        tabLayout = getActivity().findViewById(R.id.tab_layout);
    }

    private void receiveData() {
        Bundle bundle = this.getArguments();
        ModelTimesheet modelTimesheet = null;

        if (bundle != null){
            modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");
            String dStatus = modelTimesheet.getStatus();
            String dClient = modelTimesheet.getClient();
            String dDate = modelTimesheet.getReportDate();
            String dStartWork = modelTimesheet.getTimeStartWork();
            String dEndWork = modelTimesheet.getTimeEndWork();
            String dOvertime = modelTimesheet.getOvertime();
            String dNotes = modelTimesheet.getNotes();

            date.setText(dDate);
            status.setText(dStatus);
            client.setText(dClient);
            date_time.setText(dDate + " " + dStartWork + " - " + dEndWork);
            if (dOvertime == null){
                overtime.setText("-");
            } else {
                overtime.setText(dOvertime);
            }
            notes.setText(dNotes);
        }
    }

    private void setupButton(){
//        button delete
        Bundle bundle = getArguments();
        ModelTimesheet modelTimesheet = null;
        if (bundle != null) {
            modelTimesheet = (ModelTimesheet) bundle.getSerializable("data");
        }

        final ModelTimesheet finalModelTimesheet = modelTimesheet;
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderHapus = new AlertDialog.Builder(getContext());
                builderHapus.setMessage("Hapus record?");
                builderHapus.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builderHapus.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessTimesheet().deleteViewTimesheet(finalModelTimesheet.getId());
                        tabLayout.getTabAt(0).select();
                    }
                });
                builderHapus.show();
            }
        });
//        button update
        final ModelTimesheet finalModelTimesheet1 = modelTimesheet;
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateTimesheetFragment fragmentUpdate = new UpdateTimesheetFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", finalModelTimesheet1);
                fragmentUpdate.setArguments(bundle);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentUpdate);
                transaction.commit();
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        tabLayout.getTabAt(0).select();
        return true;
    }
}
