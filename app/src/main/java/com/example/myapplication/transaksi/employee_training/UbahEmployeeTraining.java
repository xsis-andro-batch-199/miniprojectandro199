package com.example.myapplication.transaksi.employee_training;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahEmployeeTraining extends Fragment {
    private AutoCompleteTextView actvNamaPegawai, actvTrainingName, actvOrganizer, actvTrainingType, actvCertification;
    private TextInputLayout til01, til02, til03, til04;
    private EditText etDate;
    private Button btnSave, btnReset;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_employee_training, container, false);
        setupView(view);
        loadData();
        return  view;
    }

    private void loadData() {
        Bundle bundle = getArguments();
        ModelEmployeeTraining modelEmployeeTraining = (ModelEmployeeTraining) bundle.getSerializable("dataUbahET");

        actvNamaPegawai.setText(modelEmployeeTraining.getNama_pegawai());
        actvTrainingName.setText(modelEmployeeTraining.getTraining());
        etDate.setText(modelEmployeeTraining.getTraining_date());
        actvOrganizer.setText(modelEmployeeTraining.getOrganizer());
        actvTrainingType.setText(modelEmployeeTraining.getTraining_type());
        actvCertification.setText(modelEmployeeTraining.getCertification_type());
    }

    private void setupView(View view) {
        actvNamaPegawai = view.findViewById(R.id.actvNamaPegawaiET);
        actvTrainingName = view.findViewById(R.id.actvTrainingNameET);
        etDate = view.findViewById(R.id.etDateET);
        actvOrganizer = view.findViewById(R.id.actvOrganizerET);
        actvTrainingType = view.findViewById(R.id.actvTrainingTypeET);
        actvCertification = view.findViewById(R.id.actvCertificationTypeET);
        btnReset = view.findViewById(R.id.btnResetET);
        btnSave = view.findViewById(R.id.btnSaveET);

        arrayData();

        actvNamaPegawai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvTrainingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvOrganizer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvTrainingType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvCertification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "btn Reset", Toast.LENGTH_SHORT).show();
                resetAllData();

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namaPegawai = actvNamaPegawai.getText().toString();
                String dateTraining = etDate.getText().toString();
                /*if (checkDataValidasi(namaPegawai, dateTraining)){
                    etDate.setError("Anda ada jadwal di tanggal tersebut");
                } else {*/
                    Bundle bundle = getArguments();
                    ModelEmployeeTraining ubahData = (ModelEmployeeTraining) bundle.getSerializable("dataUbahET");
                    /*ModelEmployeeTraining ubahData = new ModelEmployeeTraining();*/
                    ubahData.setNama_pegawai(actvNamaPegawai.getText().toString());
                    ubahData.setTraining(actvTrainingName.getText().toString());
                    ubahData.setOrganizer(actvOrganizer.getText().toString());
                    ubahData.setTraining_date(etDate.getText().toString());
                    ubahData.setTraining_type(actvTrainingType.getText().toString());
                    ubahData.setCertification_type(actvCertification.getText().toString());
                    /*ubahData.setIs_delete(false);*/

                    db.daoAccessEmployeeTraining().updateEmployeeTraining(ubahData);
                    Toast.makeText(getContext(), "Terupdate", Toast.LENGTH_SHORT).show();

                    Fragment fragmentMain =  new MainEmployeeTrainingFragment();
                    /*Fragment fragmentDetail = new DetailEmployeeTrainingFragment();*/
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_content, fragmentMain);
                    /*fragmentTransaction.replace(R.id.mainFrameEmployeeTraining, fragmentDetail);*/
                    fragmentTransaction.commit();
                /*}*/
            }
        });

        btnReset.setEnabled(false);
        btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
        btnSave.setEnabled(false);
        btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
    }

    private void arrayData() {
        /*Nama Pegawai*/
        String[] arrNamaPegawai = getResources().getStringArray(R.array.namaPegawai);
        ArrayAdapter<String> adapterarrNamaPegawai = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaPegawai);
        actvNamaPegawai.setThreshold(0);
        actvNamaPegawai.setAdapter(adapterarrNamaPegawai);

        /*Nama Training*/
        String[] arrNamaTraining = getResources().getStringArray(R.array.trainingName);
        ArrayAdapter<String> adapterarrNamaTraining = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaTraining);
        actvTrainingName.setThreshold(0);
        actvTrainingName.setAdapter(adapterarrNamaTraining);

        /*Nama Organizer*/
        String[] arrNamaOrganizer = getResources().getStringArray(R.array.organizerName);
        ArrayAdapter<String> adapterarrNamaOrganizer = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaOrganizer);
        actvOrganizer.setThreshold(0);
        actvOrganizer.setAdapter(adapterarrNamaOrganizer);

        /*Kalender*/
        final Calendar calendar = Calendar.getInstance();
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        String bulan = new DateFormatSymbols().getMonths()[m];
                        String hari = d + "";
                        etDate.setText(bulan + " " + hari + ", " + y);
                    }
                },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });

        /*Training Type*/
        String[] arrTrainingType = getResources().getStringArray(R.array.trainingType);
        ArrayAdapter<String> adapterarrTrainingType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrTrainingType);
        actvTrainingType.setThreshold(0);
        actvTrainingType.setAdapter(adapterarrTrainingType);

        /*Nama Certification*/
        String[] arrNamaCertification = getResources().getStringArray(R.array.certification);
        ArrayAdapter<String> adapterarrNamaCertification = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaCertification);
        actvCertification.setThreshold(0);
        actvCertification.setAdapter(adapterarrNamaCertification);
    }

    private boolean checkDataValidasi(String namaPegawai, String dateTraining) {
        List<ModelEmployeeTraining> list = db.daoAccessEmployeeTraining().getAllEmployeeTraining();
        for (int i = 0; i < list.size(); i++){
            if (namaPegawai.contentEquals(list.get(i).getNama_pegawai())
                    && dateTraining.contentEquals(list.get(i).getTraining_date())){
                return true;
            }
        }
        return false;
    }

    private void btnChange() {
        Bundle bundle = getArguments();
        ModelEmployeeTraining modelEmployeeTraining = (ModelEmployeeTraining) bundle.getSerializable("dataUbahET");

        /*if ((!modelEmployeeTraining.getNama_pegawai().contentEquals(actvNamaPegawai.getText().toString()) && !actvNamaPegawai.getText().toString().isEmpty())
                || (!modelEmployeeTraining.getTraining().contentEquals(actvTrainingName.getText().toString()) && !actvTrainingName.getText().toString().isEmpty())
                || (!modelEmployeeTraining.getOrganizer().contentEquals(actvOrganizer.getText().toString()) && !actvOrganizer.getText().toString().isEmpty())
                || (!modelEmployeeTraining.getTraining_date().contentEquals(etDate.getText().toString()) && !etDate.getText().toString().isEmpty())){
            btnSave.setEnabled(true);
            btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else*/ if ((!modelEmployeeTraining.getNama_pegawai().contentEquals(actvNamaPegawai.getText().toString())
                || !modelEmployeeTraining.getTraining().contentEquals(actvTrainingName.getText().toString())
                || !modelEmployeeTraining.getOrganizer().contentEquals(actvOrganizer.getText().toString())
                || !modelEmployeeTraining.getTraining_date().contentEquals(etDate.getText().toString())
                || !modelEmployeeTraining.getTraining_type().contentEquals(actvTrainingType.getText().toString())
                || !modelEmployeeTraining.getCertification_type().contentEquals(actvCertification.getText().toString()))
                && !actvNamaPegawai.getText().toString().isEmpty()
                && !actvTrainingName.getText().toString().isEmpty()
                && !actvOrganizer.getText().toString().isEmpty()
                && !etDate.getText().toString().isEmpty()){
            btnReset.setEnabled(true);
            btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
            btnSave.setEnabled(true);
            btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            btnReset.setEnabled(false);
            btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
            btnSave.setEnabled(false);
            btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
    }

    private void resetAllData() {
        Bundle bundle = getArguments();
        ModelEmployeeTraining modelEmployeeTraining = (ModelEmployeeTraining) bundle.getSerializable("dataUbahET");

        actvNamaPegawai.setText(modelEmployeeTraining.getNama_pegawai());
        actvTrainingName.setText(modelEmployeeTraining.getTraining());
        etDate.setText(modelEmployeeTraining.getTraining_date());
        actvOrganizer.setText(modelEmployeeTraining.getOrganizer());
        actvTrainingType.setText(modelEmployeeTraining.getTraining_type());
        actvCertification.setText(modelEmployeeTraining.getCertification_type());
    }
}
