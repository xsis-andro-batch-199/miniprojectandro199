package com.example.myapplication.transaksi.employee_training;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterEmployeeTraining;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataEmployeeTrainingFragment extends Fragment {
    private RecyclerView recyclerView;

    private List<ModelEmployeeTraining> list = new ArrayList<>();
    private AdapterEmployeeTraining adapterEmployeeTraining;
    private ImageView btnTambah;


    public DataEmployeeTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_employee_training, container, false);
        setupView(view);
        loadData();
        showData();

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessEmployeeTraining().findByEmployeeTraining(s);
                showData();
                return true;
            }
        });
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.rvEmployeeTraining);
        btnTambah = view.findViewById(R.id.btnTambahEmployeeTraining);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentForm = new FormEmployeeTrainingFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_content, fragmentForm);
                fragmentTransaction.commit();
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutEmployeeTraining);
        tabLayout.getTabAt(0).select();
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessEmployeeTraining().getAllEmployeeTraining();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapterEmployeeTraining = new AdapterEmployeeTraining(getContext(), list);
        recyclerView.setAdapter(adapterEmployeeTraining);
    }

}
