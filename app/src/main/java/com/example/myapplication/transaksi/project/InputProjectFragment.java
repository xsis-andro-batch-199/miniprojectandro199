package com.example.myapplication.transaksi.project;


import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.OnBackPressed;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelProject;
import com.example.myapplication.transaksi.timesheet.HolderTimesheetFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputProjectFragment extends Fragment implements OnBackPressed {
    TextView tv_title_entry_project;
    ScrollView scrollView;
    AutoCompleteTextView actClientName;
    TextInputLayout ilClientName, ilLocation, ilDepartment, ilPicName, ilProjectName, ilStar,
            ilEnd, ilRole, ilProjectPhase, ilProjectDescription, ilProjectTechnology, ilMainTask;
    EditText edtLocation, edtDepartment, edtPicName, edtProjectName, edtStart, edtEnd,
            edtRole, edtProjectPhase, edtProjectDescription, edtProjectTechnology, edtMainTask;
    Button btnReset, btnSubmit;
    int bls, tgs, ths, ble, tge, the;
    String bln = "", tgl = "", thn = "";
    Date startDate, endDate;

    ModelProject modelProject;


    public InputProjectFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_project,
                container, false);
        setupView(view);
        setupAction();

        return view;
    }

    private void setupValidation() {
        //validasi button submit
        final EditText[] arrayEt = {actClientName, edtProjectName, edtRole, edtProjectPhase,
                edtProjectDescription, edtProjectTechnology, edtMainTask};
        final TextInputLayout[] arrayIl = {ilClientName, ilProjectName, ilRole, ilProjectPhase,
                ilProjectDescription, ilProjectTechnology, ilMainTask};
        if (!actClientName.getText().toString().isEmpty() && !edtProjectName.getText().toString().isEmpty() &&
                !edtRole.getText().toString().isEmpty() && !edtProjectPhase.getText().toString().isEmpty() &&
                !edtProjectDescription.getText().toString().isEmpty() && !edtProjectTechnology.getText().toString().isEmpty() &&
                !edtMainTask.getText().toString().isEmpty()) {
            btnSubmit.setEnabled(true);
            btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < arrayEt.length; i++) {
                    if (arrayEt[i].getText().toString().isEmpty()) {
                        arrayIl[i].setErrorEnabled(true);
                        arrayIl[i].setError(getString(R.string.fill_blank));
                        arrayIl[i].requestFocus();
                    }
                }
                if (!edtStart.getText().toString().isEmpty()) {
                    if (edtEnd.getText().toString().isEmpty()) {
                        ilEnd.setError("Error. end date must be set");
                        edtEnd.requestFocus();
                    } else {
                        ilEnd.setError(null);
                        insertData();
                    }
                } else {
                    insertData();
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        });
        //validasi button reset
        if (!actClientName.getText().toString().isEmpty() || !edtLocation.getText().toString().isEmpty() ||
                !edtDepartment.getText().toString().isEmpty() || !edtPicName.getText().toString().isEmpty() ||
                !edtProjectName.getText().toString().isEmpty() || !edtStart.getText().toString().isEmpty() ||
                !edtEnd.getText().toString().isEmpty() || !edtRole.getText().toString().isEmpty() ||
                !edtProjectPhase.getText().toString().isEmpty() || !edtProjectDescription.getText().toString().isEmpty() ||
                !edtProjectTechnology.getText().toString().isEmpty() || !edtMainTask.getText().toString().isEmpty()) {
            btnReset.setEnabled(true);
            btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            btnReset.setEnabled(false);
            btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int y_pos = actClientName.getTop();
                scrollView.scrollTo(0, y_pos);
                actClientName.requestFocus();
                for (int i = 0; i < arrayEt.length; i++) {
                    arrayEt[i].setText(null);
                    arrayIl[i].setErrorEnabled(false);
                    arrayIl[i].setError(null);
                }
                edtLocation.setText(null);
                edtDepartment.setText(null);
                edtPicName.setText(null);
                edtStart.setText(null);
                edtEnd.setText(null);
                Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertData() {
        modelProject = new ModelProject();
        modelProject.setClient_name(actClientName.getText().toString());
        modelProject.setLocation(edtLocation.getText().toString());
        modelProject.setDepartment(edtDepartment.getText().toString());
        modelProject.setPic_name(edtPicName.getText().toString());
        modelProject.setProject_name(edtProjectName.getText().toString());
        modelProject.setStart(edtStart.getText().toString());
        modelProject.setEnd(edtEnd.getText().toString());
        modelProject.setRole(edtRole.getText().toString());
        modelProject.setProject_phase(edtProjectPhase.getText().toString());
        modelProject.setProject_description(edtProjectDescription.getText().toString());
        modelProject.setProject_technology(edtProjectTechnology.getText().toString());
        modelProject.setMain_task(edtMainTask.getText().toString());
        modelProject.setIs_delete(false);
        db.daoAccessProject().InsertPoject(modelProject);

        Fragment fragmentMain = new MainProjectFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.frame_content, fragmentMain);
        transaction.commit();
    }

    private void setupAction() {
        List<String> arrayClient = new ArrayList<>();
        arrayClient.add("Xsis Mitra Utama");
        arrayClient.add("XL Axiata");
        arrayClient.add("Astra International");
        ArrayAdapter<String> adapterClient = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrayClient);
        actClientName.setThreshold(0);
        actClientName.setAdapter(adapterClient);
        actClientName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        final EditText[] arrayEt = {actClientName, edtProjectName, edtRole, edtProjectPhase,
                edtProjectDescription, edtProjectTechnology, edtMainTask};
        final TextInputLayout[] arrayIl = {ilClientName, ilProjectName, ilRole, ilProjectPhase,
                ilProjectDescription, ilProjectTechnology, ilMainTask};
        for (int i = 0; i < arrayEt.length; i++) {
            final int count = i;
            arrayEt[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!arrayEt[count].hasFocus()) {
                        if (arrayEt[count].getText().toString().isEmpty()) {
                            arrayIl[count].setErrorEnabled(true);
                            arrayIl[count].setError(getString(R.string.fill_blank));
                        } else {
                        }
                    } else {
                    }
                }
            });
            arrayEt[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (arrayEt[count].getText().length() == 0) {
                        arrayIl[count].setErrorEnabled(true);
                        arrayIl[count].setError(getString(R.string.fill_blank));
                    } else {
                        arrayIl[count].setErrorEnabled(false);
                        arrayIl[count].setError(null);
                    }
                    setupValidation();
                }
            });
        }
        setDatePickerView();
    }

    private void setDatePickerView() {
        edtStart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtStart.hasFocus()) {
                    edtStart.setShowSoftInputOnFocus(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    setDateEvent();
                } else {
                }
            }
        });
        edtEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtEnd.hasFocus()) {
                    edtEnd.setShowSoftInputOnFocus(false);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    setDateEvent();
                } else {
                    ilEnd.setError(null);
                }
            }
        });
        edtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                setDateEvent();
            }
        });
        edtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                setDateEvent();
            }
        });
    }

    private void setDateEvent() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        String selectedDate = y + "/" + m + "/" + d;
                        bln = new DateFormatSymbols().getMonths()[m];
                        tgl = d + "";
                        thn = y + "";
                        if (edtStart.hasFocus()) {
                            edtStart.setText(bln + " " + tgl + ", " + thn);
                            startDate = new Date(selectedDate);
                            bls = m;
                            tgs = d;
                            ths = y;
                        } else if (edtEnd.hasFocus()) {
                            edtEnd.setText(bln + " " + tgl + ", " + thn);
                            endDate = new Date(selectedDate);
                            ble = m;
                            tge = d;
                            the = y;
                            if (!edtStart.getText().toString().isEmpty()) {
                                if (endDate.before(startDate)) {
                                    edtEnd.setText(null);
                                    ilEnd.setError("Error. end date must be set after than start date");
                                } else {
                                    ilEnd.setError(null);
                                }
                            } else if (edtStart.getText().toString().isEmpty()) {
                                edtEnd.setText(null);
                                ilEnd.setError("Error. please fill start date first");
                            } else {
                            }
                        } else {
                        }
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        if (edtStart.hasFocus()) {
            if (!edtStart.getText().toString().isEmpty()) {
                datePickerDialog.updateDate(ths, bls, tgs);
                datePickerDialog.setTitle(null);
            } else {
            }
        } else if (edtEnd.hasFocus()) {
            if (!edtEnd.getText().toString().isEmpty()) {
                datePickerDialog.updateDate(the, ble, tge);
                datePickerDialog.setTitle(null);
            } else {
            }
        }
        datePickerDialog.show();
    }

    private void setupView(View view) {
        tv_title_entry_project = view.findViewById(R.id.tv_title_entry_project);
        scrollView = view.findViewById(R.id.scrollView_project);

        ilClientName = view.findViewById(R.id.il_nama_client_project);
        ilLocation = view.findViewById(R.id.il_location_project);
        ilDepartment = view.findViewById(R.id.il_department_project);
        ilPicName = view.findViewById(R.id.il_pic_project);
        ilProjectName = view.findViewById(R.id.il_name_project);
        ilStar = view.findViewById(R.id.il_start_project);
        ilEnd = view.findViewById(R.id.il_end_project);
        ilRole = view.findViewById(R.id.il_role_project);
        ilProjectPhase = view.findViewById(R.id.il_phase_project);
        ilProjectDescription = view.findViewById(R.id.il_description_project);
        ilProjectTechnology = view.findViewById(R.id.il_technology_project);
        ilMainTask = view.findViewById(R.id.il_main_task_project);

        actClientName = view.findViewById(R.id.act_nama_client_project);
        edtLocation = view.findViewById(R.id.et_location_project);
        edtDepartment = view.findViewById(R.id.et_department_project);
        edtPicName = view.findViewById(R.id.et_pic_project);
        edtProjectName = view.findViewById(R.id.et_name_project);
        edtStart = view.findViewById(R.id.et_start_project);
        edtEnd = view.findViewById(R.id.et_end_project);
        edtRole = view.findViewById(R.id.et_role_project);
        edtProjectPhase = view.findViewById(R.id.et_phase_project);
        edtProjectDescription = view.findViewById(R.id.et_description_project);
        edtProjectTechnology = view.findViewById(R.id.et_technology_project);
        edtMainTask = view.findViewById(R.id.et_main_task_project);

        btnReset = view.findViewById(R.id.button_reset_project);
        btnSubmit = view.findViewById(R.id.button_submit_project);
    }

    @Override
    public void onBackPressed() {
        Fragment mainProject = new MainProjectFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_content, mainProject);
        transaction.commit();
        super.getActivity().onBackPressed();
    }
}
