package com.example.myapplication.transaksi.prfrequest;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.PRFRequestCandidatesAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class CandidatePRFRequestFragment extends Fragment {
    private List<ModelPRFRequestCandidates> listModel = new ArrayList<>();
    private PRFRequestCandidatesAdapter candidatesAdapter;
    private RecyclerView recyclerView;
    private ImageView btnAdd;


    public CandidatePRFRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_candidate_prfrequest, container, false);
        setupView(view);
        loadData();
        ShowData();

        return view;
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.recCandidatePRF);
        btnAdd = view.findViewById(R.id.btn_tambah);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentCandidateData = new PRFRequestFormCandidatesFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentCandidateData);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project")
                .allowMainThreadQueries().build();
        listModel = db.daoAccessPRFRequest().getAllPRFCandidates();

    }

    private void ShowData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        candidatesAdapter = new PRFRequestCandidatesAdapter(getContext(), listModel);
        recyclerView.setAdapter(candidatesAdapter);

    }

}
