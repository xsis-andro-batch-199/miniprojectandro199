package com.example.myapplication.transaksi.employee_training;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormEmployeeTrainingFragment extends Fragment {
    private AutoCompleteTextView actvNamaPegawai, actvTrainingName, actvOrganizer, actvTrainingType, actvCertification;
    private TextInputLayout til01, til02, til03, til04;
    private EditText etDate;
    private Button btnSave, btnReset;
    private ModelEmployeeTraining modelEmployeeTraining;


    public FormEmployeeTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_employee_training, container, false);
        setupView(view);
        return  view;
    }

    private void setupView(View view) {
        actvNamaPegawai = view.findViewById(R.id.actvNamaPegawaiET);
        actvTrainingName = view.findViewById(R.id.actvTrainingNameET);
        etDate = view.findViewById(R.id.etDateET);
        actvOrganizer = view.findViewById(R.id.actvOrganizerET);
        actvTrainingType = view.findViewById(R.id.actvTrainingTypeET);
        actvCertification = view.findViewById(R.id.actvCertificationTypeET);
        btnReset = view.findViewById(R.id.btnResetET);
        btnSave = view.findViewById(R.id.btnSaveET);

        arrayData();

        actvNamaPegawai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvTrainingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvOrganizer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvTrainingType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        actvCertification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAllData();

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namaPegawai = actvNamaPegawai.getText().toString();
                String dateTraining = etDate.getText().toString();
                if (checkDataValidasi(namaPegawai, dateTraining)){
                    etDate.setError("Anda ada jadwal di tanggal tersebut");
                } else {
                    modelEmployeeTraining = new ModelEmployeeTraining();
                    modelEmployeeTraining.setNama_pegawai(actvNamaPegawai.getText().toString());
                    modelEmployeeTraining.setTraining(actvTrainingName.getText().toString());
                    modelEmployeeTraining.setOrganizer(actvOrganizer.getText().toString());
                    modelEmployeeTraining.setTraining_date(etDate.getText().toString());
                    modelEmployeeTraining.setTraining_type(actvTrainingType.getText().toString());
                    modelEmployeeTraining.setCertification_type(actvCertification.getText().toString());
                    modelEmployeeTraining.setIs_delete(false);

                    db.daoAccessEmployeeTraining().insertEmployeeTraining(modelEmployeeTraining);
                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                    Fragment fragmentMain = new MainEmployeeTrainingFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_content, fragmentMain);
                    fragmentTransaction.commit();
                }
            }
        });

        btnReset.setEnabled(false);
        btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
        btnSave.setEnabled(false);
        btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
    }

    private void arrayData() {
        /*Nama Pegawai*/
        String[] arrNamaPegawai = getResources().getStringArray(R.array.namaPegawai);
        ArrayAdapter<String> adapterarrNamaPegawai = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaPegawai);
        actvNamaPegawai.setThreshold(0);
        actvNamaPegawai.setAdapter(adapterarrNamaPegawai);

        /*Nama Training*/
        String[] arrNamaTraining = getResources().getStringArray(R.array.trainingName);
        ArrayAdapter<String> adapterarrNamaTraining = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaTraining);
        actvTrainingName.setThreshold(0);
        actvTrainingName.setAdapter(adapterarrNamaTraining);

        /*Nama Organizer*/
        String[] arrNamaOrganizer = getResources().getStringArray(R.array.organizerName);
        ArrayAdapter<String> adapterarrNamaOrganizer = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaOrganizer);
        actvOrganizer.setThreshold(0);
        actvOrganizer.setAdapter(adapterarrNamaOrganizer);

        /*Kalender*/
        final Calendar calendar = Calendar.getInstance();
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        String bulan = new DateFormatSymbols().getMonths()[m];
                        String hari = d + "";
                        etDate.setText(bulan + " " + hari + ", " + y);
                    }
                },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                dialog.show();
            }
        });

        /*Training Type*/
        String[] arrTrainingType = getResources().getStringArray(R.array.trainingType);
        ArrayAdapter<String> adapterarrTrainingType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrTrainingType);
        actvTrainingType.setThreshold(0);
        actvTrainingType.setAdapter(adapterarrTrainingType);

        /*Nama Certification*/
        String[] arrNamaCertification = getResources().getStringArray(R.array.certification);
        ArrayAdapter<String> adapterarrNamaCertification = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, arrNamaCertification);
        actvCertification.setThreshold(0);
        actvCertification.setAdapter(adapterarrNamaCertification);
    }

    private boolean checkDataValidasi(String namaPegawai, String dateTraining) {
        List<ModelEmployeeTraining> list = db.daoAccessEmployeeTraining().getAllEmployeeTraining();
        for (int i = 0; i < list.size(); i++){
            if (namaPegawai.contentEquals(list.get(i).getNama_pegawai())
                && dateTraining.contentEquals(list.get(i).getTraining_date())){
                return true;
            }
        }
        return false;
    }

    private void btnChange() {
        if (!actvNamaPegawai.getText().toString().isEmpty()
            && !actvTrainingName.getText().toString().isEmpty()
            && !actvOrganizer.getText().toString().isEmpty()
            && !etDate.getText().toString().isEmpty()){
            btnSave.setEnabled(true);
            btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else if (!actvNamaPegawai.getText().toString().isEmpty()
                    || !actvTrainingName.getText().toString().isEmpty()
                    || !actvOrganizer.getText().toString().isEmpty()
                    || !etDate.getText().toString().isEmpty()
                    || !actvTrainingType.getText().toString().isEmpty()
                    || !actvCertification.getText().toString().isEmpty()){
            btnReset.setEnabled(true);
            btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            btnReset.setEnabled(false);
            btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
            btnSave.setEnabled(false);
            btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
    }

    private void resetAllData() {
        actvNamaPegawai.setText("");
        actvTrainingName.setText("");
        etDate.setText("");
        actvOrganizer.setText("");
        actvTrainingType.setText("");
        actvCertification.setText("");
    }


}
