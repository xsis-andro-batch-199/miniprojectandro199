package com.example.myapplication.transaksi.leave_request;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterLeaveData;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelLeaveData;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class LeaveDataFragment extends Fragment {
    private TextView previousQuota, regularQuota, annualColect, leaveTaken, totalRemain;
    private ImageView buttonAdd;
    ModelLeaveData modelLeaveData;

    public LeaveDataFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_leave_data, container, false);
        setupView(view);
        loadData();

        setHasOptionsMenu(true);
        return view;
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class,"xsis_project").allowMainThreadQueries().build();
        modelLeaveData = db.daoAccessLeaveRequest().getViewLeaveData();

        previousQuota.setText("2");
        regularQuota.setText("10");
        annualColect.setText("11");
        leaveTaken.setText("22");
        totalRemain.setText("2");
    }

    private void setupView(View view) {
        previousQuota = view.findViewById(R.id.previous_quota);
        regularQuota = view.findViewById(R.id.regular_quota);
        annualColect = view.findViewById(R.id.annual_collect);
        leaveTaken = view.findViewById(R.id.already_taken);
        totalRemain = view.findViewById(R.id.total_leave);

        buttonAdd = view.findViewById(R.id.btn_tambah);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentInput = new InputLeaveFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentInput);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

}