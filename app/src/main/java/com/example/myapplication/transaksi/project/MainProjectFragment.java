package com.example.myapplication.transaksi.project;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.OnBackPressed;
import com.example.myapplication.R;
import com.example.myapplication.fragment.MasterFragment;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainProjectFragment extends Fragment implements OnBackPressed {

    ImageView btnAdd;
    Fragment fragment;


    public MainProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_project,
                container,
                false);
        setupTabLayout(view);

        return view;
    }

    private void setupTabLayout(View view) {
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout_project);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.data)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.detail_activity)));
        tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1) {
                viewGroupTab.setEnabled(false);
            }
        }
        Fragment dataProject = new DataProjectFragment();
        loadFragment(dataProject);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction =
                getActivity().getSupportFragmentManager()
                        .beginTransaction();
        transaction.replace(R.id.frameLayout_project, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.getActivity().onBackPressed();
        startActivity(new Intent(getContext(), MainActivity.class));
        /*Fragment master = new MasterFragment();
        loadFragment(master);*/
    }
}
