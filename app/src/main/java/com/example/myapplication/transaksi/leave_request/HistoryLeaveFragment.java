package com.example.myapplication.transaksi.leave_request;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterLeaveHistory;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelLeave;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class HistoryLeaveFragment extends Fragment {
    private RecyclerView recyclerView;
    private AdapterLeaveHistory adapterLeaveHistory;
    private List<ModelLeave> list = new ArrayList<>();
    private ImageView btn_tambah;

    public HistoryLeaveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_leave, container, false);
        setupView(view);
        loadData();
        showData();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessLeaveRequest().findByDate(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessLeaveRequest().findByDate(s);
                showData();
                return true;
            }
        });
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapterLeaveHistory = new AdapterLeaveHistory(getContext(), list);
        recyclerView.setAdapter(adapterLeaveHistory);
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessLeaveRequest().getDBLeave();
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.rv_card);

        btn_tambah = view.findViewById(R.id.btn_tambah);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentInput = new InputLeaveFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentInput);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_leave, container, false);
        setupView(view);
        loadData();
        showData();

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessLeaveRequest().findByDate(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessLeaveRequest().findByDate(s);
                showData();
                return true;
            }
        });
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapterLeaveHistory = new AdapterLeaveHistory(getContext(), list);
        recyclerView.setAdapter(adapterLeaveHistory);
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessLeaveRequest().getDBLeave();
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.rv_card);

        btn_tambah = view.findViewById(R.id.btn_tambah);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentInput = new InputLeaveFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentInput);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    private void setTabLayout() {
        TabLayout tabLayout = getActivity().findViewById(R.id.tab_layout);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 0){
                viewGroupTab.setEnabled(true);
            } else {

            }
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    Fragment fragment = new HistoryLeaveFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_leave, fragment);
                    transaction.commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.getTabAt(0).select();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTabLayout();
    }*/
}
