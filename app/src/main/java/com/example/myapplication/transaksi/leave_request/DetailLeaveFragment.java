package com.example.myapplication.transaksi.leave_request;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelLeave;

import static com.example.myapplication.AppController.db;

public class DetailLeaveFragment extends Fragment {
    private TextView leaveType, leaveName, startDate, endDate, address, contact, reason;
    private ImageView buttonUpdate, buttonDelete;

    public DetailLeaveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_leave, container, false);
        setupView(view);
        receiveData();
        setupButton();
        return view;
    }

    private void setupButton() {
        Bundle bundle = getArguments();
        final ModelLeave modelLeave = (ModelLeave) bundle.getSerializable("data");

        //BUTTON UPDATE
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateLeaveFragment updateLeaveFragment = new UpdateLeaveFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelLeave);
                updateLeaveFragment.setArguments(bundle);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content,updateLeaveFragment);
                transaction.commit();
            }
        });

        //BUTTON DELETE
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus request?");
                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessLeaveRequest().deleteLeave(modelLeave.getId());
                        Fragment fragment = new HolderLeaveFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_content, fragment);
                        transaction.commit();
                    }
                });
                builder.show();
            }
        });
    }

    private void receiveData() {
        Bundle bundle           = this.getArguments();
        ModelLeave modelLeave   = (ModelLeave) bundle.getSerializable("data");

        if (bundle != null){
            String leaveTypeDetail  = modelLeave.getLeave_type();
            String leaveNameDetail  = modelLeave.getLeave_name();
            String startDateDetail  = modelLeave.getStartDate();
            String endDateDetail    = modelLeave.getEndDate();
            String addressDetail    = modelLeave.getAddress();
            String contactDetail    = modelLeave.getContact();
            String reasonDetail     = modelLeave.getReason();

            leaveType.setText(leaveTypeDetail);
            leaveName.setText(leaveNameDetail);
            startDate.setText(startDateDetail);
            endDate.setText(endDateDetail);
            address.setText(addressDetail);
            contact.setText(contactDetail);
            reason.setText(reasonDetail);

        } else {
            Toast.makeText(getContext(), "null bundle", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupView(View view) {
        leaveType = view.findViewById(R.id.detail_leave_type);
        leaveName = view.findViewById(R.id.detail_leave_name);
        startDate = view.findViewById(R.id.detail_start);
        endDate = view.findViewById(R.id.detail_end);
        address = view.findViewById(R.id.detail_address);
        contact = view.findViewById(R.id.detail_contact);
        reason = view.findViewById(R.id.detail_reason);
        buttonUpdate = view.findViewById(R.id.btn_update);
        buttonDelete = view.findViewById(R.id.btn_delete);
    }

}
