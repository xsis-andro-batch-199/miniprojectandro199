package com.example.myapplication.transaksi.leave_request;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterLeavePager2;
import com.example.myapplication.model.ModelLeave;
import com.google.android.material.tabs.TabLayout;

public class HolderLeaveFragment_2 extends Fragment {


    public HolderLeaveFragment_2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_holder_leave_fragment_2, container, false);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        final AdapterLeavePager2 adapterLeavePager2 = new AdapterLeavePager2(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapterLeavePager2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount ; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1){
                viewGroupTab.setEnabled(true);
            }
        }

        receiveData();
        kirimData();
        return view;
    }

    private void kirimData() {
        final ModelLeave modelLeave = new ModelLeave();
        DetailLeaveFragment detailLeaveFragment = new DetailLeaveFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", modelLeave);
        detailLeaveFragment.setArguments(bundle);
    }

    private void receiveData() {
        Bundle bundle           = this.getArguments();
        ModelLeave modelLeave   = (ModelLeave) bundle.getSerializable("data");
    }

}
