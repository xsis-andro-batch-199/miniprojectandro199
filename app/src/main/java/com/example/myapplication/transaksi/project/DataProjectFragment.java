package com.example.myapplication.transaksi.project;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.OnBackPressed;
import com.example.myapplication.R;
import com.example.myapplication.adapter.ProjectDataAdapter;
import com.example.myapplication.adapter.TimesheetDataAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.fragment.MasterFragment;
import com.example.myapplication.model.ModelProject;
import com.example.myapplication.transaksi.timesheet.DataTimesheetFragment;
import com.example.myapplication.transaksi.timesheet.InputTimesheetFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataProjectFragment extends Fragment implements OnBackPressed {
    private RecyclerView recyclerView;
    private ProjectDataAdapter adapter;
    private List<ModelProject> list = new ArrayList<>();

    private ImageView btn_tambah;

    public DataProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_project, container, false);
        setupView(view);
        loadData();
        showData();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessProject().findByClientName(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessProject().findByClientName(s);
                showData();
                searchView.clearFocus();
                return true;
            }
        });
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new ProjectDataAdapter(getContext(), list);
        recyclerView.setAdapter(adapter);
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessProject().getViewProject();
    }

    private void setupView(View view) {
        recyclerView = view.findViewById(R.id.recyclerview_project);
        btn_tambah = view.findViewById(R.id.btn_input_project);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentInput = new InputProjectFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, fragmentInput);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    private void setupTabLayout() {
        TabLayout tabLayout = getActivity().findViewById(R.id.tab_layout_project);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 0){
                viewGroupTab.setEnabled(true);
            }
        }
        //noinspection deprecation
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    Fragment fragment = new DataProjectFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frameLayout_project, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        /*tabLayout.getTabAt(0).select();*/
    }

    @Override
    public void onResume() {
        super.onResume();
        setupTabLayout();
    }

    @Override
    public void onBackPressed() {
        super.getActivity().onBackPressed();
        startActivity(new Intent(getContext(), MainActivity.class));
    }
}

