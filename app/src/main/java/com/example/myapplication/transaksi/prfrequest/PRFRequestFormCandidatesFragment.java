package com.example.myapplication.transaksi.prfrequest;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;
import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class PRFRequestFormCandidatesFragment extends Fragment {
    private EditText bootcamp, placementDate, customAllowance, signContract, notes;
    private AutoCompleteTextView name, positionCandidate, srfNumber, candidatesStatus;
    private Button submit, reset;
    private ModelPRFRequestCandidates modelPRFRequest;



    public PRFRequestFormCandidatesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prfrequest_form_candidates, container, false);

        setupView(view);
        inputData();
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Candidate");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    private void setupView(View view) {
        final TextInputLayout msgErrorName = (TextInputLayout) view.findViewById(R.id.msgErrorNama);
        final TextInputLayout msgErrorPositionCandidate = (TextInputLayout) view.findViewById(R.id.msgErrorPositionCandidate);
        final TextInputLayout msgErrorPlacementDate = (TextInputLayout) view.findViewById(R.id.msgErrorPlacementDate);
        final TextInputLayout msgErrorSRFnumber = (TextInputLayout) view.findViewById(R.id.msgErrorSRFNumber);
        final TextInputLayout msgErrorCandidateStatus = (TextInputLayout) view.findViewById(R.id.msgErrorCandidateStatus);

        bootcamp = view.findViewById(R.id.inputBatch);
        bootcamp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();

            }
        });
        placementDate = view.findViewById(R.id.inputPlacementDate);
        placementDate.setHint("Placement Date *");
        placementDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(placementDate.getText())) {
                    placementDate.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPlacementDate.setError("Required");
                } else {
                    msgErrorPlacementDate.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });
        customAllowance = view.findViewById(R.id.inputCustom);
        customAllowance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });
        signContract = view.findViewById(R.id.inputSignContract);
        signContract.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();

            }
        });
        notes = view.findViewById(R.id.inputNotes);
        notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();

            }
        });

        name = view.findViewById(R.id.inputNamaCandidate);
        name.setHint("Name *");
        name.setThreshold(0);
        String[] nameSpinner = getResources().getStringArray(R.array.input_name);
        name.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, nameSpinner));
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(name.getText())) {
                    name.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorName.setError("Required");
                } else {
                    msgErrorName.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });


        srfNumber = view.findViewById(R.id.inputSRFNumber);
        srfNumber.setHint("SRF Number *");
        srfNumber.setThreshold(0);
        String[] srfSpinner = getResources().getStringArray(R.array.SRF_Number);
        srfNumber.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, srfSpinner));
        srfNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                buttonsetup();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(srfNumber.getText())) {
                    srfNumber.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorSRFnumber.setError("Required");
                } else {
                    msgErrorSRFnumber.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });

        positionCandidate = view.findViewById(R.id.inputPositionCandidate);
        positionCandidate.setHint("Position *");
        positionCandidate.setThreshold(0);
        String[] positionCandidateSpinner = getResources().getStringArray(R.array.position);
        positionCandidate.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, positionCandidateSpinner));
        positionCandidate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(positionCandidate.getText())) {
                    positionCandidate.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorPositionCandidate.setError("Required");
                } else {
                    msgErrorPositionCandidate.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });

        candidatesStatus = view.findViewById(R.id.inputCandidateStatus);
        candidatesStatus.setHint("Candidate Status *");
        candidatesStatus.setThreshold(0);
        String[] candidateStatusSpinner = getResources().getStringArray(R.array.status);
        candidatesStatus.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, candidateStatusSpinner));
        candidatesStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(candidatesStatus.getText())) {
                    candidatesStatus.setHintTextColor(getResources().getColor(R.color.orens));
                    msgErrorCandidateStatus.setError("Required");
                } else {
                    msgErrorCandidateStatus.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonsetup();
            }
        });

        reset = view.findViewById(R.id.btnReset);
        submit = view.findViewById(R.id.btnSubmit);

    }

    private void buttonsetup() {
        final String inputNama = name.getText().toString();
        final String inputPlacement = placementDate.getText().toString();
        final String inputPosition = positionCandidate.getText().toString();
        final String inputSRF = srfNumber.getText().toString();
        final String inputCandidateStatus = candidatesStatus.getText().toString();
        final String inputBootcamp = bootcamp.getText().toString();
        final String inputCustomAllowance = customAllowance.getText().toString();
        final String inputSignContract = signContract.getText().toString();
        final String inputNotes = notes.getText().toString();

        if (!inputNama.isEmpty() && !inputPlacement.isEmpty() && !inputPosition.isEmpty() &&
                !inputSRF.isEmpty() && !inputCandidateStatus.isEmpty()) {
            submit.setEnabled(true);
        } else {
            submit.setEnabled(false);
        }

        if (!inputNama.isEmpty() || !inputPlacement.isEmpty() || !inputPosition.isEmpty() ||
                !inputSRF.isEmpty() || !inputCandidateStatus.isEmpty() || !inputBootcamp.isEmpty() ||
                !inputCustomAllowance.isEmpty() || !inputSignContract.isEmpty() ||
                !inputNotes.isEmpty()) {
            reset.setEnabled(true);
            reset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            reset.setEnabled(false);
            reset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
    }

    private void inputData() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelPRFRequest = new ModelPRFRequestCandidates();
                modelPRFRequest.setNama(name.getText().toString());
                modelPRFRequest.setBatchBootcamp(bootcamp.getText().toString());
                modelPRFRequest.setPlacementdate(placementDate.getText().toString());
                modelPRFRequest.setCustomallowance(customAllowance.getText().toString());
                modelPRFRequest.setSigncontractdate(signContract.getText().toString());
                modelPRFRequest.setNotes(notes.getText().toString());
                modelPRFRequest.setSrfnumber(srfNumber.getText().toString());
                modelPRFRequest.setPosition(positionCandidate.getText().toString());
                modelPRFRequest.setCandidatestatus(candidatesStatus.getText().toString());
                db.daoAccessPRFRequest().insertDataPRFCandidates(modelPRFRequest);
                Fragment MainFragmentPRF = new MainPRFRequestFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, MainFragmentPRF);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name.setText(null);
                bootcamp.setText(null);
                placementDate.setText(null);
                customAllowance.setText(null);
                signContract.setText(null);
                notes.setText(null);
                srfNumber.setText(null);
                positionCandidate.setText(null);
                candidatesStatus.setText(null);
            }
        });
    }
}
