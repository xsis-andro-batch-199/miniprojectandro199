package com.example.myapplication.master.company;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelCompany;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import static com.example.myapplication.AppController.db;

public class UpdateCompanyFragment extends Fragment {
    private TextView textTitle;
    private EditText etCompany, etCity, etPostalCode, etStreet, etBuilding, etFloor;
    private TextInputLayout tilCompany, tilCity, tilPostalCode, tilStreet, tilBuilding, tilFloor;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;
    private ImageView buttonDelete;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_company, container, false);
        setupView(view);
        receiveData();
        setupAction();
        setupButton();
        return view;
    }

    private void setupView(View view) {
        textTitle   = view.findViewById(R.id.tv_title);
        textTitle.setText("Edit Company");

        etCompany   = view.findViewById(R.id.et_company);
        etCity      = view.findViewById(R.id.et_city);
        etPostalCode= view.findViewById(R.id.et_postalcode);
        etStreet    = view.findViewById(R.id.et_street);
        etBuilding  = view.findViewById(R.id.et_building);
        etFloor     = view.findViewById(R.id.et_floor);

        buttonReset   = view.findViewById(R.id.btn_reset);
        buttonSave    = view.findViewById(R.id.btn_save);
        buttonDelete  = view.findViewById(R.id.btn_delete);
        buttonDelete.setVisibility(View.VISIBLE);

        tilCompany   = view.findViewById(R.id.til_company);
        tilCity      = view.findViewById(R.id.til_city);
        tilPostalCode= view.findViewById(R.id.til_postalcode);
        tilStreet    = view.findViewById(R.id.til_street);
        tilBuilding  = view.findViewById(R.id.til_building);
        tilFloor     = view.findViewById(R.id.til_floor);

        tabLayout = getActivity().findViewById(R.id.tab_layout_company);
    }

    private void receiveData(){
        Bundle bundle = this.getArguments();
        ModelCompany modelCompany = (ModelCompany) bundle.getSerializable("data");

        etCompany.setText(modelCompany.getCompany());
        etCity.setText(modelCompany.getCity());
        etPostalCode.setText(modelCompany.getPostalCode());
        etStreet.setText(modelCompany.getStreet());
        etBuilding.setText(modelCompany.getBuilding());
        etFloor.setText(modelCompany.getFloor());
    }

    private void setupAction(){
        Bundle bundle = this.getArguments();
        final ModelCompany modelCompany = (ModelCompany) bundle.getSerializable("data");

        etCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCompany.setText(modelCompany.getCompany());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCompany.setText(modelCompany.getCompany());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCity.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCity.setText(modelCompany.getCity());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCity.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCity.setText(modelCompany.getCity());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etPostalCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        etStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        etBuilding.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        etFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton(){
        Bundle bundle = this.getArguments();
        final ModelCompany modelCompany = (ModelCompany) bundle.getSerializable("data");

        final String inputCompany   = etCompany.getText().toString();
        final String inputCity      = etCity.getText().toString();
        final String inputPostalCode= etPostalCode.getText().toString();
        final String inputStreet    = etStreet.getText().toString();
        final String inputBuilding  = etBuilding.getText().toString();
        final String inputFloor     = etFloor.getText().toString();

        if ((!inputCompany.equals(modelCompany.getCompany())
            || !inputCity.equals(modelCompany.getCity())
            || !inputPostalCode.equals(modelCompany.getPostalCode())
            || !inputStreet.equals(modelCompany.getStreet())
            || !inputBuilding.equals(modelCompany.getBuilding())
            || !inputFloor.equals(modelCompany.getFloor()))
                && !inputCompany.isEmpty()
                && !inputCity.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FormCompanyFragment fcf = new FormCompanyFragment();

                if (inputCompany.equals(modelCompany.getCompany()) && !inputCity.equals(modelCompany.getCity())){
                    updateData(modelCompany);
                    tabLayout.getTabAt(0).select();
                } else if (!inputCompany.equals(modelCompany.getCompany())){
                    if (fcf.cekCompany(inputCompany)){
                        tilCompany.setError(getString(R.string.data_ada));
                    } else {
                        updateData(modelCompany);
                        tabLayout.getTabAt(0).select();
                    }
                } else {
                    updateData(modelCompany);
                    tabLayout.getTabAt(0).select();
                }
            }
        });

        if (!inputCompany.equals(modelCompany.getCompany())
                || !inputCity.equals(modelCompany.getCity())
                || !inputPostalCode.equals(modelCompany.getPostalCode())
                || !inputStreet.equals(modelCompany.getStreet())
                || !inputBuilding.equals(modelCompany.getBuilding())
                || !inputFloor.equals(modelCompany.getFloor())){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCompany.setText(modelCompany.getCompany());
                etCity.setText(modelCompany.getCity());
                etPostalCode.setText(modelCompany.getPostalCode());
                etStreet.setText(modelCompany.getStreet());
                etBuilding.setText(modelCompany.getBuilding());
                etFloor.setText(modelCompany.getFloor());
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus record?");
                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessCompany().deleteViewCompany(modelCompany.getId());
                        tabLayout.getTabAt(0).select();
                    }
                });
                builder.show();
            }
        });
    }

    private void updateData(ModelCompany modelCompany){
        String inputCompany   = etCompany.getText().toString();
        String inputCity      = etCity.getText().toString();
        String inputPostalCode= etPostalCode.getText().toString();
        String inputStreet    = etStreet.getText().toString();
        String inputBuilding  = etBuilding.getText().toString();
        String inputFloor     = etFloor.getText().toString();

        ModelCompany ubahModelCompany = new ModelCompany();
        ubahModelCompany.setId(modelCompany.getId());

        ubahModelCompany.setCompany(inputCompany);
        ubahModelCompany.setCity(inputCity);
        ubahModelCompany.setPostalCode(inputPostalCode);
        ubahModelCompany.setStreet(inputStreet);
        ubahModelCompany.setBuilding(inputBuilding);
        ubahModelCompany.setFloor(inputFloor);
        ubahModelCompany.setDelete(false);
        db.daoAccessCompany().updateCompany(ubahModelCompany);
    }
}
