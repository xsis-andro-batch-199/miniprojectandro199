package com.example.myapplication.master.pendidikan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPendidikan;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputPendidikanActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText et_pendidikan, et_deskripsi;
    private Button btn_batal, btn_simpan;
    private TextInputLayout pesan_error;

    private ModelPendidikan modelPendidikan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_pendidikan);
        setupView();
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.jenjang_pendidikan));
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupView() {
        et_pendidikan = findViewById(R.id.et_pendidikan);
        et_deskripsi = findViewById(R.id.et_deskripsi);

        btn_batal = findViewById(R.id.btn_batal);
        btn_batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputPendidikanActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), JenjangPendidikanActivity.class));
                finish();
            }
        });

        btn_simpan = findViewById(R.id.btn_simpan);

        pesan_error = findViewById(R.id.pesan_error);

        et_pendidikan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesan_error.setErrorEnabled(true);
                pesan_error.setError(getResources().getString(R.string.error_pendidikan));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = et_pendidikan.getText().toString();
                if (!inputText.isEmpty()){
                    pesan_error.setErrorEnabled(false);
                    pesan_error.setError(null);
                } else {
                    pesan_error.setErrorEnabled(true);
                    pesan_error.setError(getResources().getString(R.string.error_pendidikan));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = et_pendidikan.getText().toString();
                if(inputText.length() > 0) {
                    btn_simpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btn_simpan.setEnabled(true);
                    btn_simpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!et_pendidikan.getText().toString().isEmpty()){
                                if (cekNama(inputText)){
                                    pesan_error.setError(getString(R.string.data_ada));
                                } else {
                                    modelPendidikan = new ModelPendidikan();
                                    modelPendidikan.setPendidikan(et_pendidikan.getText().toString());
                                    modelPendidikan.setDeskripsi(et_deskripsi.getText().toString());
                                    modelPendidikan.setIs_delete(false);

                                    //mengisi db dengan data
                                    db.daoAccess().insertPendidikan(modelPendidikan);
                                    Toast.makeText(InputPendidikanActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), JenjangPendidikanActivity.class));
                                    finish();
                                }
                            } else {
                                //Toast.makeText(InputPendidikanActivity.this, getString(R.string.lengkapi_data), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btn_simpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btn_simpan.setEnabled(false);
                }
            }
        });
    }

    private boolean cekNama(String inputText) {
        List<ModelPendidikan> list = db.daoAccess().getAllPendidikanNoCondition();
        for (int i=0; i<list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPendidikan())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        super.onBackPressed();
    }
}
