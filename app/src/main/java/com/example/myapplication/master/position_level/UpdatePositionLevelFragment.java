package com.example.myapplication.master.position_level;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPositionLevel;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UpdatePositionLevelFragment extends Fragment {

    TextView HeaderUpdatePosition;
    EditText etPositionName, etNote;
    Button btnReset, btnSave;
    TextInputLayout requiredPosition;
    ImageView btnDelete;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_position_level, container, false);
        setupView(view);
        loadData();
        return view;
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelPositionLevel modelPositionLevel = (ModelPositionLevel) bundle.getSerializable("data");

        if (bundle != null){
            String namePosition = modelPositionLevel.getPosition_name();
            String notePosition = modelPositionLevel.getNotes();
            etPositionName.setText(namePosition);
            etNote.setText(notePosition);
        } else {

        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelPositionLevel modelPositionLevel = (ModelPositionLevel) bundle.getSerializable("data");
        HeaderUpdatePosition = view.findViewById(R.id.tvAddEditPositionLevel);
        HeaderUpdatePosition.setText(getResources().getString(R.string.edit_position));
        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelPositionLevel.getPosition_name()+" ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessPositionLevel().deletePositionLevel(modelPositionLevel.getId());

                        Toast.makeText(getContext(), "Data terhapus", Toast.LENGTH_SHORT).show();
                        Fragment fragment = new DataPositionLevelFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frameMainPositionLevel, fragment);
                        transaction.commit();
                    }
                });
                builder.show();
            }
        });
        requiredPosition = view.findViewById(R.id.required_position_level);
        btnReset = view.findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPositionName.setText(modelPositionLevel.getPosition_name());
                etNote.setText(modelPositionLevel.getNotes());
            }
        });
        btnSave = view.findViewById(R.id.btnSave);
        etPositionName = view.findViewById(R.id.etAddEditPosition);
        etPositionName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etPositionName.getText().toString();
                if (!modelPositionLevel.getPosition_name().equals(inputText)
                    && !etPositionName.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText)){
                                etPositionName.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelPositionLevel updateModelPositionLevel = new ModelPositionLevel();
                                updateModelPositionLevel.setId(modelPositionLevel.getId());
                                updateModelPositionLevel.setPosition_name(etPositionName.getText().toString());
                                updateModelPositionLevel.setNotes(etNote.getText().toString());

                                db.daoAccessPositionLevel().updatePositionLevel(updateModelPositionLevel);
                                Fragment fragment = new DataPositionLevelFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.frameMainPositionLevel, fragment);
                                transaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        etNote = view.findViewById(R.id.etNotesPositon);
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNote.getText().toString();
                if (!modelPositionLevel.getNotes().equals(inputText)
                        && !etPositionName.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText)){
                                etPositionName.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelPositionLevel updateModelPositionLevel = new ModelPositionLevel();
                                updateModelPositionLevel.setId(modelPositionLevel.getId());
                                updateModelPositionLevel.setPosition_name(etPositionName.getText().toString());
                                updateModelPositionLevel.setNotes(etNote.getText().toString());

                                db.daoAccessPositionLevel().updatePositionLevel(updateModelPositionLevel);
                                Fragment fragmentData = new DataPositionLevelFragment();
                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frameMainPositionLevel, fragmentData);
                                fragmentTransaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutPositionLevel);
        tabLayout.getTabAt(1).select();

    }

    public boolean cekData(String inputText){
        List<ModelPositionLevel> list = db.daoAccessPositionLevel().getDBPositionLevel();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPosition_name())){
                return true;
            }
        }
        return false;
    }

}
