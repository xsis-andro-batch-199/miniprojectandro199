package com.example.myapplication.master.tipe_identitas;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.SearchManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterTipeIdentitas;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.google.android.material.navigation.NavigationView;
import android.app.SearchManager;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class TipeIdentitasActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView btn_tambah;

    private List<ModelTipeIdentitas> list = new ArrayList<>();
    private AdapterTipeIdentitas adapterTipeIdentitas;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipe_identitas);

        setupToolbar();
        setupView();
        loadData();
        showData();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        /*toolbar.setOverflowIcon(getDrawable(R.drawable.ic_more_vert_white_24dp));*/
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout_identitas);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.VERTICAL,
                false));
        adapterTipeIdentitas = new AdapterTipeIdentitas(this,list);
        recyclerView.setAdapter(adapterTipeIdentitas);
    }

    private void loadData() {
        db = Room.databaseBuilder(this, Database.class, "xsis_project")
                .allowMainThreadQueries()
                .build();
        list = db.daoAccess().getAllTipeIdentitas();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.recyclerview_identitas);

        btn_tambah = findViewById(R.id.button_tambah_identitas);
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InputTipeIdentitasActivity.class));
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Cari tipe identitas");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                db = Room.databaseBuilder(getApplicationContext(),
                        Database.class,
                        "xsis_project")
                        .allowMainThreadQueries()
                        .build();
                list = db.daoAccess().findByIdentitas(query);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                db = Room.databaseBuilder(getApplicationContext(),
                        Database.class,
                        "xsis_project")
                        .allowMainThreadQueries()
                        .build();
                list = db.daoAccess().findByIdentitas(newText);
                showData();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_transaksi) {
            //Bersiap untuk interaksi antar fragment dengan main acyivity
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            // memulai transaction fragment manager
            MasterFragment masterFragment = new MasterFragment();
            //membuat object fragment pertama
            transaction.replace(R.id.frame_content, masterFragment);
            //menambah fragment
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //transaction.addToBackStack("Welcome Fragment");
            //dapat menyimpan fragment kedalam state, ketika tombol back diklik
            transaction.commit();
            //mengeksekusi fragment transaction
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_identitas);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

}
