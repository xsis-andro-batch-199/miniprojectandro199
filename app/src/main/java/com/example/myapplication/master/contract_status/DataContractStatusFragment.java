package com.example.myapplication.master.contract_status;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterContractStatus;
import com.example.myapplication.adapter.EmployeeStatusAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelContractStatus;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataContractStatusFragment extends Fragment {
    private List<ModelContractStatus> listModel = new ArrayList<>();
    private AdapterContractStatus contractStatus;
    private RecyclerView recyclerView;
    private ImageView btnAdd;


    public DataContractStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_contract_status, container, false);
        SetupView(view);
        loadData();
        showData();
        setHasOptionsMenu(true);
        return view;

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project")
                .allowMainThreadQueries().build();
                listModel = db.daoContractStatus().findContractStatus(s);
                showData();
                search.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project")
                        .allowMainThreadQueries().build();
                listModel = db.daoContractStatus().findContractStatus(s);
                showData();
                return true;
            }
        });
    }

    private void SetupView(View view) {
        recyclerView = view.findViewById(R.id.recDataContractStatus);
        btnAdd = view.findViewById(R.id.btn_tambah);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentForm = new FormContractStatusFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.mainContractStatus, fragmentForm);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutContractStatus);
        tabLayout.getTabAt(0).select();
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project")
                .allowMainThreadQueries().build();
        listModel = db.daoContractStatus().getAllContractStatus();

    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        contractStatus = new AdapterContractStatus(getContext(), listModel);
        recyclerView.setAdapter(contractStatus);

    }

}
