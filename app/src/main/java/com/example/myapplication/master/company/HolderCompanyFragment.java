package com.example.myapplication.master.company;


import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class HolderCompanyFragment extends Fragment {


    public HolderCompanyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_holder_company, container, false);
        setupView();
        setupTabLayout(view);

        return view;
    }

    private void setupView() {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Company");
    }

    private void setupTabLayout(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tab_layout_company);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.company_data)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.company_form)));

        Fragment fragmentData = new DataCompanyFragment();
        loadFragment(fragmentData);

        //noinspection deprecation
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            Fragment fragmentData = new DataCompanyFragment();
            Fragment fragmentForm = new FormCompanyFragment();
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    loadFragment(fragmentData);
                }
                if (tab.getPosition() == 1){
                    loadFragment(fragmentForm);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_company, fragment);
        transaction.commit();
    }
}