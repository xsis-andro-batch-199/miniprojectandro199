package com.example.myapplication.master.status_pernikahan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;


import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterStatusPernikahan;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class StatusPernikahanActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FloatingActionButton btnAdd;
    private List<ModelStatusPernikahan> statusPernikahanList = new ArrayList<>();
    private AdapterStatusPernikahan adapterStatusPernikahan;
    private RecyclerView recyclerView;
    private EditText searching;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_pernikahan);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setupView();
        loadData();
        showData();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.recStatusPernikahan);
        searching = findViewById(R.id.et_search);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InputStatusPernikahanActivity.class));
                finish();
            }
        });

        searching.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                return false;
            }
        });
        searching.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadData();
                showData();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String searchText = searching.getText().toString();
                if (searchText.isEmpty()){
                    recyclerView.setAdapter(adapterStatusPernikahan);
                } else {
                    db = Room.databaseBuilder(getApplicationContext(),
                            Database.class, "xsis_project")
                            .allowMainThreadQueries().build();
                    statusPernikahanList = db.daoAccessStatusPernikahan().findByStatusPernikahan(searchText);
                    showData();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }



    private void loadData() {
        db = Room.databaseBuilder(this, Database.class, "xsis_project")
                .allowMainThreadQueries().build();
        statusPernikahanList = db.daoAccessStatusPernikahan().getAllStatusPernikahan();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                RecyclerView.VERTICAL, false));
        adapterStatusPernikahan = new AdapterStatusPernikahan(this, statusPernikahanList);
        recyclerView.setAdapter(adapterStatusPernikahan);

    }
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
