package com.example.myapplication.master.company;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.OnBackPressed;
import com.example.myapplication.OnBackPressedFragment;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelCompany;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormCompanyFragment extends Fragment implements OnBackPressedFragment {
    private EditText etCompany, etCity, etPostalCode, etStreet, etBuilding, etFloor;
    private TextInputLayout tilCompany, tilCity, tilPostalCode, tilStreet, tilBuilding, tilFloor;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;

    public FormCompanyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_company, container, false);
        setupView(view);
        setupAction();

        return view;
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        etCompany   = view.findViewById(R.id.et_company);
        etCity      = view.findViewById(R.id.et_city);
        etPostalCode= view.findViewById(R.id.et_postalcode);
        etStreet    = view.findViewById(R.id.et_street);
        etBuilding  = view.findViewById(R.id.et_building);
        etFloor     = view.findViewById(R.id.et_floor);

        buttonReset = view.findViewById(R.id.btn_reset);
        buttonSave  = view.findViewById(R.id.btn_save);

        tilCompany   = view.findViewById(R.id.til_company);
        tilCity      = view.findViewById(R.id.til_city);
        tilPostalCode= view.findViewById(R.id.til_postalcode);
        tilStreet    = view.findViewById(R.id.til_street);
        tilBuilding  = view.findViewById(R.id.til_building);
        tilFloor     = view.findViewById(R.id.til_floor);

        tabLayout = getActivity().findViewById(R.id.tab_layout_company);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void setupAction(){
        etCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etPostalCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etBuilding.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etFloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton(){
        final String inputCompany   = etCompany.getText().toString();
        final String inputCity      = etCity.getText().toString();
        final String inputPostalCode= etPostalCode.getText().toString();
        final String inputStreet    = etStreet.getText().toString();
        final String inputBuilding  = etBuilding.getText().toString();
        final String inputFloor     = etFloor.getText().toString();

        if (!inputCompany.isEmpty() && !inputCity.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cekCompany(inputCompany)){
                    tilCompany.setError(getString(R.string.data_ada));
                } else {
                    ModelCompany modelCompany = new ModelCompany();
                    modelCompany.setCompany(inputCompany);
                    modelCompany.setCity(inputCity);
                    modelCompany.setPostalCode(inputPostalCode);
                    modelCompany.setStreet(inputStreet);
                    modelCompany.setBuilding(inputBuilding);
                    modelCompany.setFloor(inputFloor);
                    modelCompany.setDelete(false);
                    db.daoAccessCompany().insertCompany(modelCompany);

                    tabLayout.getTabAt(0).select();
                }
            }
        });

        if (!inputCompany.isEmpty()
            || !inputCity.isEmpty()
            || !inputPostalCode.isEmpty()
            || !inputStreet.isEmpty()
            || !inputBuilding.isEmpty()
            || !inputFloor.isEmpty()){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCompany.setText(null);
                etCity.setText(null);
                etPostalCode.setText(null);
                etStreet.setText(null);
                etBuilding.setText(null);
                etFloor.setText(null);
            }
        });
    }

    protected boolean cekCompany(String inputText) {
        List<ModelCompany> list = db.daoAccessCompany().getDBCompany();
        for (int i=0; i<list.size(); i++){
            if (inputText.contentEquals(list.get(i).getCompany())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onBackPressed() {
        tabLayout.getTabAt(0).select();
        return true;
    }
}
