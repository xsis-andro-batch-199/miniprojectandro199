package com.example.myapplication.master.training_organizer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTrainingOrganizer;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class FormTrainingOrganizerFragment extends Fragment {

    EditText etTOName, etNote;
    Button btnReset, btnSave;
    TextInputLayout requiredTO;
    ModelTrainingOrganizer modelTrainingOrganizer;
    ImageView btnDelete;

    public FormTrainingOrganizerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_training_organizer, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);
        requiredTO = view.findViewById(R.id.required_TO);
        etTOName = view.findViewById(R.id.etAddEditTO);
        etTOName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredTO.setError(getResources().getString(R.string.requiredText));
                requiredTO.setErrorEnabled(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etTOName.getText().toString();
                if (!inputText.isEmpty()){
                    requiredTO.setErrorEnabled(false);
                    requiredTO.setError(null);
                } else {
                    requiredTO.setErrorEnabled(true);
                    requiredTO.setError(getResources().getString(R.string.requiredText));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTOName.getText().toString();
                if (inputText.length() > 0){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etTOName.getText().toString().isEmpty()){
                                if (cekData(inputText)){
                                    etTOName.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    modelTrainingOrganizer = new ModelTrainingOrganizer();
                                    modelTrainingOrganizer.setTo_name(etTOName.getText().toString());
                                    modelTrainingOrganizer.setNotes(etNote.getText().toString());

                                    db.daoAccessTrainingOrganizer().insert_TO(modelTrainingOrganizer);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragment = new DataTrainingOrganizerFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frameMainTrainingOrganizer, fragment);
                                    transaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }

            }
        });
        etNote = view.findViewById(R.id.etNotesTO);
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnSave.setEnabled(false);
                btnReset.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etNote.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTOName.getText().toString();
                if (inputText.length() > 0){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etTOName.getText().toString().isEmpty()){
                                if (cekData(inputText)){
                                    etTOName.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    modelTrainingOrganizer = new ModelTrainingOrganizer();
                                    modelTrainingOrganizer.setTo_name(etTOName.getText().toString());
                                    modelTrainingOrganizer.setNotes(etNote.getText().toString());

                                    db.daoAccessTrainingOrganizer().insert_TO(modelTrainingOrganizer);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragment = new DataTrainingOrganizerFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frameMainTrainingOrganizer, fragment);
                                    transaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }

            }
        });
        btnReset = view.findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTOName.setText(null);
                etNote.setText(null);
            }
        });
        btnSave = view.findViewById(R.id.btnSave);
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutTrainingOrganizer);
        tabLayout.getTabAt(1).select();
    }

    public boolean cekData(String inputText){
        List<ModelTrainingOrganizer> list = db.daoAccessTrainingOrganizer().getDB_TO();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTo_name())){
                return true;
            }
        }
        return false;
    }

}
