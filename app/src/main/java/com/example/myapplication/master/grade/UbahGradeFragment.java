package com.example.myapplication.master.grade;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelGrade;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahGradeFragment extends Fragment {
    TextView tvHeaderGrade;
    EditText etGrade, etNote;
    Button btnReset, btnSave;
    TextInputLayout errorRequired;
    ImageView btnDelete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_grade, container, false);
        setupView(view);
        loadData();
        return view;
    }

    private void setupView(View view) {
        tvHeaderGrade = view.findViewById(R.id.tvHeaderAddEditGrade);
        etGrade = view.findViewById(R.id.etNameGrade);
        etNote = view.findViewById(R.id.etNotesGrade);
        btnReset = view.findViewById(R.id.btnResetGrade);
        btnSave = view.findViewById(R.id.btnSaveGrade);
        btnDelete = view.findViewById(R.id.btnDeleteGrade);
        errorRequired = view.findViewById(R.id.tilGrade);

        tvHeaderGrade.setText(getResources().getString(R.string.editGrade));

        btnSave.setEnabled(false);
        btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
        btnReset.setEnabled(false);
        btnReset.setBackgroundColor(getResources().getColor(R.color.gray));

        Bundle bundle = this.getArguments();
        final ModelGrade modelGrade = (ModelGrade) bundle.getSerializable("data");

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etGrade.setText(modelGrade.getGrade());
                etNote.setText(modelGrade.getNotes());
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputText = etGrade.getText().toString();
                if (cekDataGrade(inputText)){
                    etGrade.setError(getResources().getString(R.string.data_ada));
                } else {
                    ModelGrade update = new ModelGrade();
                    update.setId(modelGrade.getId());
                    update.setGrade(etGrade.getText().toString());
                    update.setNotes(etNote.getText().toString());

                    db.daoAccessGrade().updateGrade(update);
                    Fragment fragmentData = new DataGradeFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.mainFrameGrade, fragmentData);
                    fragmentTransaction.commit();

                    Toast.makeText(getContext(), "Terupdate", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etGrade.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnChange();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelGrade.getGrade() + " ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessGrade().getUpdateGrade(modelGrade.getId());

                        Toast.makeText(getContext(), "Data " + modelGrade.getGrade() + " terhapus", Toast.LENGTH_SHORT).show();

                        Fragment fragmentData = new DataGradeFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.mainFrameGrade, fragmentData);
                        fragmentTransaction.commit();
                    }
                });
                builder.show();
            }
        });

    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        final ModelGrade modelGrade = (ModelGrade) bundle.getSerializable("data");

        if (bundle != null){
            etGrade.setText(modelGrade.getGrade());
            etNote.setText(modelGrade.getNotes());
        } else {
            Toast.makeText(getContext(), "bundle is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnChange(){
        Bundle bundle = this.getArguments();
        final ModelGrade modelGrade = (ModelGrade) bundle.getSerializable("data");
        if ((!modelGrade.getGrade().equals(etGrade.getText().toString())
            && !etGrade.getText().toString().isEmpty())
            || !modelGrade.getNotes().equals(etNote.getText().toString())){
            btnReset.setEnabled(true);
            btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
            btnSave.setEnabled(true);
            btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            btnReset.setEnabled(false);
            btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
            btnSave.setEnabled(false);
            btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
    }

    private boolean cekDataGrade(String input){
        List<ModelGrade> list = db.daoAccessGrade().getAllGradeNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (input.contentEquals(list.get(i).getGrade())){
                return true;
            }
        }
        return false;
    }
}
