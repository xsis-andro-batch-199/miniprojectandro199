package com.example.myapplication.master.provider_tools;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toolbar;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderToolsFragment extends Fragment {
    Toolbar toolbar;



    public ProviderToolsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_provider_tools, container, false);
        setupTabLayout(view);
        getActivity().setTitle(getResources().getString(R.string.providerTools));
        return  view;
    }

    private void setupTabLayout(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tabLayoutProvider);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.data)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.formProvider)));

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1 || i == 0){
                viewGroupTab.setEnabled(false);
            }
        }
        Fragment fragmentDataProvider = new DataProviderFragment();
        loadFragment(fragmentDataProvider);
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameMainProvider, fragment);
        transaction.commit();
    }



}
