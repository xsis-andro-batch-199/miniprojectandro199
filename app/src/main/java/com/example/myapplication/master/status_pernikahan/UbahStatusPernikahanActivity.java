package com.example.myapplication.master.status_pernikahan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahStatusPernikahanActivity extends AppCompatActivity {

    private Button btnupdate, btncancel;
    private EditText updateStatus, updateDiskripsi;

    ModelStatusPernikahan modelStatusPernikahan;

    public static final String EXTRA_DATA = "extra_data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_status_pernikahan);

        modelStatusPernikahan = (ModelStatusPernikahan) getIntent().getSerializableExtra(EXTRA_DATA);
        setupView();
        loadData();
    }

    private void setupView() {
        updateStatus = findViewById(R.id.updateStatusPernikahan);
        updateDiskripsi = findViewById(R.id.updateDiskripsi);
        final TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.errorMessage);
        usernameWrapper.setHint("Status Pernikahan*");


        btncancel = findViewById(R.id.btnUpdateCancel);
        btnupdate = findViewById(R.id.btnUpdateSave);

        updateStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    usernameWrapper.setErrorEnabled(false);
                    usernameWrapper.setHint("Status Pernikahan*");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(updateStatus.getText())){
                    usernameWrapper.setErrorEnabled(true);
                    usernameWrapper.setError(getResources().getString(R.string.error_statusPernikahan));
                    usernameWrapper.setHint("Status pernikahan*");
                } else {
                    usernameWrapper.setErrorEnabled(false);
                    usernameWrapper.setHint("Status pernikahan*");
                    updateStatus.setHint("Status pernikahan");

                }
                usernameWrapper.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateStatus.setText(modelStatusPernikahan.getStatuspernikahan());
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = updateStatus.getText().toString();
                if (!modelStatusPernikahan.getStatuspernikahan().equals(inputText)
                        && !updateStatus.getText().toString().isEmpty()){
                    btnupdate.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnupdate.setEnabled(true);
                    btnupdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!inputText.isEmpty()){
                                if (cekData(inputText)){
                                    updateStatus.setError("Data sudah ada");
                                } else {
                                    ModelStatusPernikahan updateStatusPernikahan = new ModelStatusPernikahan();
                                    updateStatusPernikahan.setId(modelStatusPernikahan.getId());
                                    updateStatusPernikahan.setStatuspernikahan(updateStatus.getText().toString());
                                    updateStatusPernikahan.setDeskripsi(updateDiskripsi.getText().toString());
                                    updateStatusPernikahan.setIs_delete(false);

                                    db.daoAccessStatusPernikahan().updateStatusPernikahan(updateStatusPernikahan);
                                    startActivity(new Intent(getApplicationContext(),StatusPernikahanActivity.class));
                                    finish();
                                }
                            }


                        }
                    });

                } else {
                    btnupdate.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnupdate.setEnabled(false);
                }

            }
        });

        updateDiskripsi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputDiskripsi = updateDiskripsi.getText().toString();
                if (!modelStatusPernikahan.getDeskripsi().equals(inputDiskripsi)
                        && !updateDiskripsi.getText().toString().isEmpty()){
                    btnupdate.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnupdate.setEnabled(true);
                    btnupdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                    ModelStatusPernikahan updateStatusPernikahan = new ModelStatusPernikahan();
                                    updateStatusPernikahan.setId(modelStatusPernikahan.getId());
                                    updateStatusPernikahan.setStatuspernikahan(updateStatus.getText().toString());
                                    updateStatusPernikahan.setDeskripsi(updateDiskripsi.getText().toString());
                                    updateStatusPernikahan.setIs_delete(false);

                                    db.daoAccessStatusPernikahan().updateStatusPernikahan(updateStatusPernikahan);
                                    startActivity(new Intent(getApplicationContext(),StatusPernikahanActivity.class));
                                    finish();
                                }
                    });

                } else {
                    btnupdate.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnupdate.setEnabled(false);
                }

            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StatusPernikahanActivity.class));
                finish();
            }
        });

    }

    public boolean cekData(String inputText){
        List<ModelStatusPernikahan> list = db.daoAccessStatusPernikahan().getAllStatusNoCondition();
        for (int i = 0; i < list.size(); i++) {
            if (inputText.contentEquals(list.get(i).getStatuspernikahan())){
                return true;
            }
        }
        return false;
    }

    private void loadData() {
        updateStatus.setText(modelStatusPernikahan.getStatuspernikahan());
        updateDiskripsi.setText(modelStatusPernikahan.getDeskripsi());

    }
    public void onBackPressed(){
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), StatusPernikahanActivity.class));

    }
}

