package com.example.myapplication.master.jenis_catatan;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelJenisCatatan;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputJenisCatatan extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText etJenisCatatan, etDeskripsiJenisCatatan;
    private TextView idTitleJenisCatatan;
    private Button btnSimpanJenisCatatan, btnBatalJenisCatatan;
    private ModelJenisCatatan modelJenisCatatan;
    private TextInputLayout psnError;
    private ImageView ivClearJenisCatatan, ivClearDesJenisCatatan;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_jenis_catatan);
        setToolbar();
        setUpView();
        idTitleJenisCatatan.setText(getResources().getString(R.string.title_jeniscatatan_input));
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void setUpView() {
        idTitleJenisCatatan = findViewById(R.id.idTitleJenisCatatan);
        etJenisCatatan = findViewById(R.id.etJenisCatatan);
        etDeskripsiJenisCatatan = findViewById(R.id.etDeskripsiJenisCatatan);
        btnBatalJenisCatatan = findViewById(R.id.btnBatalJenisCatatan);
        btnSimpanJenisCatatan = findViewById(R.id.btnSimpanJenisCatatan);
        ivClearJenisCatatan = findViewById(R.id.ivClearJenisCatatan);
        ivClearDesJenisCatatan = findViewById(R.id.ivClearDesJenisCatatan);
        ivClearJenisCatatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etJenisCatatan.getText().clear();
            }
        });
        ivClearDesJenisCatatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDeskripsiJenisCatatan.getText().clear();
            }
        });
        psnError = findViewById(R.id.pesan_error);
        btnBatalJenisCatatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputJenisCatatan.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), JenisCatatanActivity.class));
                finish();
            }
        });
        validasi();

    }
    private void validasi() {
        etDeskripsiJenisCatatan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(etDeskripsiJenisCatatan.getText())) {
                    ivClearDesJenisCatatan.setVisibility(View.GONE);
                } else {
                    ivClearDesJenisCatatan.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etJenisCatatan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesanErrorInputJenisCatatan();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String valueNama = etJenisCatatan.getText().toString();
                if (valueNama.length() > 0) {
                    btnSimpanJenisCatatan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanJenisCatatan.setEnabled(true);
                    btnSimpanJenisCatatan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!valueNama.isEmpty()) {
                                if (cekDataNama(valueNama)) {
                                    etJenisCatatan.setError("Data sudah ada");
                                } else {
                                    modelJenisCatatan = new ModelJenisCatatan();
                                    modelJenisCatatan.setJenisCatatanNama(etJenisCatatan.getText().toString());
                                    modelJenisCatatan.setJenisCatatanDeskripsi(etDeskripsiJenisCatatan.getText().toString());
                                    modelJenisCatatan.setDelete(false);

                                    db.daoAccessJenisCatatan().QueryInserJenisCatatan(modelJenisCatatan);
                                    Toast.makeText(InputJenisCatatan.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), JenisCatatanActivity.class));
                                    finish();
                                }

                            } else {
                                Toast.makeText(InputJenisCatatan.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpanJenisCatatan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanJenisCatatan.setEnabled(false);
                }
            }
        });
    }
    private void pesanErrorInputJenisCatatan() {
        if (TextUtils.isEmpty(etJenisCatatan.getText())) {
            ivClearJenisCatatan.setVisibility(View.GONE);
            psnError.setErrorEnabled(true);
            psnError.setError(getResources().getString(R.string.error_jeniscatatan));
            psnError.setHint("Jenis Catatan");
        } else {
            ivClearJenisCatatan.setVisibility(View.VISIBLE);
            psnError.setErrorEnabled(false);
            psnError.setHint("Jenis Catatan");
            etJenisCatatan.setHint("Jenis Catatan");
        }
    }
    public boolean cekDataNama(String valueCek) {
        List<ModelJenisCatatan> list = db.daoAccessJenisCatatan().QueryGetAllJenisCatatanNoCon();
        for (int i = 0; i < list.size(); i++) {
            if (valueCek.contentEquals(list.get(i).getJenisCatatanNama())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } /*else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }*/
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
