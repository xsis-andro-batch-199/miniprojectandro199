package com.example.myapplication.master.grade;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelGrade;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormGradeFragment extends Fragment {
    EditText etGrade, etNotesGade;
    Button btnReset, btnSave;
    TextInputLayout errorRequirment;
    ModelGrade gradeModel;
    ImageView btnDelete;


    public FormGradeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_grade, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
     btnDelete = view.findViewById(R.id.btnDeleteGrade);
     btnDelete.setVisibility(View.GONE);
     errorRequirment = view.findViewById(R.id.tilGrade);
     etGrade = view.findViewById(R.id.etNameGrade);
     etGrade.addTextChangedListener(new TextWatcher() {
         @Override
         public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

         }

         @Override
         public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
             String inputText = etGrade.getText().toString();
             if (!inputText.isEmpty()){
                 errorRequirment.setErrorEnabled(false);
                 errorRequirment.setError(null);
             } else {
                 errorRequirment.setErrorEnabled(true);
                 errorRequirment.setError(getResources().getString(R.string.required));
             }
         }

         @Override
         public void afterTextChanged(Editable editable) {
             final String input = etGrade.getText().toString();
             if (input.length() > 0){
                 btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                 btnReset.setEnabled(true);
                 btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                 btnSave.setEnabled(true);
                 btnSave.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         if (!etGrade.getText().toString().isEmpty()){
                             if (cekDataGrade(input)){
                                 etGrade.setError(getResources().getString(R.string.data_ada));
                             } else {
                                 gradeModel = new ModelGrade();
                                 gradeModel.setGrade(etGrade.getText().toString());
                                 gradeModel.setNotes(etNotesGade.getText().toString());
                                 gradeModel.setIs_delete(false);

                                 db.daoAccessGrade().insertGrade(gradeModel);
                                 Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                 Fragment fragmentData = new DataGradeFragment();
                                 FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                 fragmentTransaction.replace(R.id.mainFrameGrade, fragmentData);
                                 fragmentTransaction.commit();
                             }
                         }
                     }
                 });
             } else {
                 btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                 btnSave.setEnabled(false);
                 btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                 btnReset.setEnabled(false);
             }
         }
     });

     etNotesGade = view.findViewById(R.id.etNotesGrade);
     etNotesGade.addTextChangedListener(new TextWatcher() {
         @Override
         public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

         }

         @Override
         public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
             if (!etNotesGade.getText().toString().isEmpty()){
                 btnReset.setEnabled(true);
                 btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
             } else {
                 btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                 btnReset.setEnabled(false);
             }
         }

         @Override
         public void afterTextChanged(Editable editable) {

         }
     });
     btnReset = view.findViewById(R.id.btnResetGrade);
     btnReset.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             etGrade.setText("");
             etNotesGade.setText("");
         }
     });
     btnSave = view.findViewById(R.id.btnSaveGrade);
     btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
     btnSave.setEnabled(false);
     btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
     btnReset.setEnabled(false);

        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutGrade);
        tabLayout.getTabAt(1).select();

    }

    private boolean cekDataGrade(String input){
        List<ModelGrade> list = db.daoAccessGrade().getAllGradeNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (input.contentEquals(list.get(i).getGrade())){
                return true;
            }
        }
        return false;
    }
}
