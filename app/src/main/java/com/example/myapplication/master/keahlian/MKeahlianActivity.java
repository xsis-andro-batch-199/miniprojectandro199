package com.example.myapplication.master.keahlian;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterKeahlian;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelKeahlian;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class MKeahlianActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ImageView btnTambahKeahlian;
    private List<ModelKeahlian> list = new ArrayList<>();
    AdapterKeahlian adapterKeahlian;
    private RecyclerView recyclerView;
    private EditText etCariKeahlian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mkeahlian);

        setupDrawer();
        setupView();
        loadData();
        showData();
    }

    private void setupDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbarKeahlian);
        setSupportActionBar(toolbar);
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayoutKeahlian);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvKeahlian);
        etCariKeahlian = findViewById(R.id.etCariKeahlian);
        btnTambahKeahlian = findViewById(R.id.btnTambahkeahlian);

        etCariKeahlian.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                return false;
            }
        });

        etCariKeahlian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadData();
                showData();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etCariKeahlian.getText().toString();
                if (inputText.isEmpty()){
                    recyclerView.setAdapter(adapterKeahlian);
                } else{
                    db = Room.databaseBuilder(getApplicationContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                    list = db.daoAccessKeahlian().findByKeahlian(inputText);
                    showData();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnTambahKeahlian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InputKeahlianActivity.class));
                finish();
            }
        });


    }

    private void loadData() {
        db = Room.databaseBuilder(this, Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessKeahlian().getAllKeahlian();

    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapterKeahlian = new AdapterKeahlian(this, list);
        recyclerView.setAdapter(adapterKeahlian);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
