package com.example.myapplication.master.back_office_position;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.OnBackPressed;
import com.example.myapplication.OnBackPressedFragment;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelBOP;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormBOPFragment extends Fragment implements OnBackPressedFragment {
    private AutoCompleteTextView actvPositionLevel, actvCompany;
    private EditText etCode, etName, etPositionLevel, etCompany, etNotes;
    private TextInputLayout tilCode, tilName, tilPositionLevel, tilCompany, tilNotes;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;

    public FormBOPFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_bop, container, false);
        setupView(view);
        setupAction();
        return view;
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        actvPositionLevel = view.findViewById(R.id.actv_position_level_bop);
        actvCompany       = view.findViewById(R.id.actv_company_bop);

        etCode          = view.findViewById(R.id.et_code_bop);
        etName          = view.findViewById(R.id.et_name_bop);
//        etPositionLevel = view.findViewById(R.id.et_position_level_bop);
//        etCompany       = view.findViewById(R.id.et_company_bop);
        etNotes         = view.findViewById(R.id.et_notes_bop);

        buttonReset = view.findViewById(R.id.btn_reset_bop);
        buttonSave  = view.findViewById(R.id.btn_save_bop);

        tilCode         = view.findViewById(R.id.til_code_bop);
        tilName         = view.findViewById(R.id.til_name_bop);
        tilPositionLevel= view.findViewById(R.id.til_position_level_bop);
        tilCompany      = view.findViewById(R.id.til_company_bop);
        tilNotes        = view.findViewById(R.id.til_notes_bop);

        tabLayout = getActivity().findViewById(R.id.tab_layout_bop);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void setupAction() {
        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

//        position level
        String[] positionLevelArray = getResources().getStringArray(R.array.array_position_level_bop);
        ArrayAdapter<String> adapterPositionLevel = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, positionLevelArray);
        actvPositionLevel.setThreshold(0);
        actvPositionLevel.setAdapter(adapterPositionLevel);
        actvPositionLevel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        /*
        etPositionLevel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
*/

//        company
        String[] companyArray = getResources().getStringArray(R.array.client_timesheet);
        ArrayAdapter<String> adapterCompany = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, companyArray);
        actvCompany.setThreshold(0);
        actvCompany.setAdapter(adapterCompany);
        actvCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
        /*
        etCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
*/

        etNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton(){
        final String inputCode          = etCode.getText().toString();
        final String inputName          = etName.getText().toString();
//        final String inputPositionLevel = etPositionLevel.getText().toString();
        final String inputPositionLevel = actvPositionLevel.getText().toString();
//        final String inputCompany       = etCompany.getText().toString();
        final String inputCompany       = actvCompany.getText().toString();
        final String inputNotes         = etNotes.getText().toString();

        if (!inputCode.isEmpty() && !inputName.isEmpty() && !inputCompany.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cekCode(inputCode)){
                    tilCode.setError(getString(R.string.code_must_be_unique));
                }
                else if (cekName(inputName)){
                    tilName.setError(getString(R.string.data_already_exist));
                } else {
                    ModelBOP modelBOP = new ModelBOP();
                    modelBOP.setCode(inputCode);
                    modelBOP.setName(inputName);
                    modelBOP.setPositionLevel(inputPositionLevel);
                    modelBOP.setCompany(inputCompany);
                    modelBOP.setNotes(inputNotes);
                    modelBOP.setDelete(false);
                    db.daoAccessBOP().insertBOP(modelBOP);

                    tabLayout.getTabAt(0).select();
                }
            }
        });

        if (!inputCode.isEmpty()
                || !inputName.isEmpty()
                || !inputPositionLevel.isEmpty()
                || !inputCompany.isEmpty()
                || !inputNotes.isEmpty()){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCode.setText(null);
                etName.setText(null);
                etPositionLevel.setText(null);
                etCompany.setText(null);
                etNotes.setText(null);
            }
        });
    }

    protected boolean cekCode(String inputText) {
        List<ModelBOP> list = db.daoAccessBOP().getDBBOP();
        for (int i=0; i<list.size(); i++){
            if (inputText.contentEquals(list.get(i).getCode())){
                return true;
            }
        }
        return false;
    }

    protected boolean cekName(String inputText) {
        List<ModelBOP> list = db.daoAccessBOP().getDBBOP();
        for (int i=0; i<list.size(); i++){
            if (inputText.contentEquals(list.get(i).getName())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onBackPressed() {
        tabLayout.getTabAt(0).select();
        return true;
    }
}
