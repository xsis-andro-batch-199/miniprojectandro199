package com.example.myapplication.master.keahlian;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelKeahlian;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputKeahlianActivity extends AppCompatActivity {
    private EditText etKeahlian, etDeskripsiKeahlian;
    private TextView idTitleKeahlian;
    private Button btnSimpan, btnBatal;
    private TextInputLayout psnError;
    private ModelKeahlian listKeahlian;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_keahlian);
        setUpView();
        idTitleKeahlian.setText(getResources().getString(R.string.title_keahlian_input));
    }

    private void setUpView() {
        idTitleKeahlian =findViewById(R.id.tvInputKeahlian);
        etKeahlian = findViewById(R.id.etinKeahlian);
        etDeskripsiKeahlian = findViewById(R.id.etinDeskripsiKeahlian);
        btnBatal = findViewById(R.id.btnBatalKeahlian);
        btnSimpan = findViewById(R.id.btnSimpanKeahlian);

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputKeahlianActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
                finish();
            }
        });
        psnError = findViewById(R.id.errorKeahlian);
        psnError.setErrorEnabled(true);
        btnSimpan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!hasWindowFocus()) {
                    if (!TextUtils.isEmpty(etKeahlian.getText())){
                        psnError.setError(null);
                    } else {
                        psnError.setError(getResources().getString(R.string.error_keahlian));
                    }
                }
            }
        });

        etKeahlian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                psnError.setErrorEnabled(true);
                psnError.setError(getResources().getString(R.string.error_keahlian));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etKeahlian.getText().toString();
                if (!inputText.isEmpty()){
                    psnError.setErrorEnabled(false);
                    psnError.setError(null);
                } else {
                    psnError.setErrorEnabled(true);
                    psnError.setError(getResources().getString(R.string.error_keahlian));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etKeahlian.getText().toString();
                if(inputText.length() > 0) {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etKeahlian.getText().toString().isEmpty()){
                                listKeahlian = new ModelKeahlian();
                                listKeahlian.setNama_keahlian(etKeahlian.getText().toString());
                                listKeahlian.setDeskripsi_keahlian(etDeskripsiKeahlian.getText().toString());
                                listKeahlian.setIs_delete(false);
                                if (cekKeahlian(inputText)){
                                    etKeahlian.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    //mengisi db dengan data
                                    db.daoAccessKeahlian().insertKeahlian(listKeahlian);
                                    Toast.makeText(InputKeahlianActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
                                    finish();

                                }

                            } else {
                                Toast.makeText(InputKeahlianActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }
            }
        });
    }

    private boolean cekKeahlian(String inputText){
        List<ModelKeahlian> list = db.daoAccessKeahlian().getAllKeahlianNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getNama_keahlian())){
                return true;
            }
        }
        return false;
    }
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
    }
}