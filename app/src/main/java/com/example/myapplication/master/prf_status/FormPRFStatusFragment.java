package com.example.myapplication.master.prf_status;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFStatus;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormPRFStatusFragment extends Fragment {
    private EditText nama, notes;
    private Button submit, reset;
    private ModelPRFStatus modelPRFStatus;


    public FormPRFStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_prfstatus, container, false);
        setupView(view);
        buttonAction();
        return view;
    }

    private void setupView(View view) {
        final TextInputLayout msgError = (TextInputLayout) view.findViewById(R.id.msgErrorNamaPRFStatus);
        nama = view.findViewById(R.id.inputNamaPRFStatus);
        nama.setHint("Name *");
        nama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setupButton();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(nama.getText())) {
                    nama.setHintTextColor(getResources().getColor(R.color.orens));
                    msgError.setError("Required");
                } else {
                    msgError.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
                final String inputText = nama.getText().toString();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!nama.getText().toString().isEmpty()) {
                            if (cekData(inputText)) {
                                nama.setError("Data sudah ada");
                            } else {
                                inputData();
                            }
                        }
                    }
                });

            }
        });
        notes = view.findViewById(R.id.inputNotesPRFStatus);
        notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();

            }
        });
        notes.setHint("Notes");
        submit = view.findViewById(R.id.btnSubmit);
        reset = view.findViewById(R.id.btnReset);
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutPRFStatus);
        tabLayout.getTabAt(1).select();
    }

    public boolean cekData(String inputText) {
        List<ModelPRFStatus> list = db.daoAccessPRFStatus().getAllPRFStatus();
        for (int i = 0; i < list.size(); i++) {
            if (inputText.contentEquals(list.get(i).getNamaprf())) {
                return true;
            }
        }
        return false;
    }

    private void setupButton() {
        if (TextUtils.isEmpty(nama.getText())) {
            submit.setEnabled(false);
        } else {
            submit.setEnabled(true);
            reset.setBackgroundColor(getResources().getColor(R.color.orens));
        }
        if (!TextUtils.isEmpty(nama.getText()) || !TextUtils.isEmpty(notes.getText())){
            reset.setEnabled(true);
            reset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            reset.setEnabled(false);
            reset.setBackgroundColor(getResources().getColor(R.color.gray));

        }
    }

    private void inputData() {
        modelPRFStatus = new ModelPRFStatus();
        modelPRFStatus.setNamaprf(nama.getText().toString());
        modelPRFStatus.setNotes(notes.getText().toString());
        db.daoAccessPRFStatus().insertPRFStatus(modelPRFStatus);
        Fragment mainFragmentPRFStatus = new MainPRFStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainPRFStatusLayout, mainFragmentPRFStatus);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private void buttonAction(){
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelPRFStatus = new ModelPRFStatus();
                modelPRFStatus.setNamaprf(nama.getText().toString());
                modelPRFStatus.setNotes(notes.getText().toString());
                db.daoAccessPRFStatus().insertPRFStatus(modelPRFStatus);
                Fragment mainFragmentPRFStatus = new MainPRFStatusFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.mainPRFStatusLayout, mainFragmentPRFStatus);
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nama.setText(null);
                notes.setText(null);
            }
        });
    }


}
