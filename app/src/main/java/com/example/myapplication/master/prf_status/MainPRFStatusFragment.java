package com.example.myapplication.master.prf_status;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainPRFStatusFragment extends Fragment {


    public MainPRFStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_main_prfstatus, container, false);
        getActivity().setTitle("PRF Status");
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutPRFStatus);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i ==1){
                viewGroupTab.setEnabled(false);
            }
        }
        Fragment fragmentData = new DataPRFStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainPRFStatusLayout, fragmentData);
        transaction.addToBackStack(null);
        transaction.commit();
        return view;

    }

}
