package com.example.myapplication.master.employee_type;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.EmployeeTypeAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelEmployeeType;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataEmployeeTypeFragment extends Fragment {
    private RecyclerView recyclerView;
    private EmployeeTypeAdapter adapter;
    private List<ModelEmployeeType> list = new ArrayList<>();

    private ImageView buttonTambah;
    private TabLayout tabLayout;

    public DataEmployeeTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_employee_type, container, false);
        setupView(view);
        setupButton();
        loadData();
        showData();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessEmployeeType().findByEmployeeType(query);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessEmployeeType().findByEmployeeType(newText);
                showData();
                searchView.clearFocus();
                return true;            }
        });
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        recyclerView = view.findViewById(R.id.rv_et);
        buttonTambah = view.findViewById(R.id.btn_tambah_et);
        tabLayout = getActivity().findViewById(R.id.tab_layout_et);
    }

    private void setupButton() {
        buttonTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabLayout.getTabAt(1).select();
            }
        });
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessEmployeeType().getViewEmployeeType();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new EmployeeTypeAdapter(getContext(), list);
        recyclerView.setAdapter(adapter);
    }
}