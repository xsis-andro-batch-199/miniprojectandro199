package com.example.myapplication.master.tipe_tes;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelTipeTes;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahTipeTesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText etNamaTipeTes, etDesTipeTes;
    private TextView idTitleTipeTes;
    private Button btnSubmit, btnCancel;
    private ImageView ivClearTipeTes, ivClearDesTipeTes;
    private TextInputLayout psnError;
    private ModelTipeTes modelTipeTes;
    private String valNamaTipeTes, valDesTipeTes;
    public static final String EXTRA_DATA = "extra_data";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_tipe_tes);
        modelTipeTes = (ModelTipeTes) getIntent().getSerializableExtra(EXTRA_DATA);
        setToolbar();
        setUpView();
        loadData();
        batalListener();
        simpanListener();

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void setUpView() {
        idTitleTipeTes = findViewById(R.id.idTitleTipeTes);
        etNamaTipeTes = findViewById(R.id.etNamaTipeTes);
        etDesTipeTes = findViewById(R.id.etDeskripsiTipeTes);
        btnCancel = findViewById(R.id.btnBatalTipeTes);
        btnSubmit = findViewById(R.id.btnSaveTipeTes);
        ivClearTipeTes = findViewById(R.id.ivClearTipeTes);
        ivClearDesTipeTes = findViewById(R.id.ivClearDesTipeTes);
        psnError = findViewById(R.id.psnError);
        valNamaTipeTes = etNamaTipeTes.getText().toString();
        valDesTipeTes  = etDesTipeTes.getText().toString();

    }
    private void loadData() {
        etNamaTipeTes.setText(modelTipeTes.getTipeTesNama());
        etDesTipeTes.setText(modelTipeTes.getTipeTesDeskripsi());
    }
    private void batalListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(UbahTipeTesActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                Intent intentCancel = new Intent(getBaseContext(), TipeTesActivity.class);
                startActivity(intentCancel);
                finish();
            }
        });
    }
    private void simpanListener() {
        ivClearTipeTes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etNamaTipeTes.setText(modelTipeTes.getTipeTesNama());
            }
        });
        ivClearDesTipeTes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etDesTipeTes.setText(modelTipeTes.getTipeTesDeskripsi());
            }
        });
        etDesTipeTes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (modelTipeTes.getTipeTesDeskripsi().equals(etDesTipeTes.getText().toString()) || TextUtils.isEmpty(etDesTipeTes.getText())) {
                    ivClearDesTipeTes.setVisibility(View.GONE);
                } else {
                    ivClearDesTipeTes.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!modelTipeTes.getTipeTesDeskripsi().equals(etDesTipeTes.getText().toString())){
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSubmit.setEnabled(true);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelTipeTes updateTipeTes = new ModelTipeTes();
                            updateTipeTes.setId(modelTipeTes.getId());
                            updateTipeTes.setTipeTesNama(etNamaTipeTes.getText().toString());
                            updateTipeTes.setTipeTesDeskripsi(etDesTipeTes.getText().toString());
                            updateTipeTes.setDelete(false);
                            db.daoAccessTipeTes().QueryUpdateTipeTes(updateTipeTes);
                            Toast.makeText(UbahTipeTesActivity.this, "Berhasil di Ubah", Toast.LENGTH_SHORT).show();
                            Intent intentUpdate = new Intent(getBaseContext(), TipeTesActivity.class);
                            startActivity(intentUpdate);
                            finish();
                        }
                    });
                }else{
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSubmit.setEnabled(false);
                }
            }
        });
        etNamaTipeTes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorAlert();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNamaTipeTes.getText().toString();
                if(!modelTipeTes.getTipeTesNama().equals(inputText)
                        && !etNamaTipeTes.getText().toString().isEmpty()){
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSubmit.setEnabled(true);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!inputText.isEmpty()) {
                                if (cekDataNama(inputText)) {
                                    etNamaTipeTes.setError("Data sudah ada");
                                } else {
                                    ModelTipeTes updateTipeTes = new ModelTipeTes();
                                    updateTipeTes.setId(modelTipeTes.getId());
                                    updateTipeTes.setTipeTesNama(etNamaTipeTes.getText().toString());
                                    updateTipeTes.setTipeTesDeskripsi(etDesTipeTes.getText().toString());
                                    updateTipeTes.setDelete(false);
                                    db.daoAccessTipeTes().QueryUpdateTipeTes(updateTipeTes);
                                    Toast.makeText(UbahTipeTesActivity.this, "Berhasil di Ubah", Toast.LENGTH_SHORT).show();
                                    Intent intentUpdate = new Intent(getBaseContext(), TipeTesActivity.class);
                                    startActivity(intentUpdate);
                                    finish();
                                }
                            } else {
                                Toast.makeText(UbahTipeTesActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSubmit.setEnabled(false);
                }
            }
        });
    }
    private boolean cekDataNama(String inputText) {
        List<ModelTipeTes> list = db.daoAccessTipeTes().QueryGetAllTipeTesNoCon();
        for (int i = 0; i < list.size(); i++) {
            if (inputText.contentEquals(list.get(i).getTipeTesNama())) {
                return true;
            }
        }
        return false;
    }
    private void errorAlert() {
        if (modelTipeTes.getTipeTesNama().equals(etNamaTipeTes.getText().toString()) || TextUtils.isEmpty(etNamaTipeTes.getText())) {
            ivClearTipeTes.setVisibility(View.GONE);
        } else {
            ivClearTipeTes.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(etNamaTipeTes.getText())) {
            psnError.setErrorEnabled(true);
            psnError.setError(getResources().getString(R.string.error_tipe_tes));
            psnError.setHint("Tipe Tes");
        } else {
            psnError.setErrorEnabled(false);
            psnError.setHint("Tipe Tes");
            etNamaTipeTes.setHint("Tipe Tes");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
