package com.example.myapplication.master.keahlian;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelKeahlian;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahKeahlianActivity extends AppCompatActivity {
    private EditText etKeahlian, etDeskripsiKeahlian;
    private TextView idTitleKeahlian;
    private Button btnSimpan, btnBatal;
    private TextInputLayout psnError, tilDeskripsi;
    private ModelKeahlian listKeahlian;
    public static final  String EXTRA_DATA = "extra_data";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_keahlian);

        listKeahlian = (ModelKeahlian) getIntent().getSerializableExtra(EXTRA_DATA);
        setUpView();
        idTitleKeahlian.setText(getResources().getString(R.string.ubahKeahlian));
        loadData();
    }

    private void loadData() {
        etKeahlian.setText(listKeahlian.getNama_keahlian());
        etDeskripsiKeahlian.setText(listKeahlian.getDeskripsi_keahlian());
    }

    private void setUpView() {
        idTitleKeahlian = findViewById(R.id.tvInputKeahlian);
        etKeahlian = findViewById(R.id.etinKeahlian);
        etDeskripsiKeahlian = findViewById(R.id.etinDeskripsiKeahlian);
        btnBatal = findViewById(R.id.btnBatalKeahlian);
        btnSimpan = findViewById(R.id.btnSimpanKeahlian);

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(UbahKeahlianActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
                finish();
            }
        });
        psnError = findViewById(R.id.errorKeahlian);
        psnError.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etKeahlian.setText(listKeahlian.getNama_keahlian());
            }
        });
        psnError.setErrorEnabled(true);
        tilDeskripsi = findViewById(R.id.tilDeskripsiKeahlian);
        tilDeskripsi.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDeskripsiKeahlian.setText(listKeahlian.getDeskripsi_keahlian());
            }
        });
        btnSimpan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!hasWindowFocus()) {
                    if (!TextUtils.isEmpty(etKeahlian.getText())){
                        psnError.setError(null);
                    } else {
                        psnError.setError(getResources().getString(R.string.error_keahlian));
                    }
                }
            }
        });
        etDeskripsiKeahlian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etDeskripsiKeahlian.getText().toString();
                if (!listKeahlian.getDeskripsi_keahlian().equals(inputText)
                        && !etKeahlian.getText().toString().isEmpty()){
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etKeahlian.getText().toString().isEmpty()){
                                ModelKeahlian modelKeahlian = new ModelKeahlian();
                                modelKeahlian.setId(listKeahlian.getId());
                                modelKeahlian.setNama_keahlian(etKeahlian.getText().toString());
                                modelKeahlian.setDeskripsi_keahlian(etDeskripsiKeahlian.getText().toString());
                                db.daoAccessKeahlian().updateKeahlian(modelKeahlian);
                                startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
                                finish();

                            } else {
                                Toast.makeText(UbahKeahlianActivity.this, "Lengkapi data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }

            }
        });
        etKeahlian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                psnError.setErrorEnabled(true);
                psnError.setError(getResources().getString(R.string.error_keahlian));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etKeahlian.getText().toString();
                if (!inputText.isEmpty()){
                    psnError.setErrorEnabled(false);
                    psnError.setError(null);
                } else {
                    psnError.setErrorEnabled(true);
                    psnError.setError(getResources().getString(R.string.error_keahlian));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etKeahlian.getText().toString();
                if (!listKeahlian.getNama_keahlian().equals(inputText)
                        && !etKeahlian.getText().toString().isEmpty()){
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelKeahlian updateKeahlian = new ModelKeahlian();
                            updateKeahlian.setId(listKeahlian.getId());
                            updateKeahlian.setNama_keahlian(etKeahlian.getText().toString());
                            updateKeahlian.setDeskripsi_keahlian(etDeskripsiKeahlian.getText().toString());
                            if (cekKeahlian(inputText)){
                                etKeahlian.setError(getResources().getString(R.string.data_ada));
                            } else {

                                db.daoAccessKeahlian().updateKeahlian(updateKeahlian);
                                startActivity(new Intent(getApplicationContext(), MKeahlianActivity.class));
                                finish();
                            }

                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }
            }
        });
    }

    private boolean cekKeahlian(String inputText){
        List<ModelKeahlian> list = db.daoAccessKeahlian().getAllKeahlianNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getNama_keahlian())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MKeahlianActivity.class));
        finish();
    }
}