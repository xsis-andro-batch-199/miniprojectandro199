package com.example.myapplication.master.employee_status;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeStatus;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormEmployeeStatusFragment extends Fragment {
    private EditText nama, notes;
    private Button submit, reset;
    private ModelEmployeeStatus modelEmployeeStatus;


    public FormEmployeeStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_employee_status, container, false);

        setupView(view);
        buttonAction();
        return view;
    }

    private void setupView(View view) {
        final TextInputLayout msgError = (TextInputLayout) view.findViewById(R.id.msgErrorNamaEmployeeStatus);
        nama = view.findViewById(R.id.inputNamaEmployeeStatus);
        nama.setHint("Name *");
        nama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(nama.getText())) {
                    nama.setHintTextColor(getResources().getColor(R.color.orens));
                    msgError.setError("Required");
                } else {
                    msgError.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
                final String inputText = nama.getText().toString();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!nama.getText().toString().isEmpty()) {
                            if (cekData(inputText)) {
                                nama.setError("Data sudah ada");
                            } else {
                                inputData();
                            }
                        }
                    }
                });

            }

        });
        notes = view.findViewById(R.id.inputNotesEmployeeStatus);
        notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();

            }
        });
        notes.setHint("Notes");
        submit = view.findViewById(R.id.btnSubmit);
        reset = view.findViewById(R.id.btnReset);
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutEmployeeStatus);
        tabLayout.getTabAt(1).select();

    }

    private boolean cekData(String inputText) {
        List<ModelEmployeeStatus> listStatus = db.daoAccessEmployeeStatus().getAllNoCondition();
        for (int i = 0; i < listStatus.size(); i++) {
            if (inputText.contentEquals(listStatus.get(i).getNamaemployee())) {
                return true;
            }

        }
        return false;
    }

    private void setupButton() {
        if (TextUtils.isEmpty(nama.getText())) {
            submit.setEnabled(false);
        } else {
            submit.setEnabled(true);
            reset.setBackgroundColor(getResources().getColor(R.color.orens));
        }
        if (!TextUtils.isEmpty(nama.getText()) || !TextUtils.isEmpty(notes.getText())){
            reset.setEnabled(true);
            reset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            reset.setEnabled(false);
            reset.setBackgroundColor(getResources().getColor(R.color.gray));

        }
    }


    private void inputData() {
        modelEmployeeStatus = new ModelEmployeeStatus();
        modelEmployeeStatus.setNamaemployee(nama.getText().toString());
        modelEmployeeStatus.setNotes(notes.getText().toString());
        modelEmployeeStatus.setIs_delete(false);
        db.daoAccessEmployeeStatus().insertEmployeeStatus(modelEmployeeStatus);
        Toast.makeText(getContext(), modelEmployeeStatus.getNamaemployee(), Toast.LENGTH_SHORT).show();
        Fragment mainFragmentEmployee = new MainEmployeeStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_content, mainFragmentEmployee);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private void buttonAction(){
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelEmployeeStatus = new ModelEmployeeStatus();
                modelEmployeeStatus.setNamaemployee(nama.getText().toString());
                modelEmployeeStatus.setNotes(notes.getText().toString());
                modelEmployeeStatus.setIs_delete(false);
                db.daoAccessEmployeeStatus().insertEmployeeStatus(modelEmployeeStatus);
                Toast.makeText(getContext(), modelEmployeeStatus.getNamaemployee(), Toast.LENGTH_SHORT).show();
                Fragment mainFragmentEmployee = new MainEmployeeStatusFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_content, mainFragmentEmployee);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nama.setText(null);
                notes.setText(null);
            }
        });
    }
}
