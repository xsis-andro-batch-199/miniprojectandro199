package com.example.myapplication.master.employee_status;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.adapter.EmployeeStatusPagerAdapter;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainEmployeeStatusFragment extends Fragment {


    public MainEmployeeStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_main_employee_status, container, false);
        getActivity().setTitle("Employee Status");
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutEmployeeStatus);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1){
                viewGroupTab.setEnabled(false);
            }

        }
        Fragment fragmentData = new DataEmployeeStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainEmployeeStatusLayout, fragmentData);
        transaction.commit();
        return view;
    }

}
