package com.example.myapplication.master.provider_tools;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterProviderTools;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelProviderTools;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataProviderFragment extends Fragment {
    ImageView btnTambahProvider;
    RecyclerView recyclerView;
    AdapterProviderTools adapterProviderTools;
    List<ModelProviderTools> list = new ArrayList<>();


    public DataProviderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_provider, container, false);
        setupView(view);
        loadData();
        showData();

        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                /*db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessProviderTools().findByProvider(s);
                showData();
                searchView.clearFocus();*/
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessProviderTools().findByProvider(s);
                showData();
                /*searchView.clearFocus();*/
                return true;
            }
        });
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapterProviderTools = new AdapterProviderTools(getContext(), list);
        recyclerView.setAdapter(adapterProviderTools);
    }

    private void loadData() {
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessProviderTools().getAllProviderTools();
    }

    private void setupView(View view) {
        btnTambahProvider = view.findViewById(R.id.btnTambahProvider);
        recyclerView = view.findViewById(R.id.rvProviderTools);
        btnTambahProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentFormProvider = new FormProviderFragment();
                loadFragment(fragmentFormProvider);
            }
        });

        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutProvider);
        tabLayout.getTabAt(0).select();
    }


    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameMainProvider, fragment);
        transaction.commit();
    }

}
