package com.example.myapplication.master.provider_tools;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelProviderTools;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormProviderFragment extends Fragment {
    EditText etProviderTools, etNotesProvider;
    Button btnResetProvider, btnSaveProvider;
    TextInputLayout errorRequirment;
    ModelProviderTools providerToolsModel;
    ImageView btnDelete;


    public FormProviderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_provider, container, false);
        setupView(view);
        return view;
    }

    private boolean cekProviderInput(String inputText){
        List<ModelProviderTools> list = db.daoAccessProviderTools().getAllNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getProvider())){
                return true;
            }
        }
        return false;
    }

    private void setupView(View view){
        btnDelete = view.findViewById(R.id.btnDeleteProvider);
        btnDelete.setVisibility(View.GONE);
        errorRequirment = view.findViewById(R.id.errorProvider);
        etProviderTools = view.findViewById(R.id.etAddEditProvider);
        etProviderTools.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorRequirment.setErrorEnabled(true);
                errorRequirment.setError(getResources().getString(R.string.requiredText));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etProviderTools.getText().toString();
                if (!inputText.isEmpty()){
                    errorRequirment.setErrorEnabled(false);
                    errorRequirment.setError(null);
                } else {
                    errorRequirment.setErrorEnabled(true);
                    errorRequirment.setError(getResources().getString(R.string.requiredText));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etProviderTools.getText().toString();
                if (inputText.length() > 0){
                    btnResetProvider.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnResetProvider.setEnabled(true);
                    btnSaveProvider.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSaveProvider.setEnabled(true);
                    btnSaveProvider.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etProviderTools.getText().toString().isEmpty()){
                                if (cekProviderInput(inputText)){
                                    etProviderTools.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    providerToolsModel = new ModelProviderTools();
                                    providerToolsModel.setProvider(etProviderTools.getText().toString());
                                    providerToolsModel.setNotes(etNotesProvider.getText().toString());
                                    providerToolsModel.setIs_delete(false);

                                    db.daoAccessProviderTools().insertProviderTools(providerToolsModel);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragmentData = new DataProviderFragment();
                                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frameMainProvider, fragmentData);
                                    fragmentTransaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnResetProvider.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnResetProvider.setEnabled(false);
                    btnSaveProvider.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSaveProvider.setEnabled(false);
                }
            }
        });
        etNotesProvider = view.findViewById(R.id.etNotesProvider);
        etNotesProvider.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                btnResetProvider.setEnabled(false);
//                btnSaveProvider.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etNotesProvider.getText().toString().isEmpty()){
                    btnResetProvider.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnResetProvider.setEnabled(true);
                } else {
                    btnResetProvider.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnResetProvider.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnResetProvider = view.findViewById(R.id.btnResetProvider);
        btnResetProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "reset", Toast.LENGTH_SHORT).show();
                etProviderTools.setText("");
                etNotesProvider.setText("");
            }
        });
        btnSaveProvider = view.findViewById(R.id.btnSaveProvider);
        btnResetProvider.setBackgroundColor(getResources().getColor(R.color.gray));
        btnResetProvider.setEnabled(false);
        btnSaveProvider.setBackgroundColor(getResources().getColor(R.color.gray));
        btnSaveProvider.setEnabled(false);

        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutProvider);
        tabLayout.getTabAt(1).select();

    }

}
