package com.example.myapplication.master.employee_status;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeStatus;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class UbahFormEmployeeStatusFragment extends Fragment {
    private EditText nama, notes;
    private Button submit, reset;
    private ImageView delete;


    public UbahFormEmployeeStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ubah_form_employee_status, container, false);
        setupView(view);
        loadData();
        buttonAction();

        return view;
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelEmployeeStatus modelEmployeeStatus = (ModelEmployeeStatus) bundle.getSerializable("data");

        if (bundle != null) {
            String namaEmployee = modelEmployeeStatus.getNamaemployee();
            String notesEmployee = modelEmployeeStatus.getNotes();

            nama.setText(namaEmployee);
            notes.setText(notesEmployee);
        } else {

        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelEmployeeStatus modelEmployeeStatus = (ModelEmployeeStatus) bundle.getSerializable("data");
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutEmployeeStatus);
        tabLayout.getTabAt(1).select();
        final TextInputLayout msgError = (TextInputLayout) view.findViewById(R.id.msgErrorNamaEmployeeStatus);
        nama = view.findViewById(R.id.inputNamaEmployeeStatus);
        nama.setHint("Name *");
        nama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(nama.getText())) {
                    nama.setHintTextColor(getResources().getColor(R.color.orens));
                    msgError.setError("Required");
                } else {
                    msgError.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = nama.getText().toString();
                if (!modelEmployeeStatus.getNamaemployee().equals(inputText) &&
                        !nama.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!nama.getText().toString().isEmpty()) {
                                if (cekData(inputText)) {
                                    nama.setError("Data sudah ada");
                                } else {
                                    inputData();
                                }
                            }
                        }
                    });
                } else {
                    submit.setEnabled(false);
                    reset.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }
            }
        });
        notes = view.findViewById(R.id.inputNotesEmployeeStatus);
        notes.setHint("Notes");
        notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputNotes = notes.getText().toString();
                if (!modelEmployeeStatus.getNotes().equals(inputNotes) &&
                        !notes.getText().toString().isEmpty()) {
                    reset.setEnabled(true);
                    submit.setEnabled(true);
                    reset.setBackgroundColor(getResources().getColor(R.color.orens));

                } else {
                    submit.setEnabled(false);
                    reset.setEnabled(false);
                    reset.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
        });
        submit = view.findViewById(R.id.btnSubmit);
        reset = view.findViewById(R.id.btnReset);
        delete = view.findViewById(R.id.btnDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelEmployeeStatus.getNamaemployee() + "?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessEmployeeStatus().getUpdateEmployeeStatus(modelEmployeeStatus.getId());
                        Fragment mainFragmentEmployee = new MainEmployeeStatusFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_content, mainFragmentEmployee);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
                builder.show();
            }
        });

    }

    private boolean cekData(String inputText) {
        List<ModelEmployeeStatus> listStatus = db.daoAccessEmployeeStatus().getAllNoCondition();
        for (int i = 0; i < listStatus.size(); i++) {
            if (inputText.contentEquals(listStatus.get(i).getNamaemployee())) {
                return true;
            }

        }
        return false;
    }

    private void inputData() {
        Bundle bundle = this.getArguments();
        final ModelEmployeeStatus modelEmployeeStatus = (ModelEmployeeStatus) bundle.getSerializable("data");
        ModelEmployeeStatus update = new ModelEmployeeStatus();
        update.setId(modelEmployeeStatus.getId());
        update.setNamaemployee(nama.getText().toString());
        update.setNotes(notes.getText().toString());
        update.setIs_delete(false);
        db.daoAccessEmployeeStatus().updateEmployeeStatus(update);
        Fragment mainFragmentEmployee = new MainEmployeeStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainEmployeeStatusLayout, mainFragmentEmployee);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private void buttonAction(){
        Bundle bundle = this.getArguments();
        final ModelEmployeeStatus modelEmployeeStatus = (ModelEmployeeStatus) bundle.getSerializable("data");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelEmployeeStatus update = new ModelEmployeeStatus();
                update.setId(modelEmployeeStatus.getId());
                update.setNamaemployee(nama.getText().toString());
                update.setNotes(notes.getText().toString());
                update.setIs_delete(false);
                db.daoAccessEmployeeStatus().updateEmployeeStatus(update);
                Fragment mainFragmentEmployee = new MainEmployeeStatusFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.mainEmployeeStatusLayout, mainFragmentEmployee);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nama.setText(modelEmployeeStatus.getNamaemployee());
                notes.setText(modelEmployeeStatus.getNotes());
            }
        });
    }

}
