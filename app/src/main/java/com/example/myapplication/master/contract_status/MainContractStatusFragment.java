package com.example.myapplication.master.contract_status;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainContractStatusFragment extends Fragment {


    public MainContractStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_main_contract_status, container, false);
        getActivity().setTitle("Contract Status");
        TabLayout tablayout = (TabLayout) view.findViewById(R.id.tabLayoutContractStatus);
        ViewGroup viewGroup = (ViewGroup) tablayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1){
                viewGroupTab.setEnabled(false);
            }

        }
        Fragment fragmentData = new DataContractStatusFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainContractStatus, fragmentData);
        transaction.addToBackStack(null);
        transaction.commit();
        return view;
    }

}
