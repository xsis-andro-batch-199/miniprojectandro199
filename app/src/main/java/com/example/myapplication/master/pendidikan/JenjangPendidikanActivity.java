package com.example.myapplication.master.pendidikan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterPendidikan;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelPendidikan;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
public class JenjangPendidikanActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView buttonTambah;

    private List<ModelPendidikan> list = new ArrayList<>();
    private AdapterPendidikan pendidikanAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenjang_pendidikan);
        setupToolbar();
        setupView();
        loadData();
        showData();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        pendidikanAdapter = new AdapterPendidikan(this, list);
        recyclerView.setAdapter(pendidikanAdapter);
    }

    private void loadData() {
        db = Room.databaseBuilder(this, Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccess().getAllPendidikan();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rv_card);

        buttonTambah = findViewById(R.id.btn_tambah);
        buttonTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InputPendidikanActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Cari...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                db = Room.databaseBuilder(getApplicationContext(),
                        Database.class,
                        "xsis_project")
                        .allowMainThreadQueries()
                        .build();
                list = db.daoAccess().findByPendidikan(query);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                db = Room.databaseBuilder(getApplicationContext(),
                        Database.class,
                        "xsis_project")
                        .allowMainThreadQueries()
                        .build();
                list = db.daoAccess().findByPendidikan(newText);
                showData();
                return false;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
