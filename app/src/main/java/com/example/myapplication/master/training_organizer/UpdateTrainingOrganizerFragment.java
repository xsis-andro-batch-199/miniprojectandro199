package com.example.myapplication.master.training_organizer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTrainingOrganizer;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UpdateTrainingOrganizerFragment extends Fragment {

    TextView HeaderTrainingOrganizer;
    EditText etTOName, etNote;
    Button btnReset, btnSave;
    TextInputLayout requiredTO;
    ImageView btnDelete;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_training_organizer,container, false);
        setupView(view);
        loadData();
        return view;
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelTrainingOrganizer modelTrainingOrganizer = (ModelTrainingOrganizer) bundle.getSerializable("data");

        if (bundle != null){
            String nameTO = modelTrainingOrganizer.getTo_name();
            String noteTO = modelTrainingOrganizer.getNotes();
            etTOName.setText(nameTO);
            etNote.setText(noteTO);
        } else {

        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelTrainingOrganizer modelTrainingOrganizer = (ModelTrainingOrganizer) bundle.getSerializable("data");
        HeaderTrainingOrganizer = view.findViewById(R.id.tvAddEditTO);
        HeaderTrainingOrganizer.setText(getResources().getString(R.string.edit_TO));
        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelTrainingOrganizer.getTo_name()+" ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessTrainingOrganizer().delete_TO(modelTrainingOrganizer.getId());

                        Toast.makeText(getContext(), "Data terhapus", Toast.LENGTH_SHORT).show();
                        Fragment fragmentData = new DataTrainingOrganizerFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frameMainTrainingOrganizer, fragmentData);
                        transaction.commit();
                    }
                });
                builder.show();
            }
        });
        requiredTO = view.findViewById(R.id.required_TO);
        btnReset = view.findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTOName.setText(modelTrainingOrganizer.getTo_name());
                etNote.setText(modelTrainingOrganizer.getNotes());
            }
        });
        btnSave = view.findViewById(R.id.btnSave);
        etTOName = view.findViewById(R.id.etAddEditTO);
        etTOName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTOName.getText().toString();
                if (!modelTrainingOrganizer.getTo_name().equals(inputText)
                    && !etTOName.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText)){
                                etTOName.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelTrainingOrganizer updateModel = new ModelTrainingOrganizer();
                                updateModel.setId(modelTrainingOrganizer.getId());
                                updateModel.setTo_name(etTOName.getText().toString());
                                updateModel.setNotes(etNote.getText().toString());

                                db.daoAccessTrainingOrganizer().update_TO(updateModel);
                                Fragment fragment = new DataTrainingOrganizerFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.frameMainTrainingOrganizer, fragment);
                                transaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        etNote = view.findViewById(R.id.etNotesTO);
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNote.getText().toString();
                if (!modelTrainingOrganizer.getNotes().equals(inputText)
                    && !etTOName.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText)){
                                etTOName.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelTrainingOrganizer updateModel = new ModelTrainingOrganizer();
                                updateModel.setId(modelTrainingOrganizer.getId());
                                updateModel.setTo_name(etTOName.getText().toString());
                                updateModel.setNotes(etNote.getText().toString());

                                db.daoAccessTrainingOrganizer().update_TO(updateModel);
                                Fragment fragment = new DataTrainingOrganizerFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.frameMainTrainingOrganizer, fragment);
                                transaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutTrainingOrganizer);
        tabLayout.getTabAt(1).select();
    }

    public boolean cekData(String inputText){
        List<ModelTrainingOrganizer> list = db.daoAccessTrainingOrganizer().getDB_TO();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTo_name())){
                return true;
            }
        }
        return false;
    }
}
