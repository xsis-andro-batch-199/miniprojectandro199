package com.example.myapplication.master.position_level;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPositionLevel;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;


public class FormPositionLevelFragment extends Fragment {
    EditText etPositionName, etNote;
    Button btnReset, btnSave;
    TextInputLayout requiredPosition;
    ModelPositionLevel modelPositionLevel;
    ImageView btnDelete;

    public FormPositionLevelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_position_level, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        btnDelete           = view.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);
        requiredPosition    = view.findViewById(R.id.required_position_level);
        etPositionName      = view.findViewById(R.id.etAddEditPosition);
        etPositionName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredPosition.setErrorEnabled(true);
                requiredPosition.setError(getResources().getString(R.string.requiredText));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etPositionName.getText().toString();
                if (!inputText.isEmpty()){
                    requiredPosition.setErrorEnabled(false);
                    requiredPosition.setError(null);
                } else {
                    requiredPosition.setErrorEnabled(true);
                    requiredPosition.setError(getResources().getString(R.string.requiredText));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etPositionName.getText().toString();
                if (inputText.length() > 0){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etPositionName.getText().toString().isEmpty()){
                                if (cekData(inputText)){
                                    etPositionName.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    modelPositionLevel = new ModelPositionLevel();
                                    modelPositionLevel.setPosition_name(etPositionName.getText().toString());
                                    modelPositionLevel.setNotes(etNote.getText().toString());

                                    db.daoAccessPositionLevel().insertPositionLevel(modelPositionLevel);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragment = new DataPositionLevelFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frameMainPositionLevel, fragment);
                                    transaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }
            }
        });
        etNote = view.findViewById(R.id.etNotesPositon);
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnSave.setEnabled(false);
                btnReset.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etNote.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnReset = view.findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPositionName.setText(null);
                etNote.setText(null);
            }
        });
        btnSave = view.findViewById(R.id.btnSave);
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutPositionLevel);
        tabLayout.getTabAt(1).select();
    }

    public boolean cekData(String inputText){
        List<ModelPositionLevel> list = db.daoAccessPositionLevel().getDBPositionLevel();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPosition_name())){
                return true;
            }
        }
        return false;
    }

}