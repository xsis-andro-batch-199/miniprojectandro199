package com.example.myapplication.master.training;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTraining;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class FormTrainingFragment extends Fragment {

    EditText etTrainingCode, etTrainingName;
    Button btnReset, btnSave;
    TextInputLayout requiredCode, requiredName;
    ModelTraining modelTraining;
    ImageView btnDelete;

    public FormTrainingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_training, container, false);
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);
        requiredCode = view.findViewById(R.id.required_training_code);
        requiredName = view.findViewById(R.id.required_training_name);
        etTrainingCode = view.findViewById(R.id.etAddEditCodeTraining);
        etTrainingName = view.findViewById(R.id.etAddEditTrainingName);
        btnReset = view.findViewById(R.id.btnReset);
        btnSave = view.findViewById(R.id.btnSave);

        etTrainingCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredCode.setError(getResources().getString(R.string.requiredText));
                requiredCode.setErrorEnabled(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etTrainingCode.getText().toString();
                if (!inputText.isEmpty()){
                    requiredCode.setErrorEnabled(false);
                    requiredCode.setError(null);
                } else {
                    requiredCode.setError(getResources().getString(R.string.requiredText));
                    requiredCode.setErrorEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTrainingCode.getText().toString();
                final String inputText2 = etTrainingName.getText().toString();
                if (inputText.length() > 0 && inputText2.length() > 0){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etTrainingCode.getText().toString().isEmpty()){
                                if (cekData(inputText) || cekData(inputText2)){
                                    /*etTrainingCode.setError(getResources().getString(R.string.data_ada));
                                    etTrainingName.setError(getResources().getString(R.string.data_ada));*/
                                    if (cekData(inputText) == true){
                                        etTrainingCode.setError(getResources().getString(R.string.data_ada));
                                    } else {
                                        etTrainingName.setError(getResources().getString(R.string.data_ada));
                                    }
                                } else {
                                    modelTraining = new ModelTraining();
                                    modelTraining.setTraining_code(etTrainingCode.getText().toString());
                                    modelTraining.setTraining_name(etTrainingName.getText().toString());

                                    db.daoAccessTraining().insert_Training(modelTraining);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragment = new DataTrainingFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frameMainTraining, fragment);
                                    transaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }
            }
        });
        etTrainingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requiredName.setError(getResources().getString(R.string.requiredText));
                requiredName.setErrorEnabled(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etTrainingName.getText().toString();
                if (!inputText.isEmpty()){
                    requiredName.setErrorEnabled(false);
                    requiredName.setError(null);
                } else {
                    requiredName.setError(getResources().getString(R.string.requiredText));
                    requiredName.setErrorEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTrainingName.getText().toString();
                final String inputText2 = etTrainingCode.getText().toString();
                if (inputText.length() > 0 && inputText2.length() > 0){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etTrainingName.getText().toString().isEmpty()){
                                if (cekData(inputText) || cekData(inputText2)){
                                    /*etTrainingName.setError(getResources().getString(R.string.data_ada));
                                    etTrainingCode.setError(getResources().getString(R.string.data_ada));*/
                                    if (cekData(inputText) == true){
                                        etTrainingName.setError(getResources().getString(R.string.data_ada));
                                    } else {
                                        etTrainingCode.setError(getResources().getString(R.string.data_ada));
                                    }
                                } else {
                                    modelTraining = new ModelTraining();
                                    modelTraining.setTraining_code(etTrainingCode.getText().toString());
                                    modelTraining.setTraining_name(etTrainingName.getText().toString());

                                    db.daoAccessTraining().insert_Training(modelTraining);
                                    Toast.makeText(getContext(), "Terdaftar", Toast.LENGTH_SHORT).show();

                                    Fragment fragment = new DataTrainingFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frameMainTraining, fragment);
                                    transaction.commit();
                                }
                            }
                        }
                    });
                } else {
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTrainingName.setText(null);
                etTrainingCode.setText(null);
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutTraining);
        tabLayout.getTabAt(1).select();

    }

    public boolean cekData(String inputText){
        List<ModelTraining> list = db.daoAccessTraining().getDB_Training();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTraining_code())
                || inputText.contentEquals(list.get(i).getTraining_name())){
                return true;
            }
        }
        return false;
    }

}
