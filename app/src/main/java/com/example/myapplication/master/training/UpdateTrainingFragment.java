package com.example.myapplication.master.training;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterTraining;
import com.example.myapplication.model.ModelTraining;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UpdateTrainingFragment extends Fragment {

    TextView HeaderTraining;
    EditText etTrainingCode, etTrainingName;
    Button btnReset, btnSave;
    TextInputLayout requiredCode, requiredName;
    ImageView btnDelete;

    public UpdateTrainingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_training, container, false);
        setupView(view);
        loadData();
        return view;
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelTraining modelTraining = (ModelTraining) bundle.getSerializable("data");

        if (bundle != null){
            String codeT = modelTraining.getTraining_code();
            String nameT = modelTraining.getTraining_name();
            etTrainingCode.setText(codeT);
            etTrainingName.setText(nameT);
        } else {

        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelTraining modelTraining = (ModelTraining) bundle.getSerializable("data");
        HeaderTraining = view.findViewById(R.id.tvAddEditTraining);
        HeaderTraining.setText(getResources().getString(R.string.edit_training));
        btnDelete = view.findViewById(R.id.btnDelete);
        requiredCode = view.findViewById(R.id.required_training_code);
        requiredName = view.findViewById(R.id.required_training_name);
        etTrainingCode = view.findViewById(R.id.etAddEditCodeTraining);
        etTrainingName = view.findViewById(R.id.etAddEditTrainingName);
        btnReset = view.findViewById(R.id.btnReset);
        btnSave = view.findViewById(R.id.btnSave);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus" + modelTraining.getTraining_name()+" ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessTraining().delete_Training(modelTraining.getId());

                        Toast.makeText(getContext(), "Data terhapus", Toast.LENGTH_SHORT).show();
                        Fragment fragment = new DataTrainingFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frameMainTraining, fragment);
                        transaction.commit();
                    }
                });
                builder.show();
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etTrainingCode.setText(modelTraining.getTraining_code());
                etTrainingName.setText(modelTraining.getTraining_name());
            }
        });
        etTrainingCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTrainingCode.getText().toString();
                final String inputText2 = etTrainingName.getText().toString();
                if (!modelTraining.getTraining_code().equals(inputText)
                    && !etTrainingCode.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText) || cekData(inputText2)){
                                /*etTrainingCode.setError(getResources().getString(R.string.data_ada));*/
                                if (cekData(inputText) == true){
                                    etTrainingCode.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    etTrainingName.setError(getResources().getString(R.string.data_ada));
                                }
                            } else {
                                ModelTraining updateModel = new ModelTraining();
                                updateModel.setId(modelTraining.getId());
                                updateModel.setTraining_code(etTrainingCode.getText().toString());
                                updateModel.setTraining_name(etTrainingName.getText().toString());

                                db.daoAccessTraining().update_Training(updateModel);
                                Fragment fragment = new DataTrainingFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.frameMainTraining, fragment);
                                transaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        etTrainingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etTrainingName.getText().toString();
                final String inputText2 = etTrainingCode.getText().toString();
                if (!modelTraining.getTraining_name().equals(inputText)
                    && !etTrainingName.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekData(inputText) || cekData(inputText2)){
                                /*etTrainingName.setError(getResources().getString(R.string.data_ada));*/
                                if (cekData(inputText) == true){
                                    etTrainingName.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    etTrainingCode.setError(getResources().getString(R.string.data_ada));
                                }
                            } else {
                                ModelTraining updateModel = new ModelTraining();
                                updateModel.setId(modelTraining.getId());
                                updateModel.setTraining_code(etTrainingCode.getText().toString());
                                updateModel.setTraining_name(etTrainingName.getText().toString());

                                db.daoAccessTraining().update_Training(updateModel);
                                Fragment fragment = new DataTrainingFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.frameMainTraining, fragment);
                                transaction.commit();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutTraining);
        tabLayout.getTabAt(1).select();
    }

    public boolean cekData(String inputText){
        List<ModelTraining> list = db.daoAccessTraining().getDB_Training();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTraining_code())
                    || inputText.contentEquals(list.get(i).getTraining_name())){
                return true;
            }
        }
        return false;
    }

}
