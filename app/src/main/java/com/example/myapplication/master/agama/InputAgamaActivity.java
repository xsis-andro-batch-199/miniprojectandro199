package com.example.myapplication.master.agama;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelAgama;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputAgamaActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private EditText editAgama, editDeskripsiAgama;
    private Button btnBatalAgama, btnSimpanAgama;
    private TextInputLayout pesan_error;
    private ModelAgama modelAgama;
    private TextView idTitleAgama;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_agama);
        setupView();
        setToolbar();
        idTitleAgama.setText(getResources().getString(R.string.agama));
    }

    private void setupView() {
        idTitleAgama = findViewById(R.id.idTitleAgama);
        editAgama = findViewById(R.id.edit_agama);
        editDeskripsiAgama = findViewById(R.id.edit_deskripsi_agama);
        btnBatalAgama = findViewById(R.id.button_batal_agama);
        btnSimpanAgama = findViewById(R.id.button_simpan_agama);
        pesan_error = findViewById(R.id.pesan_error);
        btnBatalAgama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputAgamaActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                finish();
            }
        });
        validasi();
    }

    private void validasi() {
        pesanErrorInputAgama();
        editAgama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                pesanErrorInputAgama();
            }
        });
        editAgama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesan_error.setErrorEnabled(true);
                pesan_error.setError(getResources().getString(R.string.error_agama));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = editAgama.getText().toString();
                if (!inputText.isEmpty()){
                    pesan_error.setErrorEnabled(false);
                    pesan_error.setError(null);
                } else {
                    pesan_error.setErrorEnabled(true);
                    pesan_error.setError(getResources().getString(R.string.error_agama));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String valueNama = editAgama.getText().toString();
                final String valueDesc = editDeskripsiAgama.getText().toString();
                if (valueNama.length() > 0 ){
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanAgama.setEnabled(true);
                    btnSimpanAgama.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!valueNama.isEmpty()){
                                if (cekDataNama(valueNama)){
                                    editAgama.setError("Data sudah ada");
                                } else {
                                    modelAgama = new ModelAgama();
                                    modelAgama.setAgama(editAgama.getText().toString());
                                    modelAgama.setDeskripsi(editDeskripsiAgama.getText().toString());
                                    modelAgama.setIs_delete(false);

                                    db.daoAccess().insertAgama(modelAgama);
                                    Toast.makeText(InputAgamaActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                                    finish();
                                }
                            } else {
                                Toast.makeText(InputAgamaActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanAgama.setEnabled(false);
                }
        }
        });
    }

    private boolean cekDataNama(String valueCek) {
        List<ModelAgama> list =db.daoAccess().getAllAgamaNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (valueCek.contentEquals(list.get(i).getAgama())) {
                return true;
            }
        }
        return false;
    }

    private void pesanErrorInputAgama() {
        if (TextUtils.isEmpty(editAgama.getText())){
            pesan_error.setErrorEnabled(true);
            pesan_error.setError(getResources().getString(R.string.error_agama));
            pesan_error.setHint("Agama");
        } else {
            pesan_error.setErrorEnabled(false);
            pesan_error.setHint("Agama");
            editAgama.setHint("Agama");
        }
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

/*private void setupView() {
        editAgama = findViewById(R.id.edit_agama);
        editDeskripsiAgama = findViewById(R.id.edit_deskripsi_agama);

        btnBatalAgama = findViewById(R.id.button_batal_agama);
        btnBatalAgama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputAgamaActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                finish();
            }
        });

        btnSimpanAgama = findViewById(R.id.button_simpan_agama);

        pesan_error = findViewById(R.id.pesan_error);

        editAgama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesan_error.setErrorEnabled(true);
                pesan_error.setError(getResources().getString(R.string.error_agama));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = editAgama.getText().toString();
                if (!inputText.isEmpty()){
                    pesan_error.setErrorEnabled(false);
                    pesan_error.setError(null);
                } else {
                    pesan_error.setErrorEnabled(true);
                    pesan_error.setError(getResources().getString(R.string.error_agama));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String inputText = editAgama.getText().toString();
                if (inputText.length() > 0 ){
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanAgama.setEnabled(true);
                    btnSimpanAgama.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!editAgama.getText().toString().isEmpty()){
                                modelAgama = new ModelAgama();
                                modelAgama.setAgama(editAgama.getText().toString());
                                modelAgama.setDeskripsi(editDeskripsiAgama.getText().toString());

                                db.daoAccess().insertAgama(modelAgama);
                                Toast.makeText(InputAgamaActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                                finish();
                            } else {
                                Toast.makeText(InputAgamaActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanAgama.setEnabled(false);
                }
            }
        });
    }*/