package com.example.myapplication.master.employee_type;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class HolderEmployeeTypeFragment extends Fragment {


    public HolderEmployeeTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_holder_employee_type, container, false);
        setupView();
        setupTabLayout(view);

        return view;
    }

    private void setupView() {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Employee Type");
    }

    private void setupTabLayout(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tab_layout_et);
        tabLayout.addTab(tabLayout.newTab().setText("DATA"));
        tabLayout.addTab(tabLayout.newTab().setText("FORM"));

        Fragment fragmentData = new DataEmployeeTypeFragment();
        loadFragment(fragmentData);

        //noinspection deprecation
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            Fragment fragmentData = new DataEmployeeTypeFragment();
            Fragment fragmentForm = new FormEmployeeTypeFragment();
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    loadFragment(fragmentData);
                }
                if (tab.getPosition() == 1){
                    loadFragment(fragmentForm);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_et, fragment);
        transaction.commit();
    }
}
