package com.example.myapplication.master.tipe_identitas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahTipeIdentitasActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText editTipeIdentitas, editDeskripsi;
    private Button btnBatal, btnSimpan;
    private TextInputLayout textInputLayout;

    ModelTipeIdentitas modelTipeIdentitas;

    public static String EXTRA_DATA = "extra_data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_tipe_identitas);

        modelTipeIdentitas = (ModelTipeIdentitas) getIntent().getSerializableExtra(EXTRA_DATA);

        setupView();
        loadData();
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.ubah_tipe_identitas));
        /*toolbar.setOverflowIcon(getDrawable(R.drawable.ic_more_vert_white_24dp));*/
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout_ubah_identitas);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setupView() {
        editTipeIdentitas = findViewById(R.id.edit_update_identitas);
        editDeskripsi = findViewById(R.id.edit_update_deskripsi);

        btnBatal = findViewById(R.id.button_batal_update_identitas);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TipeIdentitasActivity.class));
                finish();
            }
        });

        textInputLayout = findViewById(R.id.inputLayoutUbahIdentitas);
        textInputLayout.setErrorEnabled(true);
        editTipeIdentitas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (TextUtils.isEmpty(editTipeIdentitas.getText())){
                    textInputLayout.setErrorEnabled(true);
                    textInputLayout.setError(getResources().getString(R.string.error_identitas));
                    textInputLayout.setHint("Tipe Identitas *");
                } else {
                    textInputLayout.setErrorEnabled(false);
                    textInputLayout.setHint("Tipe Identitas *");
                    editTipeIdentitas.setHint("Tipe Identitas");
                }
            }
        });

        btnSimpan = findViewById(R.id.button_simpan_update_identitas);

        editDeskripsi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputDeskripsi = editDeskripsi.getText().toString();
                if (!modelTipeIdentitas.getDeskripsi().equals(inputDeskripsi)
                        && !editDeskripsi.getText().toString().isEmpty()){
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelTipeIdentitas ubahIdentitas = new ModelTipeIdentitas();
                            ubahIdentitas.setId(modelTipeIdentitas.getId());
                            ubahIdentitas.setTipe_identitas(editTipeIdentitas.getText().toString());
                            ubahIdentitas.setDeskripsi(editDeskripsi.getText().toString());
                            ubahIdentitas.setDelete(false);

                            db.daoAccess().updateIdentitas(ubahIdentitas);
                            startActivity(new Intent(getApplicationContext(),TipeIdentitasActivity.class));
                            finish();
                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }
            }
        });

        editTipeIdentitas.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputTipeIdentitas = editTipeIdentitas.getText().toString();
                if (editTipeIdentitas.getText().toString().isEmpty()){
                    textInputLayout.setError(getString(R.string.error_identitas));
                }else{

                }
                if (!modelTipeIdentitas.getTipe_identitas().equals(inputTipeIdentitas)
                        && !editTipeIdentitas.getText().toString().isEmpty()){
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekNama(inputTipeIdentitas)){
                                textInputLayout.setError(getString(R.string.data_ada));
                            }else {
                                ModelTipeIdentitas ubahIdentitas = new ModelTipeIdentitas();
                                ubahIdentitas.setId(modelTipeIdentitas.getId());
                                ubahIdentitas.setTipe_identitas(editTipeIdentitas.getText().toString());
                                ubahIdentitas.setDeskripsi(editDeskripsi.getText().toString());
                                ubahIdentitas.setDelete(false);

                                db.daoAccess().updateIdentitas(ubahIdentitas);
                                startActivity(new Intent(getApplicationContext(),TipeIdentitasActivity.class));
                                finish();
                            }

                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }

            }
        });
    }

    private void loadData() {
        editTipeIdentitas.setText(modelTipeIdentitas.getTipe_identitas());
        editDeskripsi.setText(modelTipeIdentitas.getDeskripsi());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_ubah_identitas);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), TipeIdentitasActivity.class));
            finish();
        }
    }

    private boolean cekNama(String inputText) {
        List<ModelTipeIdentitas> list = db.daoAccess().getAllIdentitasNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTipe_identitas())){
                return true;
            }
        }
        return false;
    }
}
