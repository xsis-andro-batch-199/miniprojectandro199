package com.example.myapplication.master.training;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

public class MainTrainingFragment extends Fragment {

    public MainTrainingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_training, container, false);
        setupTabLayout(view);
        getActivity().setTitle(getResources().getString(R.string.training));
        return view;
    }

    private void setupTabLayout(View view) {
        TabLayout tabLayout = view.findViewById(R.id.tabLayoutTraining);
        tabLayout.addTab(tabLayout.newTab().setText("DATA"));
        tabLayout.addTab(tabLayout.newTab().setText("FORM"));

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for (int i = 0; i < tabCount; i++){
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(i);
            if (i == 1 || i == 0){
                viewGroupTab.setEnabled(false);
            }
        }
        Fragment fragment = new DataTrainingFragment();
        loadFragment(fragment);
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameMainTraining, fragment);
        transaction.commit();
    }

}
