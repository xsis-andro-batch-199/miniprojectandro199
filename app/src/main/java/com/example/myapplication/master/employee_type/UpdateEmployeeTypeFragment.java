package com.example.myapplication.master.employee_type;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeType;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import static com.example.myapplication.AppController.db;

public class UpdateEmployeeTypeFragment extends Fragment {
    private TextView textTitle;
    private EditText etName, etNotes;
    private TextInputLayout tilName, tilNotes;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;
    private ImageView buttonDelete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_employee_type, container, false);
        setupView(view);
        receiveData();
        setupAction();
        setupButton();

        return view;    }

    private void setupView(View view) {
        textTitle = view.findViewById(R.id.tv_title_et);
        textTitle.setText("Edit Employee Type");

        etName  = view.findViewById(R.id.et_name_et);
        etNotes = view.findViewById(R.id.et_notes_et);

        tilName  = view.findViewById(R.id.til_name_et);
        tilNotes = view.findViewById(R.id.til_notes_et);

        buttonReset     = view.findViewById(R.id.btn_reset_et);
        buttonSave      = view.findViewById(R.id.btn_save_et);
        buttonDelete    = view.findViewById(R.id.btn_delete_et);
        buttonDelete.setVisibility(View.VISIBLE);

        tabLayout   = getActivity().findViewById(R.id.tab_layout_et);
    }

    private void receiveData() {
        Bundle bundle = this.getArguments();
        ModelEmployeeType modelEmployeeType = (ModelEmployeeType) bundle.getSerializable("data");

        etName.setText(modelEmployeeType.getName());
        etNotes.setText(modelEmployeeType.getNotes());
    }

    private void setupAction() {
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton() {
        Bundle bundle = this.getArguments();
        final ModelEmployeeType modelEmployeeType = (ModelEmployeeType) bundle.getSerializable("data");

        final String inputName  = etName.getText().toString();
        final String inputNotes = etNotes.getText().toString();

        if ((!inputName.equals(modelEmployeeType.getName()) || !inputNotes.equals(modelEmployeeType.getNotes()))
                && !inputName.isEmpty()
                && !inputNotes.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FormEmployeeTypeFragment fetf = new FormEmployeeTypeFragment();
                if (inputName.equals(modelEmployeeType.getName())){
                    updateData(modelEmployeeType);
                    tabLayout.getTabAt(0).select();
                } else if (!inputName.equals(modelEmployeeType.getName())){
                    if (fetf.cekName(inputName)){
                        tilName.setError(getString(R.string.data_already_exist));
                    } else {
                        updateData(modelEmployeeType);
                        tabLayout.getTabAt(0).select();
                    }
                }
            }
        });

        if (!inputName.equals(modelEmployeeType.getName())
                || !inputNotes.equals(modelEmployeeType.getNotes())){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etName.setText(modelEmployeeType.getName());
                etNotes.setText(modelEmployeeType.getNotes());
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelEmployeeType.getName() + " ?");
                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessEmployeeType().deleteViewEmployeeType(modelEmployeeType.getId());
                        tabLayout.getTabAt(0).select();
                    }
                });
                builder.show();
            }
        });
    }

    private void updateData(ModelEmployeeType modelEmployeeType){
        String inputName  = etName.getText().toString();
        String inputNotes = etNotes.getText().toString();

        ModelEmployeeType ubahModelEmployeeType = new ModelEmployeeType();
        ubahModelEmployeeType.setId(modelEmployeeType.getId());

        ubahModelEmployeeType.setName(inputName);
        ubahModelEmployeeType.setNotes(inputNotes);
        ubahModelEmployeeType.setDelete(false);
        db.daoAccessEmployeeType().updateEmployeeType(ubahModelEmployeeType);
    }
}
