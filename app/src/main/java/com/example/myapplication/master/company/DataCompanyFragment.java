package com.example.myapplication.master.company;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.CompanyAdapter;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelCompany;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataCompanyFragment extends Fragment {
    private RecyclerView recyclerView;
    private CompanyAdapter adapter;
    private List<ModelCompany> list = new ArrayList<>();

    private ImageView buttonTambah;
    private TabLayout tabLayout;

    public DataCompanyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_company, container, false);
        setupView(view);
        setupButton();
        loadData();
        showData();
        return view;
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        recyclerView = view.findViewById(R.id.rv_company);
        buttonTambah = view.findViewById(R.id.btn_tambah);
        tabLayout = getActivity().findViewById(R.id.tab_layout_company);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessCompany().findByCompany(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessCompany().findByCompany(s);
                showData();
                return true;
            }
        });
    }

    private void setupButton(){
        buttonTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabLayout.getTabAt(1).select();
            }
        });
    }

    private void loadData(){
        db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessCompany().getViewCompany();
    }

    private void showData(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new CompanyAdapter(getContext(), list);
        recyclerView.setAdapter(adapter);
    }
}
