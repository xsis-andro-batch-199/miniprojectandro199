package com.example.myapplication.master.periode;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterPeriode;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelPeriode;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class PeriodeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView btnTambahPeriode, imgCariPeriode;
    private List<ModelPeriode> list = new ArrayList<>();
    private AdapterPeriode adapterPeriode;
    private RecyclerView recyclerView;
    private EditText etCariPeriode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periode);
        setupDrawer();
        setupView();
        loadData();
        showData();
    }

    private void setupDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbarPeriode);
        setSupportActionBar(toolbar);
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayoutPeriode);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvPeriode);
        btnTambahPeriode = findViewById(R.id.btnTambahPeriode);
        etCariPeriode = findViewById(R.id.etCariPeriode);
        btnTambahPeriode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InputPeriodeActivity.class));
                finish();
            }
        });

        etCariPeriode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                return false;
            }
        });
        etCariPeriode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadData();
                showData();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etCariPeriode.getText().toString();
                if (inputText.isEmpty()){
                    recyclerView.setAdapter(adapterPeriode);
                } else {
                    db = Room.databaseBuilder(getApplicationContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                    list = db.daoAccessPeriode().findByPeriode(inputText);
                    showData();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void loadData() {
        db = Room.databaseBuilder(this, Database.class, "xsis_project").allowMainThreadQueries().build();
        list = db.daoAccessPeriode().getAllPeriode();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapterPeriode = new AdapterPeriode(this, list);
        recyclerView.setAdapter(adapterPeriode);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
