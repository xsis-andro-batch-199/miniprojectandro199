package com.example.myapplication.master.employee_type;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.OnBackPressed;
import com.example.myapplication.OnBackPressedFragment;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeType;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormEmployeeTypeFragment extends Fragment implements OnBackPressedFragment {
    private EditText etName, etNotes;
    private TextInputLayout tilName, tilNotes;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;

    public FormEmployeeTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_employee_type, container, false);
        setupView(view);
        setupAction();

        return view;
    }

    private void setupView(View view) {
        setHasOptionsMenu(true);
        etName  = view.findViewById(R.id.et_name_et);
        etNotes = view.findViewById(R.id.et_notes_et);

        tilName  = view.findViewById(R.id.til_name_et);
        tilNotes = view.findViewById(R.id.til_notes_et);

        buttonReset = view.findViewById(R.id.btn_reset_et);
        buttonSave  = view.findViewById(R.id.btn_save_et);

        tabLayout   = getActivity().findViewById(R.id.tab_layout_et);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void setupAction() {
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton() {
        final String inputName  = etName.getText().toString();
        final String inputNotes = etNotes.getText().toString();

        if (!inputName.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cekName(inputName)){
                    tilName.setError(getString(R.string.data_already_exist));
                } else {
                    ModelEmployeeType modelEmployeeType = new ModelEmployeeType();
                    modelEmployeeType.setName(inputName);
                    modelEmployeeType.setNotes(inputNotes);
                    modelEmployeeType.setDelete(false);
                    db.daoAccessEmployeeType().insertEmployeeType(modelEmployeeType);

                    tabLayout.getTabAt(0).select();
                }
            }
        });

        if (!inputName.isEmpty() || !inputNotes.isEmpty()){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etName.setText(null);
                etNotes.setText(null);
            }
        });
    }

    protected boolean cekName(String inputName) {
        List<ModelEmployeeType> list = db.daoAccessEmployeeType().getDBEmployeeType();
        for (int i=0; i<list.size(); i++){
            if (inputName.contentEquals(list.get(i).getName())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onBackPressed() {
        tabLayout.getTabAt(0).select();
        return true;
    }
}
