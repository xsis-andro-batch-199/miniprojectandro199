package com.example.myapplication.master.periode;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelPeriode;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahPeriodeActivity extends AppCompatActivity {

    private EditText etPeriode, etDeskripPeriode;
    private Button btnBatalPeriode, btnSimpanPeriode;
    private TextInputLayout pesanError, tilDeskripsi;
    ModelPeriode periodeModel;
    public static final String EXTRA_DATA = "extra_data";
    private TextView headetPeriode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_periode);
        periodeModel = (ModelPeriode) getIntent().getSerializableExtra(EXTRA_DATA);
        setupView();
        loadData();
    }

    private void setupView() {
        etPeriode = findViewById(R.id.etinPeriode);
        etDeskripPeriode = findViewById(R.id.etinDeskripsiP);
        pesanError = findViewById(R.id.errorPeriode);
        pesanError.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPeriode.setText(periodeModel.getPeriode());
            }
        });
        tilDeskripsi = findViewById(R.id.tilDeskripsi);
        tilDeskripsi.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDeskripPeriode.setText(periodeModel.getDeskripsi());
            }
        });
        btnBatalPeriode = findViewById(R.id.btnBatalPeriode);
        btnSimpanPeriode = findViewById(R.id.btnSimpanPeriode);
        headetPeriode = findViewById(R.id.tvInputPeriode);
        headetPeriode.setText(getResources().getString(R.string.ubahPeriode));

        etDeskripPeriode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etDeskripPeriode.getText().toString();
                if (!periodeModel.getDeskripsi().equals(inputText)
                    && !etPeriode.getText().toString().isEmpty()){
                    btnSimpanPeriode.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanPeriode.setEnabled(true);
                    btnSimpanPeriode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etPeriode.getText().toString().isEmpty()){
                                ModelPeriode modelPeriode = new ModelPeriode();
                                modelPeriode.setId(periodeModel.getId());
                                modelPeriode.setDeskripsi(etDeskripPeriode.getText().toString());
                                modelPeriode.setPeriode(etPeriode.getText().toString());
                                db.daoAccessPeriode().updatePeriode(modelPeriode);
                                startActivity(new Intent(getApplicationContext(), PeriodeActivity.class));
                                finish();

                            } else {
                                Toast.makeText(UbahPeriodeActivity.this, "Lengkapi data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpanPeriode.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanPeriode.setEnabled(false);
                }

            }
        });

        etPeriode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesanError.setErrorEnabled(false);
                pesanError.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etPeriode.getText().toString();
                if (inputText.isEmpty()){
                    pesanError.setErrorEnabled(true);
                    pesanError.setError(getResources().getString(R.string.errorPeriode));
                } else {
                    pesanError.setErrorEnabled(false);
                    pesanError.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etPeriode.getText().toString();
                if (!periodeModel.getPeriode().equals(inputText)
                        && !etPeriode.getText().toString().isEmpty()){
                    btnSimpanPeriode.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanPeriode.setEnabled(true);
                    btnSimpanPeriode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelPeriode modelPeriode = new ModelPeriode();
                            modelPeriode.setId(periodeModel.getId());
                            modelPeriode.setPeriode(etPeriode.getText().toString());
                            modelPeriode.setDeskripsi(etDeskripPeriode.getText().toString());

                            if (cekPeriodeData(inputText)){
                                etPeriode.setError(getResources().getString(R.string.data_ada));
                            } else {
                                db.daoAccessPeriode().updatePeriode(modelPeriode);
                                startActivity(new Intent(getApplicationContext(), PeriodeActivity.class));
                                finish();

                            }
                        }
                    });
                } else {
                    btnSimpanPeriode.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanPeriode.setEnabled(false);
                }

            }
        });

        btnBatalPeriode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PeriodeActivity.class));
                finish();
            }
        });
    }

    private void loadData() {
        etPeriode.setText(periodeModel.getPeriode());
        etDeskripPeriode.setText(periodeModel.getDeskripsi());
    }

    private boolean cekPeriodeData(String inputText){
        List<ModelPeriode> list = db.daoAccessPeriode().getAllPeriodeNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPeriode())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, PeriodeActivity.class));
        finish();
    }
}
