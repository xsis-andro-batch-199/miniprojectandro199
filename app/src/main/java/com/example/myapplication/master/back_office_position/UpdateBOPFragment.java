package com.example.myapplication.master.back_office_position;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelBOP;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import static com.example.myapplication.AppController.db;

public class UpdateBOPFragment extends Fragment {
    private TextView textTitle;
    private AutoCompleteTextView actvPositionLevel, actvCompany;
    private EditText etCode, etName, etPositionLevel, etCompany, etNotes;
    private TextInputLayout tilCode, tilName, tilPositionLevel, tilCompany, tilNotes;
    private Button buttonReset, buttonSave;

    private TabLayout tabLayout;
    private ImageView buttonDelete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_bop, container, false);
        setupView(view);
        receiveData();
        setupAction();
        setupButton();

        return view;
    }

    private void setupView(View view) {
        textTitle = view.findViewById(R.id.tv_title_bop);
        textTitle.setText("Edit Back Office Position");

        actvPositionLevel = view.findViewById(R.id.actv_position_level_bop);
        actvCompany       = view.findViewById(R.id.actv_company_bop);

        etCode          = view.findViewById(R.id.et_code_bop);
        etName          = view.findViewById(R.id.et_name_bop);
//        etPositionLevel = view.findViewById(R.id.et_position_level_bop);
//        etCompany       = view.findViewById(R.id.et_company_bop);
        etNotes         = view.findViewById(R.id.et_notes_bop);

        buttonReset     = view.findViewById(R.id.btn_reset_bop);
        buttonSave      = view.findViewById(R.id.btn_save_bop);
        buttonDelete    = view.findViewById(R.id.btn_delete_bop);
        buttonDelete.setVisibility(View.VISIBLE);

        tilCode         = view.findViewById(R.id.til_code_bop);
        tilName         = view.findViewById(R.id.til_name_bop);
        tilPositionLevel= view.findViewById(R.id.til_position_level_bop);
        tilCompany      = view.findViewById(R.id.til_company_bop);
        tilNotes        = view.findViewById(R.id.til_notes_bop);

        tabLayout = getActivity().findViewById(R.id.tab_layout_bop);
    }

    private void receiveData(){
        Bundle bundle = this.getArguments();
        ModelBOP modelBOP = (ModelBOP) bundle.getSerializable("data");

        etCode.setText(modelBOP.getCode());
        etName.setText(modelBOP.getName());
//        etPositionLevel.setText(modelBOP.getPositionLevel());
        actvPositionLevel.setText(modelBOP.getPositionLevel());
//        etCompany.setText(modelBOP.getCompany());
        actvCompany.setText(modelBOP.getCompany());
        etNotes.setText(modelBOP.getNotes());
    }

    private void setupAction() {
        Bundle bundle = this.getArguments();
        final ModelBOP modelBOP = (ModelBOP) bundle.getSerializable("data");

        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCode.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCode.setText(modelBOP.getCode());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCode.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCode.setText(modelBOP.getCode());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilName.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etName.setText(modelBOP.getName());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilName.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etName.setText(modelBOP.getName());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

/*        etPositionLevel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilPositionLevel.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etPositionLevel.setText(modelBOP.getPositionLevel());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilPositionLevel.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etPositionLevel.setText(modelBOP.getPositionLevel());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });*/
        String[] positionLevelArray = getResources().getStringArray(R.array.array_position_level_bop);
        ArrayAdapter<String> adapterPositionLevel = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, positionLevelArray);
        actvPositionLevel.setThreshold(0);
        actvPositionLevel.setAdapter(adapterPositionLevel);
        actvPositionLevel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilPositionLevel.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        actvPositionLevel.setText(modelBOP.getPositionLevel());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilPositionLevel.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        actvPositionLevel.setText(modelBOP.getPositionLevel());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

/*        etCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCompany.setText(modelBOP.getCompany());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etCompany.setText(modelBOP.getCompany());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });*/
        String[] companyArray = getResources().getStringArray(R.array.client_timesheet);
        ArrayAdapter<String> adapterCompany = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, companyArray);
        actvCompany.setThreshold(0);
        actvCompany.setAdapter(adapterCompany);
        actvCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        actvCompany.setText(modelBOP.getCompany());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilCompany.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        actvCompany.setText(modelBOP.getCompany());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });

        etNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilNotes.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etNotes.setText(modelBOP.getNotes());
                    }
                });
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tilNotes.setEndIconOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etNotes.setText(modelBOP.getNotes());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setupButton();
            }
        });
    }

    private void setupButton(){
        Bundle bundle = this.getArguments();
        final ModelBOP modelBOP = (ModelBOP) bundle.getSerializable("data");

        final String inputCode          = etCode.getText().toString();
        final String inputName          = etName.getText().toString();
//        final String inputPositionLevel = etPositionLevel.getText().toString();
        final String inputPositionLevel = actvPositionLevel.getText().toString();
//        final String inputCompany       = etCompany.getText().toString();
        final String inputCompany       = actvCompany.getText().toString();
        final String inputNotes         = etNotes.getText().toString();

        if ((!inputCode.equals(modelBOP.getCode())
            || !inputName.equals(modelBOP.getName())
            || !inputCompany.equals(modelBOP.getCompany())
            || !inputPositionLevel.equals(modelBOP.getPositionLevel())
            || !inputNotes.equals(modelBOP.getNotes()))
                && !inputCode.isEmpty()
                && !inputName.isEmpty()
                && !inputCompany.isEmpty()){
            buttonSave.setEnabled(true);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.biru));
        } else {
            buttonSave.setEnabled(false);
            buttonSave.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FormBOPFragment fbf = new FormBOPFragment();

                if (inputCode.equals(modelBOP.getCode())) {
                    if (inputName.equals(modelBOP.getName())) {
                        updateData(modelBOP);
                        tabLayout.getTabAt(0).select();
                    } else if (!inputName.equals(modelBOP.getName())) {
                        if (fbf.cekName(inputName)) {
                            etName.setError(getString(R.string.data_already_exist));
                        } else {
                            updateData(modelBOP);
                            tabLayout.getTabAt(0).select();
                        }
                    }
                } else if (!inputCode.equals(modelBOP.getCode())){
                    if (fbf.cekCode(inputCode)){
                        etCode.setError(getString(R.string.data_already_exist));
                    } else if (fbf.cekName(inputName)){
                        etName.setError(getString(R.string.data_already_exist));
                    } else {
                        updateData(modelBOP);
                        tabLayout.getTabAt(0).select();
                    }
                }
            }
        });

        if (!inputCode.equals(modelBOP.getCode())
                || !inputName.equals(modelBOP.getName())
                || !inputPositionLevel.equals(modelBOP.getPositionLevel())
                || !inputCompany.equals(modelBOP.getCompany())
                || !inputNotes.equals(modelBOP.getNotes())){
            buttonReset.setEnabled(true);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.orens));
        } else {
            buttonReset.setEnabled(false);
            buttonReset.setBackgroundColor(getResources().getColor(R.color.gray));
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCode.setText(modelBOP.getCode());
                etName.setText(modelBOP.getName());
//                etPositionLevel.setText(modelBOP.getPositionLevel());
                actvPositionLevel.setText(modelBOP.getPositionLevel());
//                etCompany.setText(modelBOP.getCompany());
                actvCompany.setText(modelBOP.getCompany());
                etNotes.setText(modelBOP.getNotes());
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelBOP.getName() + " ?");
                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessBOP().deleteViewBOP(modelBOP.getId());
                        tabLayout.getTabAt(0).select();
                    }
                });
                builder.show();
            }
        });
    }

    private void updateData(ModelBOP modelBOP){
        String inputCode          = etCode.getText().toString();
        String inputName          = etName.getText().toString();
        String inputPositionLevel = actvPositionLevel.getText().toString();
        String inputCompany       = actvCompany.getText().toString();
        String inputNotes         = etNotes.getText().toString();

        ModelBOP ubahModelBOP = new ModelBOP();
        ubahModelBOP.setId(modelBOP.getId());

        ubahModelBOP.setCode(inputCode);
        ubahModelBOP.setName(inputName);
        ubahModelBOP.setPositionLevel(inputPositionLevel);
        ubahModelBOP.setCompany(inputCompany);
        ubahModelBOP.setNotes(inputNotes);
        ubahModelBOP.setDelete(false);
        db.daoAccessBOP().updateBOP(ubahModelBOP);
    }
}
