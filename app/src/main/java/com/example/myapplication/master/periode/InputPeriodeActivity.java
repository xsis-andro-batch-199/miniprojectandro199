package com.example.myapplication.master.periode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelPeriode;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputPeriodeActivity extends AppCompatActivity {

    private EditText etPeriode, etDeskripsiPeriode;
    private Button btnBatal, btnSimpan;
    private TextInputLayout pesanError;
    private ModelPeriode periodeModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_periode);

        setupView();
    }

    private void setupView() {
        etPeriode = findViewById(R.id.etinPeriode);
        etDeskripsiPeriode = findViewById(R.id.etinDeskripsiP);
        btnBatal = findViewById(R.id.btnBatalPeriode);
        btnSimpan = findViewById(R.id.btnSimpanPeriode);
        pesanError = findViewById(R.id.errorPeriode);

        etPeriode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesanError.setErrorEnabled(true);
                pesanError.setError(getResources().getString(R.string.errorPeriode));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etPeriode.getText().toString();
                if (!inputText.isEmpty()){
                    pesanError.setErrorEnabled(false);
                    pesanError.setError(null);
                } else {
                    pesanError.setErrorEnabled(true);
                    pesanError.setError(getResources().getString(R.string.errorPeriode));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etPeriode.getText().toString();
                if (inputText.length() > 0){
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!etPeriode.getText().toString().isEmpty()){
                                if (cekPeriodeInput(inputText)){
                                    etPeriode.setError(getResources().getString(R.string.data_ada));
                                } else {
                                    periodeModel = new ModelPeriode();
                                    periodeModel.setPeriode(etPeriode.getText().toString());
                                    periodeModel.setDeskripsi(etDeskripsiPeriode.getText().toString());
                                    periodeModel.setIs_delete(false);

                                    db.daoAccessPeriode().insertPeriode(periodeModel);
                                    Toast.makeText(InputPeriodeActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), PeriodeActivity.class));
                                    finish();
                                }
                            } else {
                                Toast.makeText(InputPeriodeActivity.this, getString(R.string.lengkapi_data), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }

            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputPeriodeActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), PeriodeActivity.class));
                finish();
            }
        });
    }

    private boolean cekPeriodeInput(String inputText){
        List<ModelPeriode> list = db.daoAccessPeriode().getAllPeriodeNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPeriode())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, PeriodeActivity.class));
        finish();
    }
}
