package com.example.myapplication.master.tipe_identitas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputTipeIdentitasActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private EditText editTipeIdentitas, editDeskripsi;
    private Button btnBatal, btnSimpan;
    private TextInputLayout inputLayout;

    private ModelTipeIdentitas modelTipeIdentitas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_tipe_identitas);
        setupView();
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.tipe_identitas));
        /*toolbar.setOverflowIcon(getDrawable(R.drawable.ic_more_vert_white_24dp));*/
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout_input_identitas);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setupView() {
        editTipeIdentitas = findViewById(R.id.edit_tipe_identitas);
        editDeskripsi = findViewById(R.id.edit_deskripsi);

        btnBatal = findViewById(R.id.button_batal_identitas);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputTipeIdentitasActivity.this, "Dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), TipeIdentitasActivity.class));
                finish();
            }
        });

        inputLayout = findViewById(R.id.inputLayoutInputIdentitas);
        inputLayout.setErrorEnabled(true);

        btnSimpan = findViewById(R.id.button_simpan_identitas);

        if (TextUtils.isEmpty(editTipeIdentitas.getText())){
            /*inputLayout.setErrorEnabled(true);
            inputLayout.setError(getResources().getString(R.string.error_identitas));*/
            inputLayout.setHint("Tipe Identitas *");
        } else {
            inputLayout.setErrorEnabled(false);
            inputLayout.setHint("Tipe Identitas *");
            editTipeIdentitas.setHint("Tipe Identitas");
        }

        editTipeIdentitas.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(getBaseContext().INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTipeIdentitas, InputMethodManager.SHOW_IMPLICIT);
        editTipeIdentitas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (TextUtils.isEmpty(editTipeIdentitas.getText())){
                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getResources().getString(R.string.error_identitas));
                    inputLayout.setHint("Tipe Identitas *");
                } else {
                    inputLayout.setErrorEnabled(false);
                    inputLayout.setHint("Tipe Identitas *");
                    editTipeIdentitas.setHint("Tipe Identitas");
                }
            }
        });
        editTipeIdentitas.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(editTipeIdentitas.getText())){
                    inputLayout.setErrorEnabled(true);
                    inputLayout.setError(getResources().getString(R.string.error_identitas));
                    inputLayout.setHint("Tipe Identitas *");
                } else {
                    inputLayout.setErrorEnabled(false);
                    inputLayout.setHint("Tipe Identitas *");
                    editTipeIdentitas.setHint("Tipe Identitas");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = editTipeIdentitas.getText().toString();
                if (inputText.length() > 0){
                    inputLayout.setError(null);
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpan.setEnabled(true);
                    btnSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!editTipeIdentitas.getText().toString().isEmpty()){
                                if (cekNama(inputText)){
                                    editTipeIdentitas.setError(getString(R.string.data_ada));
                                } else {
                                    modelTipeIdentitas = new ModelTipeIdentitas();
                                    modelTipeIdentitas.setTipe_identitas(editTipeIdentitas.getText().toString());
                                    modelTipeIdentitas.setDeskripsi(editDeskripsi.getText().toString());
                                    modelTipeIdentitas.setDelete(false);

                                    //mengisi db dengan data
                                    db.daoAccess().innsertTipeIdentitas(modelTipeIdentitas);
                                    Toast.makeText(InputTipeIdentitasActivity.this, "Berhasil didaftarkan", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(),TipeIdentitasActivity.class));
                                    finish();
                                }
                                
                            }else {
                                Toast.makeText(InputTipeIdentitasActivity.this, getString(R.string.lengkapi_data), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    /*inputLayout.setError(getResources().getString(R.string.error_identitas));*/
                    btnSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpan.setEnabled(false);
                }
            }
        });

    }

    private boolean cekNama(String inputText) {
        List<ModelTipeIdentitas> list = db.daoAccess().getAllIdentitasNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getTipe_identitas())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_input_identitas);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), TipeIdentitasActivity.class));
            finish();
        }
    }
}
