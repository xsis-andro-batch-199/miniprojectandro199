package com.example.myapplication.master.pendidikan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPendidikan;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahPendidikanActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText etPendidikan, etDeskripsi;
    private Button buttonBatal, buttonSimpan;
    private TextInputLayout pesanError;
    private ImageView buttonResetPendidikan, buttonResetDeskripsi;

    ModelPendidikan modelPendidikan;

    public static final  String EXTRA_DATA = "extra_data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_pendidikan);
        setupView();
        setupToolbar();
        loadData();
    }

    private void loadData() {
        modelPendidikan = (ModelPendidikan) getIntent().getSerializableExtra(EXTRA_DATA);
        etPendidikan.setText(modelPendidikan.getPendidikan());
        etDeskripsi.setText(modelPendidikan.getDeskripsi());
    }
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.ubah_jenjang_pendidikan));
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void setupView() {
        buttonResetPendidikan = findViewById(R.id.btn_clear_pendidikan);
        buttonResetDeskripsi = findViewById(R.id.btn_clear_deskripsi);
        buttonSimpan = findViewById(R.id.btn_simpan);
        pesanError = findViewById(R.id.pesan_error);

        //field edit text pendidikan
        etPendidikan = findViewById(R.id.et_pendidikan);
        etPendidikan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pesanError.setErrorEnabled(false);
                pesanError.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = etPendidikan.getText().toString();
                if (inputText.isEmpty()){
                    pesanError.setErrorEnabled(true);
                    pesanError.setError(getResources().getString(R.string.error_pendidikan));
                } else {
                    pesanError.setErrorEnabled(false);
                    pesanError.setError(null);
                }

                if (!inputText.equals(modelPendidikan.getPendidikan())){
                    buttonResetPendidikan.setVisibility(View.VISIBLE);
                    buttonResetPendidikan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etPendidikan.setText(modelPendidikan.getPendidikan());
                        }
                    });
                } else {
                    buttonResetPendidikan.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etPendidikan.getText().toString();
                if (!modelPendidikan.getPendidikan().equals(inputText)
                        && !etPendidikan.getText().toString().isEmpty()){
                    buttonSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    buttonSimpan.setEnabled(true);
                    buttonSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekNama(inputText)){
                                pesanError.setError(getString(R.string.data_ada));
                            } else {
                                ModelPendidikan ubahPendidikan = new ModelPendidikan();
                                ubahPendidikan.setId(modelPendidikan.getId());
                                ubahPendidikan.setPendidikan(etPendidikan.getText().toString());
                                ubahPendidikan.setDeskripsi(etDeskripsi.getText().toString());

                                db.daoAccess().updatePendidikan(ubahPendidikan);
                                startActivity(new Intent(getApplicationContext(),JenjangPendidikanActivity.class));
                                finish();
                            }
                        }
                    });
                } else {
                    buttonSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    buttonSimpan.setEnabled(false);
                }
            }
        });

        //field edit text deskripsi
        etDeskripsi = findViewById(R.id.et_deskripsi);
        etDeskripsi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputDeskripsi = etDeskripsi.getText().toString();
                if (!inputDeskripsi.equals(modelPendidikan.getDeskripsi())){
                    buttonResetDeskripsi.setVisibility(View.VISIBLE);
                    buttonResetDeskripsi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etDeskripsi.setText(modelPendidikan.getDeskripsi());
                        }
                    });
                } else {
                    buttonResetDeskripsi.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputDeskripsi = etDeskripsi.getText().toString();
                if (!modelPendidikan.getDeskripsi().equals(inputDeskripsi)
                        && !etPendidikan.getText().toString().isEmpty()){
                    buttonSimpan.setBackgroundColor(getResources().getColor(R.color.biru));
                    buttonSimpan.setEnabled(true);
                    buttonSimpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelPendidikan ubahPendidikan = new ModelPendidikan();
                            ubahPendidikan.setId(modelPendidikan.getId());
                            ubahPendidikan.setPendidikan(etPendidikan.getText().toString());
                            ubahPendidikan.setDeskripsi(etDeskripsi.getText().toString());

                            db.daoAccess().updatePendidikan(ubahPendidikan);
                            startActivity(new Intent(getApplicationContext(), JenjangPendidikanActivity.class));
                            finish();
                        }
                    });
                } else {
                    buttonSimpan.setBackgroundColor(getResources().getColor(R.color.gray));
                    buttonSimpan.setEnabled(false);
                }
            }
        });

        buttonBatal = findViewById(R.id.btn_batal);
        buttonBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), JenjangPendidikanActivity.class));
                finish();
            }
        });
    }
    private boolean cekNama(String inputText) {
        List<ModelPendidikan> list = db.daoAccess().getAllPendidikanNoCondition();
        for (int i=0; i<list.size(); i++){
            if (inputText.contentEquals(list.get(i).getPendidikan())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), JenjangPendidikanActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
