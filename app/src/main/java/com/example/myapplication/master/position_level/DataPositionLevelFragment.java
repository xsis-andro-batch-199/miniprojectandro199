package com.example.myapplication.master.position_level;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AdapterPositionLevel;
import com.example.myapplication.db.Database;
import com.example.myapplication.model.ModelPositionLevel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.AppController.db;

public class DataPositionLevelFragment extends Fragment {
    RecyclerView recyclerView;
    ImageView btnTambah;
    AdapterPositionLevel adapterPositionLevel;
    List<ModelPositionLevel> list = new ArrayList<>();

    public DataPositionLevelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_position_level, container, false);
        setupView(view);
        loadData();
        showData();
        setHasOptionsMenu(true);
        return view;
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapterPositionLevel = new AdapterPositionLevel(getContext(), list);
        recyclerView.setAdapter(adapterPositionLevel);
    }

    private void loadData() {
        db      = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
        list    = db.daoAccessPositionLevel().getDBPositionLevel();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.master, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessPositionLevel().findByPositionLevel(s);
                showData();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                db = Room.databaseBuilder(getContext(), Database.class, "xsis_project").allowMainThreadQueries().build();
                list = db.daoAccessPositionLevel().findByPositionLevel(s);
                showData();
                return true;
            }
        });
    }

    private void setupView(View view) {
        btnTambah = view.findViewById(R.id.btnTambah);
        recyclerView = view.findViewById(R.id.rvPositionLevel);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new FormPositionLevelFragment();
                loadFragment(fragment);
            }
        });

        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutPositionLevel);
        tabLayout.getTabAt(0).select();
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameMainPositionLevel, fragment);
        fragmentTransaction.commit();
    }

}
