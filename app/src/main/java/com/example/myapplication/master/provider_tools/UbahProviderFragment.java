package com.example.myapplication.master.provider_tools;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelProviderTools;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahProviderFragment extends Fragment {
    TextView tvHeaderProvider;
    EditText etProvider, etNotes;
    Button btnReset, btnSave;
    TextInputLayout errorRequirment;
    ImageView btnDelete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_provider, container, false);
        setupView(view);
        loadData();
        return view;
    }

    private void loadData() {
        Bundle bundle = this.getArguments();
        ModelProviderTools modelProviderTools =  (ModelProviderTools) bundle.getSerializable("data");

        if (bundle != null){
            String nameProvider = modelProviderTools.getProvider();
            String noteProveder = modelProviderTools.getNotes();

            etProvider.setText(nameProvider);
            etNotes.setText(noteProveder);

        } else {
            Toast.makeText(getContext(), "bundle is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupView(View view) {
        Bundle bundle = this.getArguments();
        final ModelProviderTools modelProviderTools =  (ModelProviderTools) bundle.getSerializable("data");
        tvHeaderProvider = view.findViewById(R.id.tvAddEditProvider);
        tvHeaderProvider.setText(getResources().getString(R.string.editProvider));
        btnDelete = view.findViewById(R.id.btnDeleteProvider);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Hapus " + modelProviderTools.getProvider()+" ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.daoAccessProviderTools().getUpdateProviderTools(modelProviderTools.getId());

                        Toast.makeText(getContext(), "Data " + modelProviderTools.getProvider() + " terhapus", Toast.LENGTH_SHORT).show();
                        Fragment fragmentData = new DataProviderFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frameMainProvider, fragmentData);
                        fragmentTransaction.commit();
                    }
                });
                builder.show();
            }
        });
        errorRequirment = view.findViewById(R.id.errorProvider);
        btnReset = view.findViewById(R.id.btnResetProvider);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Reset", Toast.LENGTH_SHORT).show();
                etProvider.setText(modelProviderTools.getProvider());
                etNotes.setText(modelProviderTools.getNotes());
            }
        });
        btnSave = view.findViewById(R.id.btnSaveProvider);
        etProvider = view.findViewById(R.id.etAddEditProvider);
        etProvider.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etProvider.getText().toString();
                if (!modelProviderTools.getProvider().equals(inputText)
                        && !etProvider.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekProviderInput(inputText)){
                                etProvider.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelProviderTools updateModel = new ModelProviderTools();
                                updateModel.setId(modelProviderTools.getId());
                                updateModel.setProvider(etProvider.getText().toString());
                                updateModel.setNotes(etNotes.getText().toString());

                                db.daoAccessProviderTools().updateProvider(updateModel);
                                Fragment fragmentData = new DataProviderFragment();
                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frameMainProvider, fragmentData);
                                fragmentTransaction.commit();

                                Toast.makeText(getContext(), "Terudate", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        etNotes = view.findViewById(R.id.etNotesProvider);
        etNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNotes.getText().toString();
                if (!modelProviderTools.getNotes().equals(inputText)
                        && !etProvider.getText().toString().isEmpty()){
                    btnReset.setBackgroundColor(getResources().getColor(R.color.orens));
                    btnReset.setEnabled(true);
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekProviderInput(inputText)){
                                etProvider.setError(getResources().getString(R.string.data_ada));
                            } else {
                                ModelProviderTools update = new ModelProviderTools();
                                update.setId(modelProviderTools.getId());
                                update.setProvider(etProvider.getText().toString());
                                update.setNotes(etNotes.getText().toString());

                                db.daoAccessProviderTools().updateProvider(update);
                                Fragment fragmentData = new DataProviderFragment();
                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frameMainProvider, fragmentData);
                                fragmentTransaction.commit();

                                Toast.makeText(getContext(), "Terudate", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                    btnReset.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnReset.setEnabled(false);
                }
            }
        });
        TabLayout tabLayout = getActivity().findViewById(R.id.tabLayoutProvider);
        tabLayout.getTabAt(1).select();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                if (tab.getPosition() == 0){
//                    Fragment fragmentData = new DataProviderFragment();
//                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.replace(R.id.frameMainProvider, fragmentData);
//                    fragmentTransaction.commit();
//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private boolean cekProviderInput(String inputText){
        List<ModelProviderTools> list = db.daoAccessProviderTools().getAllNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (inputText.contentEquals(list.get(i).getProvider())){
                return true;
            }
        }
        return false;
    }
}
