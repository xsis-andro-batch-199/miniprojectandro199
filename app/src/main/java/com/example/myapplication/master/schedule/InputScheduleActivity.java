package com.example.myapplication.master.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

import com.example.myapplication.model.ModelSchedule;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;


import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputScheduleActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private EditText etNamaSchedule, etDesSchedule;
    private TextView idTitleSchedule;
    private Button btnSubmit, btnCancel;
    private ImageView ivClearSchedule, ivClearDesSchedule;
    private TextInputLayout psnError;
    private ModelSchedule modelSchedule;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_schedule);
        setToolbar();
        setUpView();
        batalListener();
        simpanListener();
    }
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void setUpView() {
        idTitleSchedule = findViewById(R.id.idTitleSchedule);
        etNamaSchedule = findViewById(R.id.etNamaSchedule);
        etDesSchedule = findViewById(R.id.etDeskripsiSchedule);
        btnCancel = findViewById(R.id.btnBatalSchedule);
        btnSubmit = findViewById(R.id.btnSaveSchedule);
        ivClearSchedule = findViewById(R.id.ivClearSchedule);
        ivClearDesSchedule = findViewById(R.id.ivClearDesSchedule);
        psnError = findViewById(R.id.psnErrorSchedule);

    }

    private void batalListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(InputScheduleActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                Intent intentCancel = new Intent(getBaseContext(), ScheduleActivity.class);
                startActivity(intentCancel);
                finish();
            }
        });
    }
    private void simpanListener() {
        ivClearSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etNamaSchedule.getText().clear();
            }
        });
        ivClearDesSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etDesSchedule.getText().clear();
            }
        });
        etDesSchedule.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(etDesSchedule.getText())) {
                    ivClearDesSchedule.setVisibility(View.GONE);
                } else {
                    ivClearDesSchedule.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etNamaSchedule.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorAlert();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNamaSchedule.getText().toString();
                if (inputText.length() > 0) {
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSubmit.setEnabled(true);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!inputText.isEmpty()) {
                                if (cekDataNama(inputText)) {
                                    etNamaSchedule.setError("Data sudah ada");
                                } else {
                                    modelSchedule = new ModelSchedule();
                                    modelSchedule.setJadwalNama(inputText);
                                    modelSchedule.setJadwalDeskripsi(etDesSchedule.getText().toString());
                                    modelSchedule.setDelete(false);
                                    db.daoAccessSchedule().QueryInserSchedule(modelSchedule);
                                    Toast.makeText(InputScheduleActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(),ScheduleActivity.class));
                                    finish();
                                }

                            } else {
                                Toast.makeText(InputScheduleActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSubmit.setEnabled(false);
                }
            }
        });
    }
    private boolean cekDataNama(String inputText) {
        List<ModelSchedule> list = db.daoAccessSchedule().QueryGetAllScheduleNoCon();
        for (int i = 0; i < list.size(); i++) {
            if (inputText.contentEquals(list.get(i).getJadwalNama())) {
                return true;
            }
        }
        return false;
    }
    private void errorAlert() {
        if (TextUtils.isEmpty(etNamaSchedule.getText())) {
            ivClearSchedule.setVisibility(View.GONE);
            psnError.setErrorEnabled(true);
            psnError.setError(getResources().getString(R.string.error_schedule));
            psnError.setHint("Jadwal");
        } else {
            ivClearSchedule.setVisibility(View.VISIBLE);
            psnError.setErrorEnabled(false);
            psnError.setHint("Jadwal");
            etNamaSchedule.setHint("Jadwal");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
