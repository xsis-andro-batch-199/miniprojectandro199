package com.example.myapplication.master.agama;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelAgama;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class EditAgamaActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private TextView idTitleAgama;
    private EditText editAgama, editDeskripsiAgama;
    private Button btnBatalAgama, btnSimpanAgama;
    private TextInputLayout pesan_error;
    ModelAgama modelAgama;
    public static final  String EXTRA_DATA = "extra_data";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_agama);
        modelAgama = (ModelAgama) getIntent().getSerializableExtra(EXTRA_DATA);
        setToolbar();
        setupView();
        loadData();
        idTitleAgama.setText(getResources().getString(R.string.ubah_agama));
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void loadData() {
        editAgama.setText(modelAgama.getAgama());
        editDeskripsiAgama.setText(modelAgama.getDeskripsi());
    }

    private void setupView() {
        idTitleAgama = findViewById(R.id.idTitleAgama);
        editAgama = findViewById(R.id.edit_agama);
        editDeskripsiAgama = findViewById(R.id.edit_deskripsi_agama);

        btnBatalAgama = findViewById(R.id.button_batal_agama);
        btnBatalAgama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(EditAgamaActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                finish();
            }
        });

        pesan_error = findViewById(R.id.pesan_error);
        pesan_error.setErrorEnabled(true);

        btnSimpanAgama = findViewById(R.id.button_simpan_agama);
        btnSimpanAgama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!hasWindowFocus()){
                    if (!TextUtils.isEmpty(editAgama.getText())){
                        pesan_error.setError(null);
                    } else {
                        pesan_error.setError(getResources().getString(R.string.error_agama));
                    }
                }
            }
        });
        if (TextUtils.isEmpty(editAgama.getText())){
            pesan_error.setErrorEnabled(true);
            pesan_error.setError(getResources().getString(R.string.error_agama));
            pesan_error.setHint("Agama");
        } else {
            pesan_error.setErrorEnabled(false);
            pesan_error.setHint("Agama");
            editAgama.setHint("Agama");
        }
        editAgama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(editAgama.getText())){
                    pesan_error.setErrorEnabled(true);
                    pesan_error.setError(getResources().getString(R.string.error_agama));
                    pesan_error.setHint("Agama");
                } else {
                    pesan_error.setErrorEnabled(false);
                    pesan_error.setHint("Agama");
                    editAgama.setHint("Agama");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = editAgama.getText().toString();
                final String inputDes = editDeskripsiAgama.getText().toString();
                if (!modelAgama.getAgama().equals(inputText)
                    && !editAgama.getText().toString().isEmpty()){
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSimpanAgama.setEnabled(true);
                    btnSimpanAgama.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (cekDataNama(inputText)){
                                editAgama.setError("Data sudah ada");
                            } else {
                                ModelAgama updateAgama = new ModelAgama();
                                updateAgama.setId(modelAgama.getId());
                                updateAgama.setAgama(editAgama.getText().toString());
                                updateAgama.setDeskripsi(editDeskripsiAgama.getText().toString());
                                updateAgama.setIs_delete(false);
                                Toast.makeText(EditAgamaActivity.this, "Berhasil diubah", Toast.LENGTH_SHORT).show();
                                db.daoAccess().updateAgama(updateAgama);
                                startActivity(new Intent(getApplicationContext(), AgamaActivity.class));
                                finish();
                            }

                        }
                    });
                } else {
                    btnSimpanAgama.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSimpanAgama.setEnabled(false);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean cekDataNama(String valueCek) {
        List<ModelAgama> list =db.daoAccess().getAllAgamaNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (valueCek.contentEquals(list.get(i).getAgama())) {
                return true;
            }
        }
        return false;
    }
}
