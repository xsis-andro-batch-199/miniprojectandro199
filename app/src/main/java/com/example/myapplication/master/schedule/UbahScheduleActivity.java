package com.example.myapplication.master.schedule;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.ModelSchedule;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class UbahScheduleActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private EditText etNamaSchedule, etDesSchedule;
    private TextView idTitleSchedule;
    private Button btnSubmit, btnCancel;
    private ImageView ivClearSchedule, ivClearDesSchedule;
    private TextInputLayout psnError;
    private ModelSchedule modelSchedule;
    private String valNamaSchedule, valDesSchedule;
    public static final String EXTRA_DATA = "extra_data";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_schedule);
        modelSchedule = (ModelSchedule) getIntent().getSerializableExtra(EXTRA_DATA);
        setToolbar();
        setUpView();
        loadData();
        batalListener();
        simpanListener();

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void setUpView() {
        idTitleSchedule = findViewById(R.id.idTitleSchedule);
        etNamaSchedule = findViewById(R.id.etNamaSchedule);
        etDesSchedule = findViewById(R.id.etDeskripsiSchedule);
        btnCancel = findViewById(R.id.btnBatalSchedule);
        btnSubmit = findViewById(R.id.btnSaveSchedule);
        ivClearSchedule = findViewById(R.id.ivClearSchedule);
        ivClearDesSchedule = findViewById(R.id.ivClearDesSchedule);
        psnError = findViewById(R.id.psnError);
        valNamaSchedule = etNamaSchedule.getText().toString();
        valDesSchedule  = etDesSchedule.getText().toString();

    }
    private void loadData() {
        valNamaSchedule = modelSchedule.getJadwalNama();
        valDesSchedule = modelSchedule.getJadwalDeskripsi();
        etNamaSchedule.setText(valNamaSchedule);
        etDesSchedule.setText(valDesSchedule);
    }
    private void simpanListener() {
        ivClearSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etNamaSchedule.setText(valNamaSchedule);
            }
        });
        ivClearDesSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { etDesSchedule.setText(valDesSchedule);
            }
        });
        etDesSchedule.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (modelSchedule.getJadwalDeskripsi().equals(etDesSchedule.getText().toString()) || TextUtils.isEmpty(etDesSchedule.getText())) {
                    ivClearDesSchedule.setVisibility(View.GONE);
                } else {
                    ivClearDesSchedule.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!modelSchedule.getJadwalDeskripsi().equals(etDesSchedule.getText().toString())){
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSubmit.setEnabled(true);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ModelSchedule updateSchedule = new ModelSchedule();
                            updateSchedule.setId(modelSchedule.getId());
                            updateSchedule.setJadwalNama(etNamaSchedule.getText().toString());
                            updateSchedule.setJadwalDeskripsi(etDesSchedule.getText().toString());
                            updateSchedule.setDelete(false);
                            db.daoAccessSchedule().QueryUpdateSchedule(updateSchedule);
                            Toast.makeText(UbahScheduleActivity.this, "Berhasil di Ubah", Toast.LENGTH_SHORT).show();
                            Intent intentUpdate = new Intent(getBaseContext(), ScheduleActivity.class);
                            startActivity(intentUpdate);
                            finish();
                        }
                    });
                }else{
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSubmit.setEnabled(false);
                }
            }
        });
        etNamaSchedule.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorAlert();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = etNamaSchedule.getText().toString();
                if(!modelSchedule.getJadwalNama().equals(inputText)
                        && !etNamaSchedule.getText().toString().isEmpty()){
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSubmit.setEnabled(true);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!inputText.isEmpty()) {
                                if (cekDataNama(inputText)) {
                                    etNamaSchedule.setError("Data sudah ada");
                                } else {
                                    ModelSchedule updateSchedule = new ModelSchedule();
                                    updateSchedule.setId(modelSchedule.getId());
                                    updateSchedule.setJadwalNama(etNamaSchedule.getText().toString());
                                    updateSchedule.setJadwalDeskripsi(etDesSchedule.getText().toString());
                                    updateSchedule.setDelete(false);
                                    db.daoAccessSchedule().QueryUpdateSchedule(updateSchedule);
                                    Toast.makeText(UbahScheduleActivity.this, "Berhasil di Ubah", Toast.LENGTH_SHORT).show();
                                    Intent intentUpdate = new Intent(getBaseContext(), ScheduleActivity.class);
                                    startActivity(intentUpdate);
                                    finish();
                                }

                            } else {
                                Toast.makeText(UbahScheduleActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    btnSubmit.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSubmit.setEnabled(false);
                }
            }
        });
    }
    private void batalListener() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(UbahScheduleActivity.this, "dibatalkan", Toast.LENGTH_SHORT).show();
                Intent intentCancel = new Intent(getBaseContext(), ScheduleActivity.class);
                startActivity(intentCancel);
                finish();
            }
        });
    }
    private boolean cekDataNama(String inputText) {
        List<ModelSchedule> list = db.daoAccessSchedule().QueryGetAllScheduleNoCon();
        for (int i = 0; i < list.size(); i++) {
            if (inputText.contentEquals(list.get(i).getJadwalNama())) {
                return true;
            }
        }
        return false;
    }
    private void errorAlert() {
        if (modelSchedule.getJadwalNama().equals(etNamaSchedule.getText().toString()) || TextUtils.isEmpty(etNamaSchedule.getText())) {
            ivClearSchedule.setVisibility(View.GONE);
        } else {
            ivClearSchedule.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(etNamaSchedule.getText())) {
            psnError.setErrorEnabled(true);
            psnError.setError(getResources().getString(R.string.error_schedule));
            psnError.setHint("Jadwal");
        } else {
            psnError.setErrorEnabled(false);
            psnError.setHint("Jadwal");
            etNamaSchedule.setHint("Jadwal");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
