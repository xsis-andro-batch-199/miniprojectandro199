package com.example.myapplication.master.status_pernikahan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class InputStatusPernikahanActivity extends AppCompatActivity {

    private EditText inputPernikahan, inputDiskripsi;
    private Button btnSave, btnCancel;
    private ModelStatusPernikahan modelStatusPernikahan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_status_pernikahan);
        setupView();
    }



    private void setupView() {
        inputPernikahan = findViewById(R.id.inputStatusPernikahan);
        inputDiskripsi = findViewById(R.id.inputDiskripsi);
        final TextInputLayout msgError = (TextInputLayout) findViewById(R.id.msgError);

        btnSave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StatusPernikahanActivity.class));
                finish();
            }
        });

        inputPernikahan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(inputPernikahan.getText())){
                    msgError.setErrorEnabled(true);
                    msgError.setError(getResources().getString(R.string.error_statusPernikahan));
                    msgError.setHint("Status pernikahan*");
                } else {
                    msgError.setErrorEnabled(false);
                    msgError.setHint("Status pernikahan*");
                    inputPernikahan.setHint("Status pernikahan*");
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String inputText = inputPernikahan.getText().toString();
                if(inputText.length() > 0) {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.biru));
                    btnSave.setEnabled(true);
                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!inputPernikahan.getText().toString().isEmpty()){
                                if (cekStatus(inputText)){
                                    inputPernikahan.setError("Data sudah ada");
                                } else {
                                    modelStatusPernikahan = new ModelStatusPernikahan();
                                    modelStatusPernikahan.setStatuspernikahan(inputPernikahan.getText().toString());
                                    modelStatusPernikahan.setDeskripsi(inputDiskripsi.getText().toString());
                                    modelStatusPernikahan.setIs_delete(false);

                                    //mengisi db dengan data
                                    db.daoAccessStatusPernikahan().insertDataStatusPernikahan(modelStatusPernikahan);
                                    Toast.makeText(InputStatusPernikahanActivity.this, "Terdaftar", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), StatusPernikahanActivity.class));
                                    finish();
                                }

                            } else {
                                Toast.makeText(InputStatusPernikahanActivity.this, "Lengkapi field yang kosong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    btnSave.setBackgroundColor(getResources().getColor(R.color.gray));
                    btnSave.setEnabled(false);
                }
            }
        });
    }
    private boolean cekStatus(String inputText) {
        List<ModelStatusPernikahan> listStatus = db.daoAccessStatusPernikahan().getAllStatusPernikahanNoCondition();
        for (int i = 0; i < listStatus.size(); i++) {
            if (inputText.contentEquals(listStatus.get(i).getStatuspernikahan())) {
                return true;
            }
        }
        return false;
    }

    public void onBackPressed(){
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), StatusPernikahanActivity.class));

    }
}
