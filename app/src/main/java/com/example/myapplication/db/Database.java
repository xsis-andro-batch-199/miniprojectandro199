package com.example.myapplication.db;

import androidx.room.RoomDatabase;

import com.example.myapplication.dao.DaoAccess;
import com.example.myapplication.dao.DaoAccessAgama;
import com.example.myapplication.dao.DaoAccessBOP;
import com.example.myapplication.dao.DaoAccessCompany;
import com.example.myapplication.dao.DaoAccessEmployeeStatus;
import com.example.myapplication.dao.DaoAccessEmployeeTraining;
import com.example.myapplication.dao.DaoAccessEmployeeType;
import com.example.myapplication.dao.DaoAccessGrade;
import com.example.myapplication.dao.DaoAccessJenisCatatan;
import com.example.myapplication.dao.DaoAccessKeahlian;
import com.example.myapplication.dao.DaoAccessLeaveRequest;
import com.example.myapplication.dao.DaoAccessPRFRequest;
import com.example.myapplication.dao.DaoAccessPRFStatus;
import com.example.myapplication.dao.DaoAccessPeriode;
import com.example.myapplication.dao.DaoAccessPositionLevel;
import com.example.myapplication.dao.DaoAccessProject;
import com.example.myapplication.dao.DaoAccessProviderTools;
import com.example.myapplication.dao.DaoAccessSchedule;
import com.example.myapplication.dao.DaoAccessStatusPernikahan;
import com.example.myapplication.dao.DaoAccessTimesheet;
import com.example.myapplication.dao.DaoAccessTipeTes;
import com.example.myapplication.dao.DaoAccessTraining;
import com.example.myapplication.dao.DaoAccessTrainingOrganizer;
import com.example.myapplication.dao.DaoContractStatus;
import com.example.myapplication.model.ModelAgama;
import com.example.myapplication.model.ModelBOP;
import com.example.myapplication.model.ModelCompany;
import com.example.myapplication.model.ModelContractStatus;
import com.example.myapplication.model.ModelEmployeeStatus;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.example.myapplication.model.ModelEmployeeType;
import com.example.myapplication.model.ModelGrade;
import com.example.myapplication.model.ModelJenisCatatan;
import com.example.myapplication.model.ModelKeahlian;
import com.example.myapplication.model.ModelLeave;
import com.example.myapplication.model.ModelLeaveData;
import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;
import com.example.myapplication.model.ModelPRFStatus;
import com.example.myapplication.model.ModelPendidikan;
import com.example.myapplication.model.ModelPeriode;
import com.example.myapplication.model.ModelPositionLevel;
import com.example.myapplication.model.ModelProject;
import com.example.myapplication.model.ModelProviderTools;
import com.example.myapplication.model.ModelSchedule;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.example.myapplication.model.ModelTimesheet;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.example.myapplication.model.ModelTipeTes;
import com.example.myapplication.model.ModelTraining;
import com.example.myapplication.model.ModelTrainingOrganizer;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
@androidx.room.Database(entities = {
        ModelPendidikan.class,
        ModelTipeIdentitas.class,
        ModelStatusPernikahan.class,
        ModelKeahlian.class,
        ModelJenisCatatan.class,
        ModelAgama.class,
        ModelPeriode.class,
        ModelSchedule.class,
        ModelTimesheet.class,
        ModelTipeTes.class,
        ModelLeave.class,
        ModelLeaveData.class,
        ModelPRFRequestCandidates.class,
        ModelPRFRequest.class,
        ModelProviderTools.class,
        ModelCompany.class,
        ModelEmployeeStatus.class,
        ModelProject.class,
        ModelPositionLevel.class,
        ModelBOP.class,
        ModelGrade.class,
        ModelContractStatus.class,
        ModelPRFStatus.class,
        ModelEmployeeTraining.class,
        ModelEmployeeType.class,
        ModelTrainingOrganizer.class,
        ModelTraining.class/*tambah nama kelas entities klean disini, diawali dengan koma*/},
        version = 1,
        exportSchema = false)

public abstract class Database extends RoomDatabase {
    public abstract DaoAccess daoAccess();
    public abstract DaoAccessJenisCatatan daoAccessJenisCatatan();
    public abstract DaoAccessSchedule daoAccessSchedule();
    public abstract DaoAccessTipeTes daoAccessTipeTes();
    public abstract DaoAccessProviderTools daoAccessProviderTools();
    public abstract DaoAccessTimesheet daoAccessTimesheet();
    public abstract DaoAccessLeaveRequest daoAccessLeaveRequest();
    public abstract DaoAccessPeriode daoAccessPeriode();
    public abstract DaoAccessKeahlian daoAccessKeahlian();
    public abstract DaoAccessEmployeeStatus daoAccessEmployeeStatus();
    public abstract DaoAccessCompany daoAccessCompany();
    public abstract DaoAccessProject daoAccessProject();
    public abstract DaoAccessPositionLevel daoAccessPositionLevel();
    public abstract DaoAccessBOP daoAccessBOP();
    public abstract DaoAccessGrade daoAccessGrade();
    public abstract DaoContractStatus daoContractStatus();
    public abstract DaoAccessPRFStatus daoAccessPRFStatus();
    public abstract DaoAccessEmployeeTraining daoAccessEmployeeTraining();
    public abstract DaoAccessEmployeeType daoAccessEmployeeType();
    public abstract DaoAccessTrainingOrganizer daoAccessTrainingOrganizer();
    public abstract DaoAccessTraining daoAccessTraining();
    public abstract DaoAccessAgama daoAccessAgama();
    public abstract DaoAccessPRFRequest daoAccessPRFRequest();
    public abstract DaoAccessStatusPernikahan daoAccessStatusPernikahan();
}
