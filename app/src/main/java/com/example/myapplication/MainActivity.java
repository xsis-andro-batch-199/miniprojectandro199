package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.adapter.AdapterExpandableListNav;
import com.example.myapplication.fragment.MasterFragment;
import com.example.myapplication.master.back_office_position.FormBOPFragment;
import com.example.myapplication.master.company.FormCompanyFragment;
import com.example.myapplication.master.company.HolderCompanyFragment;
import com.example.myapplication.master.employee_type.FormEmployeeTypeFragment;
import com.example.myapplication.transaksi.employee_training.MainEmployeeTrainingFragment;
import com.example.myapplication.transaksi.leave_request.HolderLeaveFragment;
import com.example.myapplication.transaksi.leave_request.InputLeaveFragment;
import com.example.myapplication.transaksi.leave_request.LeaveDataFragment;
import com.example.myapplication.transaksi.prfrequest.MainPRFRequestFragment;
import com.example.myapplication.transaksi.prfrequest.PRFRequestFormFragment;
import com.example.myapplication.transaksi.project.InputProjectFragment;
import com.example.myapplication.transaksi.project.MainProjectFragment;
import com.example.myapplication.transaksi.timesheet.DetailTimesheetFragment;
import com.example.myapplication.transaksi.timesheet.HolderTimesheetFragment;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.example.myapplication.transaksi.timesheet.InputTimesheetFragment;
import com.example.myapplication.transaksi.timesheet.UpdateTimesheetFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private Fragment fragment;
    ExpandableListView expandableListView;
    AdapterExpandableListNav adapterExpandableListNav;
    List<String> listMenu = new ArrayList<>();
    HashMap<String, List<String>> listSubmenu = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupview();
    }

    private void setupview() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        /*fragment = new MasterFragment();
        loadFragment(fragment);*/
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //membuat object Fragment Pertama
        MasterFragment fragment_1 = new MasterFragment();
        //menambahkan fragment
        ft.add(R.id.frame_content, fragment_1);
        //dapat menyimpan fragment ke dlm state ketika tombol back diklik
        /*ft.addToBackStack("Fragment 1");*/
        //mengeksekusi ft
        ft.commit();

        setupExpandListNavbar();
    }

    private void setupExpandListNavbar() {
        this.listMenu.add("Timesheet");
        this.listMenu.add("Project");
        this.listMenu.add("Leave");
        this.listMenu.add("PRF Request");
        this.listMenu.add("Employee");

        List<String> sub1 = new ArrayList<>();
        sub1.add("Timesheet Entry");
        sub1.add("Timesheet History");
        List<String> sub2 = new ArrayList<>();
        sub2.add("Project Entry");
        sub2.add("Project History");
        List<String> sub3 = new ArrayList<>();
        sub3.add("Create/Edit Leave");
        List<String> sub4 = new ArrayList<>();
        sub4.add("Request");
        sub4.add("Request History");
        List<String> sub5 = new ArrayList<>();
        sub5.add("Set Training");

        this.listSubmenu.put(listMenu.get(0), sub1);
        this.listSubmenu.put(listMenu.get(1), sub2);
        this.listSubmenu.put(listMenu.get(2), sub3);
        this.listSubmenu.put(listMenu.get(3), sub4);
        this.listSubmenu.put(listMenu.get(4), sub5);

        expandableListView =(ExpandableListView) findViewById(R.id.elv_nav_drawer);
        adapterExpandableListNav = new AdapterExpandableListNav(this, listMenu, listSubmenu, expandableListView);
        expandableListView.setAdapter(adapterExpandableListNav);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Toast.makeText(MainActivity.this, listSubmenu.get(listMenu.get(i)).get(i1), Toast.LENGTH_SHORT).show();
                long childClicked = adapterExpandableListNav.getChildId(i, i1);
                if (adapterExpandableListNav.getChild(i, i1).equals("Timesheet Entry")){
                    toolbar.setTitle(getString(R.string.menu_timesheet_entry));
                    fragment = new InputTimesheetFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Timesheet History")){
                    toolbar.setTitle(getString(R.string.menu_timesheet_history));
                    fragment = new HolderTimesheetFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Project Entry")){
                    toolbar.setTitle(getString(R.string.project_entry_form));
                    fragment = new InputProjectFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Project History")){
                    toolbar.setTitle(getString(R.string.project_history));
                    fragment = new MainProjectFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Create/Edit Leave")){
                    toolbar.setTitle(getString(R.string.menu_leave));
                    fragment = new HolderLeaveFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Request")){
                    toolbar.setTitle("PRF");
                    fragment = new PRFRequestFormFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Request History")){
                    toolbar.setTitle("PRF");
                    fragment = new MainPRFRequestFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }else if (adapterExpandableListNav.getChild(i, i1).equals("Set Training")){
                    toolbar.setTitle("Employee Training");
                    fragment = new MainEmployeeTrainingFragment();
                    loadFragment(fragment);
                    expandableListView.collapseGroup(i);

                    DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                return false;
            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
               /* if (adapterExpandableListNav.getGroup(i).equals("Timesheet")){
                    expandableListView.collapseGroup(1);
                    expandableListView.collapseGroup(2);
                    expandableListView.collapseGroup(3);
                }else if (adapterExpandableListNav.getGroup(i).equals("Project")){
                    expandableListView.collapseGroup(0);
                    expandableListView.collapseGroup(2);
                    expandableListView.collapseGroup(3);
                }else if (expandableListView.getSelectedId() == 2){
                    expandableListView.collapseGroup(0);
                    expandableListView.collapseGroup(1);
                    expandableListView.collapseGroup(3);
                }else if (expandableListView.getSelectedId() == 3){
                    expandableListView.collapseGroup(0);
                    expandableListView.collapseGroup(2);
                    expandableListView.collapseGroup(1);
                }else { }*/

                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        Fragment fragment                  = getSupportFragmentManager().findFragmentById(R.id.frame_content);
        Fragment fragmentDetailTimesheet   = getSupportFragmentManager().findFragmentById(R.id.frame_timesheet);
        Fragment fragmentCompany           = getSupportFragmentManager().findFragmentById(R.id.frame_company);
        Fragment fragmentBOP               = getSupportFragmentManager().findFragmentById(R.id.frame_bop);
        Fragment fragmentEmployeeType      = getSupportFragmentManager().findFragmentById(R.id.frame_et);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (fragmentDetailTimesheet instanceof DetailTimesheetFragment){
            ((DetailTimesheetFragment) fragmentDetailTimesheet).onBackPressed();
        }
        else if (fragment instanceof InputTimesheetFragment){
            ((InputTimesheetFragment) fragment).onBackPressed();
        }
        else if (fragment instanceof UpdateTimesheetFragment){
            ((UpdateTimesheetFragment) fragment).onBackPressed();
        }
        else if (fragmentCompany instanceof FormCompanyFragment){
            ((FormCompanyFragment) fragmentCompany).onBackPressed();
        }
        else if (fragmentBOP instanceof FormBOPFragment){
            ((FormBOPFragment) fragmentBOP).onBackPressed();
        }
        else if (fragmentEmployeeType instanceof FormEmployeeTypeFragment){
            ((FormEmployeeTypeFragment) fragmentEmployeeType).onBackPressed();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_main_master) {
           *//* //Bersiap untuk interaksi antar fragment dengan main acyivity
            FragmentManager fragmentManager = get
            SupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            // memulai transaction fragment manager
            MasterFragment masterFragment = new MasterFragment();
            //membuat object fragment pertama
            transaction.replace(R.id.frame_content, masterFragment);
            //menambah fragment
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //transaction.addToBackStack("Welcome Fragment");
            //dapat menyimpan fragment kedalam state, ketika tombol back diklik
            transaction.commit();
            //mengeksekusi fragment transaction*//*
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            toolbar.setTitle("Home");
            fragment = new MasterFragment();
            loadFragment(fragment);
            /*startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();*/
        }
       /* else if (id == R.id.navPRFRequestHistory){
            toolbar.setTitle("PRF");
            fragment = new MainPRFRequestFragment();
            loadFragment(fragment);
        }
        else if (id == R.id.navPRFRequestInput){
            toolbar.setTitle("PRF");
            fragment = new PRFRequestFormFragment();
            loadFragment(fragment);
        }
        else if (id == R.id.nav_timesheet_history){
            toolbar.setTitle(getString(R.string.menu_timesheet_history));
            fragment = new HolderTimesheetFragment();
            loadFragment(fragment);
        }
        else if (id == R.id.nav_timesheet_entry){
            toolbar.setTitle(getString(R.string.menu_timesheet_entry));
            fragment = new InputTimesheetFragment();
            loadFragment(fragment);
        }else if (id == R.id.nav_create_edit_leave) {
            toolbar.setTitle(getString(R.string.menu_leave));
            fragment = new HolderLeaveFragment();
            loadFragment(fragment);
        }
        else if (id == R.id.nav_project_entry){
            toolbar.setTitle(getString(R.string.project_entry_form));
            fragment = new InputProjectFragment();
            loadFragment(fragment);
        }
        else if (id == R.id.nav_project_history){
            toolbar.setTitle(getString(R.string.project_history));
            fragment = new MainProjectFragment();
            loadFragment(fragment);
        }*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction();
        transaction.replace(R.id.frame_content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}

