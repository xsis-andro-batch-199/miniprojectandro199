package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.transaksi.leave_request.HistoryLeaveFragment;
import com.example.myapplication.transaksi.leave_request.LeaveDataFragment;

public class AdapterLeavePager extends FragmentStatePagerAdapter {
    int tab;

    public AdapterLeavePager(@NonNull FragmentManager fm, int tab) {
        super(fm);
        this.tab = tab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                LeaveDataFragment tabData = new LeaveDataFragment();
                return tabData;
            case 1:
                HistoryLeaveFragment tabHistory = new HistoryLeaveFragment();
                return tabHistory;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tab;
    }
}
