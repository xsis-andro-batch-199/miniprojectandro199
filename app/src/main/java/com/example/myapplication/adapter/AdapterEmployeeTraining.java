package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelEmployeeTraining;
import com.example.myapplication.transaksi.employee_training.DetailEmployeeTrainingFragment;

import java.util.List;

public class AdapterEmployeeTraining extends RecyclerView.Adapter<AdapterEmployeeTraining.ViewHolder> {
    private Context context;
    private List<ModelEmployeeTraining> listET;

    public AdapterEmployeeTraining(Context context, List<ModelEmployeeTraining> listET) {
        this.context = context;
        this.listET = listET;
    }

    @NonNull
    @Override
    public AdapterEmployeeTraining.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_employee_training, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEmployeeTraining.ViewHolder holder, int position) {
        final ModelEmployeeTraining modelEmployeeTraining = listET.get(position);
        final String[] kalimat = modelEmployeeTraining.getNama_pegawai().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String inisial = TextUtils.join("", value);
        holder.tvInisialET.setText(inisial);
        holder.tvNamaPegawai.setText(modelEmployeeTraining.getNama_pegawai());
        holder.tvNamaTraining.setText(modelEmployeeTraining.getTraining());
        holder.rlCardET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailEmployeeTrainingFragment detailEmployeeTrainingFragment = new DetailEmployeeTrainingFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelEmployeeTraining);
                detailEmployeeTrainingFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.mainFrameEmployeeTraining, detailEmployeeTrainingFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listET.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlCardET;
        TextView tvInisialET, tvNamaPegawai, tvNamaTraining;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rlCardET = itemView.findViewById(R.id.rlCardET);
            tvInisialET = itemView.findViewById(R.id.tvInitialEmployeeTraining);
            tvNamaPegawai = itemView.findViewById(R.id.tvNamaPegawaiEmployeeTraining);
            tvNamaTraining = itemView.findViewById(R.id.tvTrainingName);
        }
    }
}
