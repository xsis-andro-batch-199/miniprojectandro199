package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.training.UpdateTrainingFragment;
import com.example.myapplication.model.ModelTraining;

import java.util.List;

public class AdapterTraining extends RecyclerView.Adapter<AdapterTraining.ViewHolderTraining> {

    private Context context;
    private List<ModelTraining> trainingList;

    public AdapterTraining(Context context, List<ModelTraining> trainingList) {
        this.context = context;
        this.trainingList = trainingList;
    }

    @NonNull
    @Override
    public ViewHolderTraining onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_training, parent, false);
        return new ViewHolderTraining(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTraining holder, int position) {
        final ModelTraining modelTraining = trainingList.get(position);
        final String[] kalimat = modelTraining.getTraining_name().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);

        holder.tvInitialTraining.setText(initial);
        holder.tvTrainingName.setText(modelTraining.getTraining_name());
        holder.rvTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, initial + modelTraining.getId(), Toast.LENGTH_SHORT).show();
                UpdateTrainingFragment updateTrainingFragment = new UpdateTrainingFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelTraining);
                updateTrainingFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frameMainTraining, updateTrainingFragment);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    public class ViewHolderTraining extends RecyclerView.ViewHolder {
        RelativeLayout rvTraining;
        TextView tvInitialTraining, tvTrainingName;
        public ViewHolderTraining(@NonNull View itemView) {
            super(itemView);
            rvTraining = itemView.findViewById(R.id.rv_card_training);
            tvInitialTraining = itemView.findViewById(R.id.initialTraining);
            tvTrainingName = itemView.findViewById(R.id.tvTraining);
        }
    }
}