package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.back_office_position.UpdateBOPFragment;
import com.example.myapplication.model.ModelBOP;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class BOPAdapter extends RecyclerView.Adapter<BOPAdapter.ViewHolder> {
    private Context context;
    private List<ModelBOP> list;

    public BOPAdapter(Context context, List<ModelBOP> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_bop, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelBOP modelBOP = list.get(position);

        String[] name = modelBOP.getName().split(" ");
        String[] initial = new String[name.length];
        for (int i = 0; i < name.length; i++){
            initial[i] = String.valueOf(name[i].charAt(0));
        }
        String initialJoin = TextUtils.join("", initial);

        holder.id.setText(initialJoin);
        holder.name.setText(modelBOP.getName());
        holder.company.setText(modelBOP.getCompany());
        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new UpdateBOPFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelBOP);
                fragment.setArguments(bundle);

                TabLayout tabLayout = ((AppCompatActivity)context).findViewById(R.id.tab_layout_bop);
                tabLayout.getTabAt(1).select();

                FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_bop, fragment);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, name, company;
        RelativeLayout cardLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id          = itemView.findViewById(R.id.card_id_bop);
            name        = itemView.findViewById(R.id.card_name_bop);
            company     = itemView.findViewById(R.id.card_company_bop);
            cardLayout  = itemView.findViewById(R.id.card_layout_bop);
        }
    }
}
