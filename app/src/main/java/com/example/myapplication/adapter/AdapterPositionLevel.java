package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.position_level.UpdatePositionLevelFragment;
import com.example.myapplication.model.ModelPositionLevel;

import java.util.List;

public class AdapterPositionLevel extends RecyclerView.Adapter<AdapterPositionLevel.ViewHolderPositionLevel> {

    private Context context;
    private List<ModelPositionLevel> positionLevelList;

    public AdapterPositionLevel(Context context, List<ModelPositionLevel> positionLevelList) {
        this.context = context;
        this.positionLevelList = positionLevelList;
    }

    @NonNull
    @Override
    public ViewHolderPositionLevel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_positon_level, parent, false);
        return new ViewHolderPositionLevel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderPositionLevel holder, int position) {
        final ModelPositionLevel modelPositionLevel = positionLevelList.get(position);
        final String[] kalimat = modelPositionLevel.getPosition_name().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);

        holder.tvInitialPosition.setText(initial);
        holder.tvPositionName.setText(modelPositionLevel.getPosition_name());
        holder.rvPositionLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, initial + modelPositionLevel.getId(), Toast.LENGTH_SHORT).show();
                UpdatePositionLevelFragment updatePositionLevelFragment = new UpdatePositionLevelFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelPositionLevel);
                updatePositionLevelFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameMainPositionLevel, updatePositionLevelFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return positionLevelList.size();
    }

    public class ViewHolderPositionLevel extends RecyclerView.ViewHolder {
        RelativeLayout rvPositionLevel;
        TextView tvInitialPosition, tvPositionName;
        public ViewHolderPositionLevel(@NonNull View itemView) {
            super(itemView);
            rvPositionLevel = itemView.findViewById(R.id.rv_card_position_level);
            tvInitialPosition = itemView.findViewById(R.id.initialPosition);
            tvPositionName = itemView.findViewById(R.id.tvPositionLevel);
        }
    }
}
