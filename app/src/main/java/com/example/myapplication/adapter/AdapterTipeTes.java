package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.tipe_tes.TipeTesActivity;
import com.example.myapplication.master.tipe_tes.UbahTipeTesActivity;
import com.example.myapplication.model.ModelTipeTes;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterTipeTes extends RecyclerView.Adapter<AdapterTipeTes.TipeTesViewHolder> {

    private Context context;
    private List<ModelTipeTes> modelTipeTes;

    public AdapterTipeTes(Context context, List<ModelTipeTes> modelTipeTes) {
        this.context = context;
        this.modelTipeTes = modelTipeTes;
    }

    @NonNull
    @Override
    public TipeTesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_tipe_tes, parent, false);
        return new TipeTesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TipeTesViewHolder holder, int position) {
        final ModelTipeTes listSchedule = modelTipeTes.get(position);
        holder.tvIdTipeTes.setText(String.valueOf(position +1));
        holder.tvNamaTipeTes.setText(listSchedule.getTipeTesNama());
        holder.tvDesTipeTes.setText(listSchedule.getTipeTesDeskripsi());
        holder.menuTipeTes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.menuTipeTes);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                final Intent intent = new Intent(context, UbahTipeTesActivity.class);
                                intent.putExtra(UbahTipeTesActivity.EXTRA_DATA, listSchedule);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + listSchedule.getTipeTesNama() + "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccessTipeTes().QueryDeleteTipeTes(listSchedule.getId());
                                        Intent intent1 = new Intent(context, TipeTesActivity.class);
                                        context.startActivity(intent1);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelTipeTes.size();
    }

    public class TipeTesViewHolder extends RecyclerView.ViewHolder {

        TextView tvIdTipeTes, tvNamaTipeTes, tvDesTipeTes;
        ImageView menuTipeTes;

        public TipeTesViewHolder(@NonNull View itemView) {
            super(itemView);
            tvIdTipeTes = itemView.findViewById(R.id.idTipeTes);
            tvNamaTipeTes = itemView.findViewById(R.id.tvNamaTipeTes);
            tvDesTipeTes = itemView.findViewById(R.id.tvDescTipeTes);
            menuTipeTes = itemView.findViewById(R.id.idMenuTipeTes);
        }
    }
}
