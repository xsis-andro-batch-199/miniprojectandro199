package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.company.UpdateCompanyFragment;
import com.example.myapplication.model.ModelCompany;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {
    private Context context;
    private List<ModelCompany> list;

    public CompanyAdapter(Context context, List<ModelCompany> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_company, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelCompany modelCompany = list.get(position);

        final String[] company = modelCompany.getCompany().split(" ");
        String initialJoin = "";
        if (company.length < 2){
            initialJoin = String.valueOf(company[0].charAt(0));
        } else {
            String[] initial = new String[2];
            for (int i = 0; i < 2; i++){
                initial[i] = String.valueOf(company[i].charAt(0));
            }
            initialJoin = TextUtils.join("", initial);
        }

        holder.id.setText(initialJoin);
        holder.company.setText(modelCompany.getCompany());
        holder.city.setText(modelCompany.getCity());
        holder.card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentUpdate = new UpdateCompanyFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelCompany);
                fragmentUpdate.setArguments(bundle);

                TabLayout tabLayout = ((AppCompatActivity)context).findViewById(R.id.tab_layout_company);
                tabLayout.getTabAt(1).select();

                FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_company, fragmentUpdate);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, company, city;
        RelativeLayout card_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id      = itemView.findViewById(R.id.card_id);
            company = itemView.findViewById(R.id.card_company);
            city    = itemView.findViewById(R.id.card_city);
            card_layout = itemView.findViewById(R.id.card_layout_company);
        }
    }
}
