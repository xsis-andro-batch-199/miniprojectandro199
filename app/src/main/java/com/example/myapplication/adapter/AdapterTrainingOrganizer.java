package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.training_organizer.UpdateTrainingOrganizerFragment;
import com.example.myapplication.model.ModelTrainingOrganizer;

import java.util.List;

public class AdapterTrainingOrganizer extends RecyclerView.Adapter<AdapterTrainingOrganizer.ViewHolderTrainingOrganizer> {

    private Context context;
    private List<ModelTrainingOrganizer> trainingOrganizerList;

    public AdapterTrainingOrganizer(Context context, List<ModelTrainingOrganizer> trainingOrganizerList) {
        this.context = context;
        this.trainingOrganizerList = trainingOrganizerList;
    }

    @NonNull
    @Override
    public ViewHolderTrainingOrganizer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_training_organizer, parent, false);
        return new ViewHolderTrainingOrganizer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTrainingOrganizer holder, int position) {
        final ModelTrainingOrganizer modelTrainingOrganizer = trainingOrganizerList.get(position);
        final String[] kalimat = modelTrainingOrganizer.getTo_name().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);

        holder.tvInitialTrainingOrganizer.setText(initial);
        holder.tvTOName.setText(modelTrainingOrganizer.getTo_name());
        holder.rvTrainingOrganizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, initial + modelTrainingOrganizer.getId(), Toast.LENGTH_SHORT).show();
                UpdateTrainingOrganizerFragment updateTrainingOrganizerFragment = new UpdateTrainingOrganizerFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelTrainingOrganizer);
                updateTrainingOrganizerFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frameMainTrainingOrganizer, updateTrainingOrganizerFragment);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return trainingOrganizerList.size();
    }

    public class ViewHolderTrainingOrganizer extends RecyclerView.ViewHolder {
        RelativeLayout rvTrainingOrganizer;
        TextView tvInitialTrainingOrganizer, tvTOName;
        public ViewHolderTrainingOrganizer(@NonNull View itemView) {
            super(itemView);
            rvTrainingOrganizer = itemView.findViewById(R.id.rv_card_training_organizer);
            tvInitialTrainingOrganizer = itemView.findViewById(R.id.initialTrainingOrganizer);
            tvTOName = itemView.findViewById(R.id.tvTrainingOrganizer);
        }
    }
}
