package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelStatusPernikahan;
import com.example.myapplication.master.status_pernikahan.StatusPernikahanActivity;
import com.example.myapplication.master.status_pernikahan.UbahStatusPernikahanActivity;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterStatusPernikahan extends RecyclerView.Adapter<AdapterStatusPernikahan.ViewHolder> {
    private Context context;
    private List<ModelStatusPernikahan> listPernikahan;

    public AdapterStatusPernikahan(Context context, List<ModelStatusPernikahan> listPernikahan) {
        this.context = context;
        this.listPernikahan = listPernikahan;
    }

    @NonNull
    @Override
    public AdapterStatusPernikahan.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context)
                .inflate(R.layout.card_status_pernikahan, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterStatusPernikahan.ViewHolder holder, int position) {
        final ModelStatusPernikahan modelStatusPernikahan = listPernikahan.get(position);
        holder.id.setText(String.valueOf(position + 1));
        holder.sPernikahan.setText(modelStatusPernikahan.getStatuspernikahan());
        holder.diskripsi.setText(modelStatusPernikahan.getDeskripsi());
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.menu);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUpdate = new Intent(context, UbahStatusPernikahanActivity.class);
                                intentUpdate.putExtra(UbahStatusPernikahanActivity.EXTRA_DATA, modelStatusPernikahan);
                                context.startActivity(intentUpdate);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelStatusPernikahan.getStatuspernikahan()+ "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccessStatusPernikahan().getUpdateStatusPernikahanAll(modelStatusPernikahan.getId());
                                        Intent intentDelete = new Intent(context, StatusPernikahanActivity.class);
                                        context.startActivity(intentDelete);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listPernikahan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView id, sPernikahan, diskripsi;
        ImageView menu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardId);
            sPernikahan = itemView.findViewById(R.id.cardPernikahan);
            diskripsi = itemView.findViewById(R.id.cardDiskripsi);
            menu = itemView.findViewById(R.id.cardMenu);
        }
    }
}
