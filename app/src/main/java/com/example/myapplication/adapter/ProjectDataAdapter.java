package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelProject;
import com.example.myapplication.transaksi.project.DetailProjectFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class ProjectDataAdapter extends RecyclerView.Adapter<ProjectDataAdapter.MyViewHolder> {

    Context context;
    List<ModelProject> list;

    public ProjectDataAdapter(Context context, List<ModelProject> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ProjectDataAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_project, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectDataAdapter.MyViewHolder holder, int position) {
        final ModelProject modelProject = list.get(position);
        final String[] kalimat = modelProject.getClient_name().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);
        holder.initial.setText(initial);
        holder.client.setText(modelProject.getClient_name());
        holder.durasi.setText(modelProject.getStart()+" - "+modelProject.getEnd());
        holder.cardProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailProjectFragment dpf = new DetailProjectFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelProject);
                dpf.setArguments(bundle);

                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.frameLayout_project, dpf);
                /*transaction.addToBackStack(null);*/
                transaction.commit();

                TabLayout tabLayout = ((Activity)context).findViewById(R.id.tab_layout_project);
                tabLayout.getTabAt(1).select();
            }
        });


    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView initial, client, durasi;
        RelativeLayout cardProject;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            initial = itemView.findViewById(R.id.tv_id_project);
            client = itemView.findViewById(R.id.tv_nama_client_project);
            durasi = itemView.findViewById(R.id.tv_durasi_project);
            cardProject = itemView.findViewById(R.id.card_data_project);
        }
    }
}
