package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.schedule.ScheduleActivity;
import com.example.myapplication.master.schedule.UbahScheduleActivity;
import com.example.myapplication.model.ModelSchedule;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterSchedule extends RecyclerView.Adapter<AdapterSchedule.ScheduleViewHolder> {

    private Context context;
    private List<ModelSchedule> modelSchedules;

    public AdapterSchedule(Context context, List<ModelSchedule> modelSchedules) {
        this.context = context;
        this.modelSchedules = modelSchedules;
    }

    @NonNull
    @Override
    public ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_schedule, parent, false);
        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ScheduleViewHolder holder, int position) {
        final ModelSchedule listSchedule = modelSchedules.get(position);
        holder.tvIdSchedule.setText(String.valueOf(position +1));
        holder.tvNamaSchedule.setText(listSchedule.getJadwalNama());
        holder.tvDesSchedule.setText(listSchedule.getJadwalDeskripsi());
        holder.menuSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.menuSchedule);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                final Intent intent = new Intent(context, UbahScheduleActivity.class);
                                intent.putExtra(UbahScheduleActivity.EXTRA_DATA, listSchedule);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + listSchedule.getJadwalNama() + "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccessSchedule().QueryDeleteSchedule(listSchedule.getId());
                                        Intent intent1 = new Intent(context, ScheduleActivity.class);
                                        context.startActivity(intent1);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelSchedules.size();
    }

    public class ScheduleViewHolder extends RecyclerView.ViewHolder {

        TextView tvIdSchedule, tvNamaSchedule, tvDesSchedule;
        ImageView menuSchedule;

        public ScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
            tvIdSchedule = itemView.findViewById(R.id.idSchedule);
            tvNamaSchedule = itemView.findViewById(R.id.tvNamaSchedule);
            tvDesSchedule = itemView.findViewById(R.id.tvDescScheduele);
            menuSchedule = itemView.findViewById(R.id.idMenuSchedule);
        }
    }
}
