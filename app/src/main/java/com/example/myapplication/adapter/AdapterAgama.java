package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelAgama;
import com.example.myapplication.master.agama.AgamaActivity;
import com.example.myapplication.master.agama.EditAgamaActivity;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterAgama extends RecyclerView.Adapter<AdapterAgama.AgamaHolder> {

    private Context context;
    private List<ModelAgama> modelAgamaList;

    public AdapterAgama(Context context, List<ModelAgama> modelAgamaList) {
        this.context = context;
        this.modelAgamaList = modelAgamaList;
    }

    @NonNull
    @Override
    public AgamaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.
                getContext()).
                inflate(R.layout.card_agama,
                        parent, false);

        return new AgamaHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AgamaHolder holder, int position) {
        final ModelAgama modelAgama = modelAgamaList.get(position);
        holder.txtId_Agama.setText(String.valueOf(position + 1));
        holder.txtAgama.setText(modelAgama.getAgama());
        holder.txtDeskripsi_Agama.setText(modelAgama.getDeskripsi());
        holder.txtMenu_Agama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.txtMenu_Agama);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, EditAgamaActivity.class);
                                intentUbah.putExtra(EditAgamaActivity.EXTRA_DATA, modelAgama);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelAgama.getAgama() + "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccess().getUpdateAgama(modelAgama.getId());
                                        Intent intent = new Intent(context, AgamaActivity.class);
                                        context.startActivity(intent);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelAgamaList.size();
    }


    public class AgamaHolder extends RecyclerView.ViewHolder {
        TextView txtId_Agama, txtAgama, txtDeskripsi_Agama;
        ImageView txtMenu_Agama;

        public AgamaHolder(@NonNull View itemView) {
            super(itemView);
            txtId_Agama = itemView.findViewById(R.id.text_id_agama);
            txtAgama = itemView.findViewById(R.id.text_agama);
            txtDeskripsi_Agama = itemView.findViewById(R.id.text_deskripsi_agama);
            txtMenu_Agama = itemView.findViewById(R.id.text_menu_agama);
        }
    }
}
