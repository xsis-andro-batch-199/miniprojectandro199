package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.pendidikan.JenjangPendidikanActivity;
import com.example.myapplication.master.pendidikan.UbahPendidikanActivity;
import com.example.myapplication.model.ModelPendidikan;

import java.util.List;

import static com.example.myapplication.AppController.db;

/**
 * Dibuat oleh petersam pada 06/07/2019.
 * man.sanji23@gmail.com
 */
public class AdapterPendidikan extends RecyclerView.Adapter<AdapterPendidikan.ViewHolder> {

    private Context context;
    private List<ModelPendidikan> list;

    public AdapterPendidikan(Context context, List<ModelPendidikan> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_jenjang_pendidikan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ModelPendidikan modelPendidikan = list.get(position);
        holder.cardId.setText(String.valueOf(position + 1));
        holder.cardPendidikan.setText(modelPendidikan.getPendidikan());
        holder.cardDeskripsi.setText(modelPendidikan.getDeskripsi());
        holder.cardMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.cardMenu);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, UbahPendidikanActivity.class);
                                intentUbah.putExtra(UbahPendidikanActivity.EXTRA_DATA, modelPendidikan);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelPendidikan.getPendidikan() + " ?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccess().getUpdatePendidikanAll(modelPendidikan.getId());
                                        Intent intentHapus = new Intent(context, JenjangPendidikanActivity.class);
                                        context.startActivity(intentHapus);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView cardId, cardPendidikan, cardDeskripsi;
        ImageView cardMenu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardId = itemView.findViewById(R.id.card_id);
            cardPendidikan = itemView.findViewById(R.id.card_pendidikan);
            cardDeskripsi = itemView.findViewById(R.id.card_deskripsi);
            cardMenu = itemView.findViewById(R.id.card_menu);
        }
    }
}
