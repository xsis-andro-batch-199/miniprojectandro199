package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTipeIdentitas;
import com.example.myapplication.master.tipe_identitas.TipeIdentitasActivity;
import com.example.myapplication.master.tipe_identitas.UbahTipeIdentitasActivity;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterTipeIdentitas extends RecyclerView.Adapter<AdapterTipeIdentitas.MyViewHolder> {

    private Context context;
    private List<ModelTipeIdentitas> list;

    public AdapterTipeIdentitas(Context context, List<ModelTipeIdentitas> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent
                .getContext())
                .inflate(R.layout.card_tipe_identitas,
                        parent,
                        false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final ModelTipeIdentitas modelTipeIdentitas = list.get(position);

        holder.txtId.setText(String.valueOf(position + 1));
        holder.txtTipeIdentitas.setText(modelTipeIdentitas.getTipe_identitas());
        holder.txtDeskripsi.setText(modelTipeIdentitas.getDeskripsi());
        holder.btnMenuIdentitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.btnMenuIdentitas);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, UbahTipeIdentitasActivity.class);
                                intentUbah.putExtra(UbahTipeIdentitasActivity.EXTRA_DATA, modelTipeIdentitas);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelTipeIdentitas.getTipe_identitas() + " ?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccess().getUpdateTipeIdentitasAll(modelTipeIdentitas.getId());
                                        Intent intentHapus = new Intent(context, TipeIdentitasActivity.class);
                                        context.startActivity(intentHapus);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtId, txtTipeIdentitas, txtDeskripsi;
        ImageView btnMenuIdentitas;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = itemView.findViewById(R.id.text_id_identitas);
            txtTipeIdentitas = itemView.findViewById(R.id.text_tipe_dentitas);
            txtDeskripsi = itemView.findViewById(R.id.text_deskripsi_identitas);
            btnMenuIdentitas = itemView.findViewById(R.id.button_menu_identitas);
        }
    }
}
