package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.master.employee_status.DataEmployeeStatusFragment;
import com.example.myapplication.master.employee_status.FormEmployeeStatusFragment;

public class EmployeeStatusPagerAdapter extends FragmentStatePagerAdapter {
    int tab;

    public EmployeeStatusPagerAdapter(@NonNull FragmentManager fm, int tab) {
        super(fm);
        this.tab = tab;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DataEmployeeStatusFragment tabData = new DataEmployeeStatusFragment();
                return tabData;
            case 1:
                FormEmployeeStatusFragment tabForm = new FormEmployeeStatusFragment();
                return tabForm;
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return tab;
    }
}
