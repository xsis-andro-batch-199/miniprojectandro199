package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.keahlian.MKeahlianActivity;
import com.example.myapplication.master.keahlian.UbahKeahlianActivity;
import com.example.myapplication.model.ModelKeahlian;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterKeahlian extends RecyclerView.Adapter<AdapterKeahlian.ViewHolder> {

    private Context context;
    private List<ModelKeahlian> list;

    public AdapterKeahlian(Context context, List<ModelKeahlian> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public AdapterKeahlian.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_keahlian, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ModelKeahlian modelKeahlian = list.get(position);
        holder.listId.setText(String.valueOf(position + 1));
        holder.jenisKeahlian.setText(modelKeahlian.getNama_keahlian());
        holder.listDeskripsi.setText(modelKeahlian.getDeskripsi_keahlian());
        holder.listMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.listMenu);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, UbahKeahlianActivity.class);
//                                intentUbah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentUbah.putExtra(UbahKeahlianActivity.EXTRA_DATA, modelKeahlian);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
//                                tanpa alert
                                /*db.daoAccess().deleteKeahlian(modelKeahlian.getId());
                                context.startActivity(new Intent(context, KeahlianActivity.class));*/


//                                pakai alert

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelKeahlian.getNama_keahlian() + " ?")
                                .setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })

                                .setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        db.daoAccess().deleteKeahlian(modelKeahlian.getId());
                                        db.daoAccessKeahlian().getUpdateKeahlian(modelKeahlian.getId());
                                        Intent intentHapus = new Intent(context, MKeahlianActivity.class);
                                        intentHapus.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intentHapus);
                                        ((Activity)context).finish();
                                    }
                                });
//                                AlertDialog dialog = builder.create();
//                                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//                                dialog.show();
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView listId, jenisKeahlian, listDeskripsi;
        ImageView listMenu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            listId = itemView.findViewById(R.id.idKeahlian);
            jenisKeahlian = itemView.findViewById(R.id.tvKeahlian);
            listDeskripsi = itemView.findViewById(R.id.tvDeskripsiKeahlian);
            listMenu = itemView.findViewById(R.id.idMenuKeahlian);
        }
    }
}
