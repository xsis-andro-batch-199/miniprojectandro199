package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.jenis_catatan.JenisCatatanActivity;
import com.example.myapplication.master.jenis_catatan.UbahJenisCatatan;
import com.example.myapplication.model.ModelJenisCatatan;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterJenisCatatan extends RecyclerView.Adapter<AdapterJenisCatatan.JenisCatatanHolder> {
    private Context context;
    private List<ModelJenisCatatan> modelJenisCatatans;


    public AdapterJenisCatatan(Context context, List<ModelJenisCatatan> modelJenisCatatans) {
        this.context = context;
        this.modelJenisCatatans = modelJenisCatatans;
    }

    @NonNull
    @Override
    public JenisCatatanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_jenis_catatan, parent, false);
        return new JenisCatatanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final JenisCatatanHolder holder, final int position) {
        final ModelJenisCatatan listJenisCatatan = modelJenisCatatans.get(position);
        holder.tvIdJenisCatatan.setText(String.valueOf(position + 1));
        holder.tvNamaJenisCatatan.setText(listJenisCatatan.getJenisCatatanNama());
        holder.tvDeskripsiJenisCatatan.setText(listJenisCatatan.getJenisCatatanDeskripsi());
        holder.ivMenuJenisCatatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.ivMenuJenisCatatan);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, UbahJenisCatatan.class);
                                intentUbah.putExtra(UbahJenisCatatan.EXTRA_DATA, listJenisCatatan);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus Data " + listJenisCatatan.getJenisCatatanNama() + "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        db.daoAccessJenisCatatan().QueryDeleteJenisCatatan(listJenisCatatan.getId());
                                        Intent intent = new Intent(context, JenisCatatanActivity.class);
                                        context.startActivity(intent);
                                        ((Activity)context).finish();

                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelJenisCatatans.size();
    }

    public class JenisCatatanHolder extends RecyclerView.ViewHolder {
        TextView tvIdJenisCatatan, tvNamaJenisCatatan, tvDeskripsiJenisCatatan;
        ImageView ivMenuJenisCatatan;
        public JenisCatatanHolder(@NonNull View itemView) {
            super(itemView);
            tvIdJenisCatatan = itemView.findViewById(R.id.tv_id_jenis_catatan);
            tvNamaJenisCatatan = itemView.findViewById(R.id.tv_nama_jenis_catatan);
            tvDeskripsiJenisCatatan = itemView.findViewById(R.id.tv_deskripsi_jenis_catatan);
            ivMenuJenisCatatan = itemView.findViewById(R.id.iv_menu_jenis_catatan);
        }
    }
}
