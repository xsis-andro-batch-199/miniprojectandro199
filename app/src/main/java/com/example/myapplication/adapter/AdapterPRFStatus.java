package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.prf_status.UbahPRFStatusFragment;
import com.example.myapplication.model.ModelPRFStatus;

import java.util.List;

public class AdapterPRFStatus extends RecyclerView.Adapter<AdapterPRFStatus.ViewHolder> {
    private Context context;
    private List<ModelPRFStatus> prfStatusList;

    public AdapterPRFStatus(Context context, List<ModelPRFStatus> prfStatusList) {
        this.context = context;
        this.prfStatusList = prfStatusList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context)
                .inflate(R.layout.card_data_prfstatus, parent, false);
        return new ViewHolder(viewItem);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelPRFStatus modelPRFStatus = prfStatusList.get(position);
        final String[] nama = modelPRFStatus.getNamaprf().split(" ");
        final String[] value = new String[nama.length];
        for (int i = 0; i < nama.length ; i++) {
            value[i] = String.valueOf(nama[i].charAt(0));
        }
        final String initial = TextUtils.join("", value);
        holder.id.setText(initial);
        holder.nama.setText(modelPRFStatus.getNamaprf());
        holder.relativeLayoutPRFStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UbahPRFStatusFragment ubahFragment = new UbahPRFStatusFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelPRFStatus);
                ubahFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.mainPRFStatusLayout, ubahFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


    }

    @Override
    public int getItemCount() {
        return prfStatusList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, nama;
        RelativeLayout relativeLayoutPRFStatus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardIdDataPRFStatus);
            nama = itemView.findViewById(R.id.cardDataPRFStatus);
            relativeLayoutPRFStatus = itemView.findViewById(R.id.cardLayoutPRFStatus);
        }
    }
}
