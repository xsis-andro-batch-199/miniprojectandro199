package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.employee_type.UpdateEmployeeTypeFragment;
import com.example.myapplication.model.ModelBOP;
import com.example.myapplication.model.ModelEmployeeType;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class EmployeeTypeAdapter extends RecyclerView.Adapter<EmployeeTypeAdapter.ViewHolder> {
    private Context context;
    private List<ModelEmployeeType> list;

    public EmployeeTypeAdapter(Context context, List<ModelEmployeeType> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public EmployeeTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_employee_type, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeTypeAdapter.ViewHolder holder, int position) {
        final ModelEmployeeType modelEmployeeType = list.get(position);

        String[] name = modelEmployeeType.getName().split(" ");
        String[] initial = new String[name.length];
        for (int i = 0; i < name.length; i++){
            initial[i] = String.valueOf(name[i].charAt(0));
        }
        String initialJoin = TextUtils.join("", initial);

        holder.id.setText(initialJoin);
        holder.name.setText(modelEmployeeType.getName());
        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new UpdateEmployeeTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelEmployeeType);
                fragment.setArguments(bundle);

                TabLayout tabLayout = ((AppCompatActivity)context).findViewById(R.id.tab_layout_et);
                tabLayout.getTabAt(1).select();

                FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_et, fragment);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, name;
        RelativeLayout cardLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id          = itemView.findViewById(R.id.card_id_et);
            name        = itemView.findViewById(R.id.card_name_et);
            cardLayout  = itemView.findViewById(R.id.card_layout_et);
        }
    }
}
