package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.contract_status.UbahFormContractStatusFragment;
import com.example.myapplication.model.ModelContractStatus;

import java.util.List;

public class AdapterContractStatus extends RecyclerView.Adapter<AdapterContractStatus.ViewHolder> {
    private Context context;
    private List<ModelContractStatus> statusList;

    public AdapterContractStatus(Context context, List<ModelContractStatus> statusList) {
        this.context = context;
        this.statusList = statusList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context)
        .inflate(R.layout.card_data_contract_status, parent, false);
        return new  ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelContractStatus modelContractStatus = statusList.get(position);
        final String[] nama = modelContractStatus.getNamacontract().split(" ");
        final String[] value = new String[nama.length];
        for (int i = 0; i < nama.length ; i++) {
            value[i] = String.valueOf(nama[i].charAt(0));
        }
        final String initial = TextUtils.join("", value);
        holder.id.setText(initial);
        holder.nama.setText(modelContractStatus.getNamacontract());
        holder.relativeLayoutContractStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 UbahFormContractStatusFragment ubahFragment = new UbahFormContractStatusFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelContractStatus);
                ubahFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.mainContractStatus, ubahFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, nama;
        RelativeLayout relativeLayoutContractStatus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardIdDataContract);
            nama = itemView.findViewById(R.id.cardDataContractStatus);
            relativeLayoutContractStatus = itemView.findViewById(R.id.cardLayoutContractStatus);

        }
    }
}
