package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.grade.UbahGradeFragment;
import com.example.myapplication.model.ModelGrade;

import java.util.List;

public class AdapterGrade extends RecyclerView.Adapter<AdapterGrade.ViewHolder> {

    private Context context;
    private List<ModelGrade> listGrade;

    public AdapterGrade(Context context, List<ModelGrade> listGrade) {
        this.context = context;
        this.listGrade = listGrade;
    }

    @NonNull
    @Override
    public AdapterGrade.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_grade, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterGrade.ViewHolder holder, int position) {
        final ModelGrade modelGrade = listGrade.get(position);
        final String[] kalimat = modelGrade.getGrade().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("", value);
        holder.tvInitialGrade.setText(initial);
        holder.tvGade.setText(modelGrade.getGrade());
        holder.rlCardGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, initial + " idnya " + modelGrade.getId(), Toast.LENGTH_SHORT).show();
                UbahGradeFragment ubahGradeFragment = new UbahGradeFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelGrade);
                ubahGradeFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.mainFrameGrade, ubahGradeFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listGrade.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlCardGrade;
        TextView tvGade, tvInitialGrade;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rlCardGrade = itemView.findViewById(R.id.rlCardGrade);
            tvGade = itemView.findViewById(R.id.tvGrade);
            tvInitialGrade = itemView.findViewById(R.id.tvInitialGrade);
        }
    }
}
