package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDetailProjectAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> listMenu;
    private HashMap<String, List<String>> listValue;
    private HashMap<String, List<String>> listSubmenu = new HashMap<>();
    ExpandableListView expandableListView;

    public ExpandableListDetailProjectAdapter(Context context,
                                              List<String> listMenu,
                                              HashMap<String, List<String>> listValue,
                                              ExpandableListView expandableListView) {
        this.context = context;
        this.listMenu = listMenu;
        this.listValue = listValue;
        this.expandableListView = expandableListView;
    }

    @Override
    public int getGroupCount() {
        return listMenu.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return listValue.get(listMenu.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listMenu.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return listValue.get(listMenu.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String meuTitle = (String) getGroup(i);
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_menu_expandable_navbar, null);
        }
        TextView tv_menu = view.findViewById(R.id.tv_menu_expandable_navbar);
        tv_menu.setText(meuTitle);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        List<String> submenu1 = new ArrayList<>();
        submenu1.add("Client Name");
        submenu1.add("Location");
        submenu1.add("Department");
        submenu1.add("User/PIC Name");
        submenu1.add("Project Name");
        submenu1.add("Project Started - Ended");
        submenu1.add("Role");
        submenu1.add("Project Phase");
        List<String> submenu2 = new ArrayList<>();
        submenu2.add("Project Description");
        List<String> submenu3 = new ArrayList<>();
        submenu3.add("Project Technology");
        List<String> submenu4 = new ArrayList<>();
        submenu4.add("Main Task");
        listSubmenu.put(listMenu.get(0), submenu1);
        listSubmenu.put(listMenu.get(1), submenu2);
        listSubmenu.put(listMenu.get(2), submenu3);
        listSubmenu.put(listMenu.get(3), submenu4);
        String submenuTitle = (String) listSubmenu.get(listMenu.get(i)).get(i1);
        String submenuValue = (String) getChild(i, i1);
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_submenu_expandable_detail_project, null);
        }
        TextView tv_submenuTitle = view.findViewById(R.id.tv_submenu_expandable_project);
        TextView tv_submenuValue = view.findViewById(R.id.tv_value_expandable_project);
        tv_submenuTitle.setText(submenuTitle);
        tv_submenuValue.setText(submenuValue);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
