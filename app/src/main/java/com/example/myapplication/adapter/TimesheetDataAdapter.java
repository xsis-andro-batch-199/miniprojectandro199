package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelTimesheet;
import com.example.myapplication.transaksi.timesheet.DetailTimesheetFragment;
import com.example.myapplication.transaksi.timesheet.HolderTimesheetFragment;
import com.google.android.material.tabs.TabLayout;

import java.io.Serializable;
import java.util.List;

/**
 * Dibuat oleh petersam pada 10/07/2019.
 * man.sanji23@gmail.com
 */
public class TimesheetDataAdapter extends RecyclerView.Adapter<TimesheetDataAdapter.ViewHolder> {

    private Context context;
    private List<ModelTimesheet> list;

    public TimesheetDataAdapter(Context context, List<ModelTimesheet> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_timesheet, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final ModelTimesheet modelTimesheet = list.get(position);
        holder.id.setText(String.valueOf(modelTimesheet.getId()));
        holder.tanggal.setText(String.valueOf(modelTimesheet.getReportDate()));
        holder.status.setText(modelTimesheet.getStatus());
        holder.card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new DetailTimesheetFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelTimesheet);
                fragment.setArguments(bundle);

                TabLayout tabLayout = ((Activity)context).findViewById(R.id.tab_layout);
                tabLayout.getTabAt(1).select();

                FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_timesheet, fragment);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, tanggal, status;
        RelativeLayout card_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.card_id);
            tanggal = itemView.findViewById(R.id.card_date);
            status = itemView.findViewById(R.id.card_status);
            card_layout = itemView.findViewById(R.id.card_layout);
        }
    }
}
