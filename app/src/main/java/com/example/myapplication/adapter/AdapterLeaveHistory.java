package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelLeave;
import com.example.myapplication.transaksi.leave_request.DetailLeaveFragment;
import com.example.myapplication.transaksi.leave_request.HolderLeaveFragment;
import com.example.myapplication.transaksi.leave_request.HolderLeaveFragment_2;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class AdapterLeaveHistory extends RecyclerView.Adapter<AdapterLeaveHistory.HistoryHolder> {

    private Context context;
    private List<ModelLeave> list;

    public AdapterLeaveHistory(Context context, List<ModelLeave> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_leave_history, parent, false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder holder, int position) {
        final ModelLeave modelLeave = list.get(position);
        holder.id.setText(String.valueOf(modelLeave.getId()));
        holder.tanggal.setText(String.valueOf(modelLeave.getStartDate()));
        holder.leave_type.setText(modelLeave.getLeave_type());
        holder.card_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailLeaveFragment detailLeaveFragment = new DetailLeaveFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelLeave);
                detailLeaveFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_content, detailLeaveFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                /*DetailLeaveFragment detailLeaveFragment = new DetailLeaveFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id", modelLeave.getId());
                detailLeaveFragment.setArguments(bundle);

                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.mainLeave, detailLeaveFragment);
                transaction.addToBackStack(null);
                transaction.commit();

                TabLayout tabLayout = ((Activity)context).findViewById(R.id.tab_layout);
                tabLayout.getTabAt(1).select();*/
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        }
        return 0;
    }

    public class HistoryHolder extends RecyclerView.ViewHolder {
        TextView id, tanggal, leave_type;
        RelativeLayout card_history;

        public HistoryHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardId);
            tanggal = itemView.findViewById(R.id.cardTanggal);
            leave_type = itemView.findViewById(R.id.cardLeaveType);
            card_history = itemView.findViewById(R.id.card_history);
        }
    }
}
