package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.transaksi.prfrequest.CandidatePRFRequestFragment;
import com.example.myapplication.transaksi.prfrequest.DataPRFRequestFragment;

public class PRFRequestPagerAdapter extends FragmentStatePagerAdapter {
    int tab;

    public PRFRequestPagerAdapter(@NonNull FragmentManager fm, int tab) {
        super(fm);
        this.tab = tab;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DataPRFRequestFragment tabData = new DataPRFRequestFragment();
                return tabData;
            case 1:
                CandidatePRFRequestFragment tabDetail = new CandidatePRFRequestFragment();
                return tabDetail;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tab;
    }
}
