package com.example.myapplication.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.periode.PeriodeActivity;
import com.example.myapplication.master.periode.UbahPeriodeActivity;
import com.example.myapplication.model.ModelPeriode;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class AdapterPeriode extends RecyclerView.Adapter<AdapterPeriode.ViewHolder> {
    private Context context;
    private List<ModelPeriode> listPeriode;

    public AdapterPeriode(Context context, List<ModelPeriode> listPeriode) {
        this.context = context;
        this.listPeriode = listPeriode;
    }

    @NonNull
    @Override
    public AdapterPeriode.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_periode, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterPeriode.ViewHolder holder, int position) {
        final ModelPeriode modelPeriode = listPeriode.get(position);
        holder.listIdPeriode.setText(String.valueOf(position + 1));
        holder.listPeriode.setText(modelPeriode.getPeriode());
        holder.listDeskripsiPeriode.setText(modelPeriode.getDeskripsi());
        holder.imgTitik3Periode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.imgTitik3Periode);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                Intent intentUbah = new Intent(context, UbahPeriodeActivity.class);
                                intentUbah.putExtra(UbahPeriodeActivity.EXTRA_DATA, modelPeriode);
                                context.startActivity(intentUbah);
                                ((Activity)context).finish();
                            break;

                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelPeriode.getPeriode() + "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });

                                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        delete tidak permanen
                                        db.daoAccessPeriode().getUpdatePeriode(modelPeriode.getId());
//                                        delete permanent
//                                        db.daoAccess().deletePeriode(modelPeriode.getId());
                                        Intent intentHapus = new Intent(context, PeriodeActivity.class);
                                        context.startActivity(intentHapus);
                                        ((Activity)context).finish();
                                    }
                                });
                                builder.show();
                            break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listPeriode.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView listIdPeriode, listPeriode, listDeskripsiPeriode;
        ImageView imgTitik3Periode;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            listIdPeriode = itemView.findViewById(R.id.tvIdPeriode);
            listPeriode = itemView.findViewById(R.id.tvListPeriode);
            listDeskripsiPeriode = itemView.findViewById(R.id.tvListDeskripPeriode);
            imgTitik3Periode = itemView.findViewById(R.id.ivTitik3Periode);
        }
    }
}
