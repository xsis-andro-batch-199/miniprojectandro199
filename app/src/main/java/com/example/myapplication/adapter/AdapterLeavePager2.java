package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.transaksi.leave_request.ApprovalLeaveFragment;
import com.example.myapplication.transaksi.leave_request.DetailLeaveFragment;

public class AdapterLeavePager2 extends FragmentStatePagerAdapter {
    int tab;

    public AdapterLeavePager2(@NonNull FragmentManager fm, int tab) {
        super(fm);
        this.tab = tab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                DetailLeaveFragment tabDetail = new DetailLeaveFragment();
                return tabDetail;
            case 1:
                ApprovalLeaveFragment tabApprov = new ApprovalLeaveFragment();
                return tabApprov;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tab;
    }
}
