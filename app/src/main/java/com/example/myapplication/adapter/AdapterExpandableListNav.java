package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterExpandableListNav extends BaseExpandableListAdapter {
    private Context context;
    private List<String> listMenu;
    private HashMap<String, List<String>> listSubmenu;
    ExpandableListView expandableListView;

    public AdapterExpandableListNav(Context context,
                                    List<String> listMenu,
                                    HashMap<String, List<String>> listSubmenu,
                                    ExpandableListView expandableListView) {
        this.context = context;
        this.listMenu = listMenu;
        this.listSubmenu = listSubmenu;
        this.expandableListView = expandableListView;
    }

    @Override
    public int getGroupCount() {
        return this.listMenu.size();
    }

    @Override
    public int getChildrenCount(int i) {
        int childCount = 0;
        if (i != 5){
            childCount = this.listSubmenu.get(listMenu.get(i)).size();
        }
        return this.listSubmenu.get(listMenu.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return this.listMenu.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return this.listSubmenu.get(listMenu.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String menuTitle = (String)getGroup(i);
        if (view == null){
            LayoutInflater inflater =(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_menu_expandable_navbar, null);
        }
        TextView textMenu = view.findViewById(R.id.tv_menu_expandable_navbar);
        textMenu.setText(menuTitle);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        String submenuTitle = (String)getChild(i, i1);
        if (view == null){
            LayoutInflater inflater =(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.layout_submenu_expandable_navbar, null);
        }
        TextView textSubmenu = view.findViewById(R.id.tv_submenu_expandable_navbar);
        textSubmenu.setText(submenuTitle);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
