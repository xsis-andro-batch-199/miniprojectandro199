package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.provider_tools.UbahProviderFragment;
import com.example.myapplication.model.ModelProviderTools;

import java.util.List;

public class AdapterProviderTools extends RecyclerView.Adapter<AdapterProviderTools.ViewHolderProvider> {

    private Context context;
    private List<ModelProviderTools> listProvider;

    public AdapterProviderTools(Context context, List<ModelProviderTools> listProvider) {
        this.context = context;
        this.listProvider = listProvider;
    }

    @NonNull
    @Override
    public ViewHolderProvider onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_provider_tools, parent, false);

        return new ViewHolderProvider(view);
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull ViewHolderProvider holder, int position) {
        final ModelProviderTools modelProviderTools = listProvider.get(position);
        final String[] kalimat = modelProviderTools.getProvider().split(" ");
        final String[] value = new String[kalimat.length];
        for (int i = 0; i < kalimat.length; i++){
            value[i] = String.valueOf(kalimat[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);
        holder.tvInisialProvider.setText(initial);
        holder.tvProviderTools.setText(modelProviderTools.getProvider());
        holder.rlCardProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, initial + " idnya " + modelProviderTools.getId(), Toast.LENGTH_SHORT).show();
                UbahProviderFragment ubahProviderFragment = new UbahProviderFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelProviderTools);
                ubahProviderFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameMainProvider, ubahProviderFragment);
                fragmentTransaction.commit();
            }
        });

    }



    @Override
    public int getItemCount() {
        return listProvider.size();
    }

    public class ViewHolderProvider extends RecyclerView.ViewHolder {
        RelativeLayout rlCardProvider;
        TextView tvInisialProvider, tvProviderTools;
        public ViewHolderProvider(@NonNull View itemView) {
            super(itemView);
            rlCardProvider = itemView.findViewById(R.id.rlCardProviderTools);
            tvInisialProvider = itemView.findViewById(R.id.tvInisialProvider);
            tvProviderTools = itemView.findViewById(R.id.tvProviderTools);
        }
    }
}
