package com.example.myapplication.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ModelPRFRequest;
import com.example.myapplication.model.ModelPRFRequestCandidates;
import com.example.myapplication.transaksi.prfrequest.MainPRFRequestFragment;
import com.example.myapplication.transaksi.prfrequest.PRFRequestUbahFormCandidates;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class PRFRequestCandidatesAdapter extends RecyclerView.Adapter<PRFRequestCandidatesAdapter.ViewHolder> {
    private Context context;
    private List<ModelPRFRequestCandidates> modelPRFRequestList;

    public PRFRequestCandidatesAdapter(Context context, List<ModelPRFRequestCandidates> modelPRFRequestList) {
        this.context = context;
        this.modelPRFRequestList = modelPRFRequestList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context)
                .inflate(R.layout.card_prf_candidates, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ModelPRFRequestCandidates modelPRFRequest = modelPRFRequestList.get(position);
        final String[] textNamaCandidates = modelPRFRequest.getNama().split(" ");
        final String[] value = new String[textNamaCandidates.length];
        for (int i = 0; i <textNamaCandidates.length ; i++) {
            value[i] = String.valueOf(textNamaCandidates[i].charAt(0));
        }
        final String initial = TextUtils.join("",value);
        holder.id.setText(initial);
        holder.namaData.setText(modelPRFRequest.getNama());
        holder.keterangan.setText(modelPRFRequest.getPosition());
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, holder.menu);
                popupMenu.inflate(R.menu.list_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.action_ubah:
                                PRFRequestUbahFormCandidates ubahCandidate = new PRFRequestUbahFormCandidates();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("data", modelPRFRequest);
                                ubahCandidate.setArguments(bundle);

                                final FragmentManager fragmentManager = ((AppCompatActivity)context)
                                        .getSupportFragmentManager();
                                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                                transaction.replace(R.id.frame_content, ubahCandidate);
                                transaction.commit();
                                break;
                            case R.id.action_hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Hapus data " + modelPRFRequest.getNama()+ "?");
                                builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MainPRFRequestFragment deleteData = new MainPRFRequestFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("data", modelPRFRequest);
                                        deleteData.setArguments(bundle);

                                        FragmentManager fragmentManager = ((AppCompatActivity)context)
                                                .getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frame_content, deleteData);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                        db.daoAccessPRFRequest().DeletePRFCandidate(modelPRFRequest.getId());
                                    }
                                });
                                builder.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelPRFRequestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, namaData, keterangan;
        ImageView menu;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardId);
            namaData = itemView.findViewById(R.id.cardNamaCandidates);
            keterangan = itemView.findViewById(R.id.cardDiskripsi);
            menu = itemView.findViewById(R.id.cardMenuCandidatesPRF);
        }
    }
}
