package com.example.myapplication.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.master.employee_status.UbahFormEmployeeStatusFragment;
import com.example.myapplication.model.ModelEmployeeStatus;

import java.util.List;


public class EmployeeStatusAdapter extends RecyclerView.Adapter<EmployeeStatusAdapter.ViewHolder> {
    private Context context;
    private List<ModelEmployeeStatus> listEmployee;

    public EmployeeStatusAdapter(Context context, List<ModelEmployeeStatus> listEmployee) {
        this.context = context;
        this.listEmployee = listEmployee;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context)
                .inflate(R.layout.card_data_employee_status, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelEmployeeStatus modelEmployeeStatus = listEmployee.get(position);
        final String[] nama = modelEmployeeStatus.getNamaemployee().split(" ");
        final String[] value = new String[nama.length];
        for (int i = 0; i < nama.length ; i++) {
            value[i] = String.valueOf(nama[i].charAt(0));
        }
        final String initial = TextUtils.join("", value);
        holder.id.setText(initial);
        holder.nama.setText(modelEmployeeStatus.getNamaemployee());
        holder.relativeLayoutEmployeeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UbahFormEmployeeStatusFragment ubahFragment = new UbahFormEmployeeStatusFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", modelEmployeeStatus);
                ubahFragment.setArguments(bundle);

                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.mainEmployeeStatusLayout, ubahFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listEmployee.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, nama;
        RelativeLayout relativeLayoutEmployeeStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.cardIdDataEmployee);


            nama = itemView.findViewById(R.id.cardDataStatusEmployee);
            relativeLayoutEmployeeStatus = itemView.findViewById(R.id.cardDataEmployeeStatus);

        }
    }
}
