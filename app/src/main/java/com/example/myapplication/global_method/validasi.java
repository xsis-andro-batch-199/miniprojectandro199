package com.example.myapplication.global_method;

import com.example.myapplication.model.ModelAgama;

import java.util.List;

import static com.example.myapplication.AppController.db;

public class validasi {

    private boolean cekDataNama(String valueCek) {
        List<ModelAgama> list =db.daoAccess().getAllAgamaNoCondition();
        for (int i = 0; i < list.size(); i++){
            if (valueCek.contentEquals(list.get(i).getAgama())) {
                return true;
            }
        }
        return false;
    }
}
